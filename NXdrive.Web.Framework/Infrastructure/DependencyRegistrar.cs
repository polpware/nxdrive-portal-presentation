using Autofac;
using Autofac.Core;
using Microsoft.EntityFrameworkCore;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.DBMapping.UserIdentity.Context;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Services.UserIdentity;

namespace NXdrive.Web.Framework.Infrastructure
{

    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        // Note that we put it after the application has finished registered the services defined in the nop services
        // but before the appication starts to register the services in the plugins.
        public int Order => 5;

        private const string UserIdentityDBContextID = "nxdrive_user_identity_dbcontext";

        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //////////////////////////////////////////////////////////
            // User identity services
            /////////////////////////////////////////////////////////
            // data context
            //builder.RegisterPluginDataContext<UserIdentityDBContext>(UserIdentityDBContextID);

            builder.Register(context => new UserIdentityDBContext(context.Resolve<DbContextOptions<UserIdentityDBContext>>()))
                .Named<IDbContext>(UserIdentityDBContextID).InstancePerLifetimeScope();

            // Repos
            builder.RegisterType<EfRepository<UserGroup>>().As<IRepository<UserGroup>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(UserIdentityDBContextID))
                .InstancePerLifetimeScope();
            builder.RegisterType<EfRepository<UserGroupMember>>().As<IRepository<UserGroupMember>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(UserIdentityDBContextID))
                .InstancePerLifetimeScope();
            builder.RegisterType<EfRepository<UserGroupInvitation>>().As<IRepository<UserGroupInvitation>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(UserIdentityDBContextID))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<EmailIdentity>>().As<IRepository<EmailIdentity>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(UserIdentityDBContextID))
                .InstancePerLifetimeScope();
            builder.RegisterType<EfRepository<SocialNetwork>>().As<IRepository<SocialNetwork>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(UserIdentityDBContextID))
                .InstancePerLifetimeScope();

            // Services
            builder.RegisterType<UserGroupService>()
                .As<IUserGroupService>()
                .InstancePerLifetimeScope();
            builder.RegisterType<UserIdentityService>()
                .As<IUserIdentityService>()
                .InstancePerLifetimeScope();
            builder.RegisterType<MemberInvitationService>()
                .As<IMemberInvitationService>()
                .InstancePerLifetimeScope();
            builder.RegisterType<GroupMemberService>()
                .As<IGroupMemberService>()
                .InstancePerLifetimeScope();
            builder.RegisterType<GroupActivityService>()
                .As<IGroupActivityService>()
                .InstancePerLifetimeScope();

            // Goup message services
            builder.RegisterType<NXdrive.NopExt.Messaging.MessageTokenProvider>()
                .As<NXdrive.NopExt.Messaging.IMessageTokenProvider>()
                .InstancePerLifetimeScope();
            builder.RegisterType<NXdrive.NopExt.Messaging.MessageTokenProvider>()
                .As<Nop.Services.Messages.IMessageTokenProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<NXdrive.NopExt.Messaging.WorkflowMessageService>()
                .As<NXdrive.NopExt.Messaging.IWorkflowMessageService>()
                .InstancePerLifetimeScope();
            // Overwrite some services in nop
            builder.RegisterType<NXdrive.NopExt.Messaging.WorkflowMessageService>()
                .As<Nop.Services.Messages.IWorkflowMessageService>()
                .InstancePerLifetimeScope();


        }
    }
}