﻿using Microsoft.AspNetCore.Mvc.Filters;
using Nop.Core;
using Nop.Services.Events;
using NXdrive.Web.CustomEvents.Customers;
using System.Threading.Tasks;

namespace NXdrive.Web.AOP.Customers
{
    public class AccountActivationNotyAttribute : ResultFilterAttribute
    {
        public override async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            var workContext = Nop.Core.Infrastructure.EngineContext.Current.Resolve<IWorkContext>();
            var customer = workContext.CurrentCustomer;
            if (customer.Active)
            {
                var eventPublisher = Nop.Core.Infrastructure.EngineContext.Current.Resolve<IEventPublisher>();
                eventPublisher.Publish(new AccountActivationSuccessEvent(customer));
            }

            await next();
        }
    }
}
