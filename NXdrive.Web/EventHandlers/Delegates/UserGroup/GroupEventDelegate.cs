﻿using Hangfire;
using Nop.Core;
using Nop.Core.Events;
using Nop.Services.Events;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Web.EventHandlers.Actions.UserGroup;

namespace NXdrive.Web.EventHandlers.Delegates.UserGroup
{
    public class GroupEventConsumerDelegate :
        IConsumer<EntityInsertedEvent<UserGroupInvitation>>,
        IConsumer<EntityUpdatedEvent<UserGroupInvitation>>,
        IConsumer<EntityInsertedEvent<UserGroupMember>>,
        IConsumer<EntityUpdatedEvent<UserGroupMember>>
    {
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        public GroupEventConsumerDelegate(IStoreContext storeContext,
            IWorkContext workContext)
        {
            this._storeContext = storeContext;
            this._workContext = workContext;
        }

        public void HandleEvent(EntityInsertedEvent<UserGroupInvitation> eventMessage)
        {
            // There are three cases
            // Case 1: Owner sends out an invitation
            // Case 2: Application
            // Case 3: ...
            BackgroundJob.Enqueue(() =>
                NotyCreatedInvitationOrApplicationAction.Invoke(eventMessage.Entity.Id,
                _storeContext.CurrentStore.DefaultLanguageId)
            );
        }

        public void HandleEvent(EntityUpdatedEvent<UserGroupInvitation> eventMessage)
        {
            BackgroundJob.Enqueue(() =>
                NotyProcessedInvitationOrApplicationAction.Invoke4DeniedOne(eventMessage.Entity.Id,
                _storeContext.CurrentStore.DefaultLanguageId)
            );
        }

        public void HandleEvent(EntityInsertedEvent<UserGroupMember> eventMessage)
        {
            BackgroundJob.Enqueue(() =>
               NotyProcessedInvitationOrApplicationAction.Invoke4AcceptedOne(eventMessage.Entity.Id,
               _storeContext.CurrentStore.DefaultLanguageId)
           );
        }

        public void HandleEvent(EntityUpdatedEvent<UserGroupMember> eventMessage)
        {
            BackgroundJob.Enqueue(() =>
                NotyUpdatedMemberStateAction.Invoke(eventMessage.Entity.Id, _workContext.CurrentCustomer.Id,
                _storeContext.CurrentStore.DefaultLanguageId)
                );
        }
    }
}
