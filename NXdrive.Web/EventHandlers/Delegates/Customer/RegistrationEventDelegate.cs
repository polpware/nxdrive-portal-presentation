﻿using Hangfire;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Events;
using NXdrive.Web.CustomEvents.Customers;
using NXdrive.Web.EventHandlers.Actions.Customer;

namespace NXdrive.Web.EventHandlers.Delegates.Customer
{
    public class RegistrationEventDelegate : IConsumer<CustomerRegisteredEvent>,
        IConsumer<AccountActivationSuccessEvent>,
        IConsumer<CustomerLoggedinEvent>
    {
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        public RegistrationEventDelegate(IStoreContext storeContext,
            IWorkContext workContext)
        {
            this._storeContext = storeContext;
            this._workContext = workContext;
        }

        public void HandleEvent(CustomerRegisteredEvent eventMessage)
        {
            // todo: Generate a free order, for example

            // todo: Check if there is a corresponding email account ...
            // todo: Check if there is any invitation for that email account, and thus ... forward it

        }

        public void HandleEvent(AccountActivationSuccessEvent eventMessage)
        {
        }

        public void HandleEvent(CustomerLoggedinEvent eventMessage)
        {
            BackgroundJob.Enqueue(() => EnsureFreeServicePlanAction.Invoke(eventMessage.Customer.Id, _storeContext.CurrentStore.Id));
            // Shared user profile
            BackgroundJob.Enqueue(() => EnsureSharedUserProfileAction.Invoke(eventMessage.Customer.Id));
        }
    }
}
