﻿using Autofac;
using System;

namespace NXdrive.Web.EventHandlers.Actions
{
    public abstract class EventActionBase : IDisposable
    {
        protected readonly ILifetimeScope _lifeTimeScope;

        public EventActionBase()
        {
            var engine = Nop.Core.Infrastructure.EngineContext.Current as Nop.Core.Infrastructure.NopEngine;
            _lifeTimeScope = engine.Container.BeginLifetimeScope();
        }

        public void Dispose()
        {
            _lifeTimeScope.Dispose();
        }
    }
}
