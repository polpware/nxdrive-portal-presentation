﻿using Autofac;
using Nop.Services.Customers;
using NXdrive.NopExt.Messaging;
using NXdrive.Services.Internal.UserIdentity;
using System.Threading.Tasks;
using IOurWorkflowMessageService = NXdrive.NopExt.Messaging.IWorkflowMessageService;

namespace NXdrive.Web.EventHandlers.Actions.UserGroup
{
    public partial class NotyUpdatedMemberStateAction : EventActionBase
    {
        public static void Invoke(int memberId, int actorId, int languageId)
        {
            Task.Run(async () =>
            {
                using (var instance = new NotyUpdatedMemberStateAction())
                {
                    await instance.OnMemberStateUpdated(memberId, actorId, languageId);
                }
            });
        }

        private async Task OnMemberStateUpdated(int memberId, int customerId, int languageId)
        {
            var memberService = _lifeTimeScope.Resolve<IGroupMemberService>();
            var membership = await memberService.GetGroupMemberById(memberId);
            if (membership == null)
            {
                return;
            }

            var groupService = _lifeTimeScope.Resolve<IUserGroupService>();
            var group = await groupService.GetGroupById(membership.GroupId);
            if (group == null || group.IsDeleted)
            {
                return;
            }

            // No need to proceed if this membership is about the owner himself/herself.
            if (membership.CustomerId == group.AdminId)
            {
                return;
            }

            var customerService = _lifeTimeScope.Resolve<ICustomerService>();
            var customer = customerService.GetCustomerById(membership.CustomerId);
            var groupAdmin = customerService.GetCustomerById(group.AdminId);

            if (customer == null || groupAdmin == null)
            {
                return;
            }

            var emailService = _lifeTimeScope.Resolve<IOurWorkflowMessageService>();

            var isQuit = customerId == membership.CustomerId;
            if (isQuit)
            {
                // Noty to group admin
                emailService.SendMessageOnGroupMember(group, membership,
                  customer, // member
                  groupAdmin, // also email receiver
                  groupAdmin.Email,
                  GroupEmailTemplateID.MemberLeftNoty2Owner,
                  languageId);
            }
            else
            {
                // Noty to customer
                emailService.SendMessageOnGroupMember(group, membership,
                    customer, // member
                    customer, // also email receiver
                    customer.Email,
                    GroupEmailTemplateID.MemberRemovedNoty2Customer,
                    languageId);
            }
        }

        // todo: in the future, add more methods when memberstate is updated but not deleted
    }
}
