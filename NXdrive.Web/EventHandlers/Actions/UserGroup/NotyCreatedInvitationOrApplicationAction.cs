﻿using Autofac;
using Nop.Services.Common;
using Nop.Services.Customers;
using NXdrive.NopExt.Messaging;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Extensions;
using System;
using System.Threading.Tasks;
using IOurWorkflowMessageService = NXdrive.NopExt.Messaging.IWorkflowMessageService;

namespace NXdrive.Web.EventHandlers.Actions.UserGroup
{
    public class NotyCreatedInvitationOrApplicationAction : EventActionBase
    {
        public static void Invoke(int invitationId, int languageId)
        {
            Task.Run(async () =>
            {
                using (var instance = new NotyCreatedInvitationOrApplicationAction())
                {
                    await instance.OnInvitationOrApplicationCreated(invitationId, languageId);
                }
            });
        }

        /// <summary>
        /// Sends out an email when an invitation/application is created
        /// </summary>
        /// <param name="invitationOrApplicationId">Invitation or Application Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Task</returns>
        private async Task OnInvitationOrApplicationCreated(int invitationOrApplicationId, int languageId)
        {
            // Check if this invitation still make senses or not
            var invitationService = _lifeTimeScope.Resolve<IMemberInvitationService>();
            var invitationOrApplication = await invitationService.GetInvitationOrApplicationById(invitationOrApplicationId);

            if (invitationOrApplication == null || invitationOrApplication.IsDeleted)
            {
                return;
            }

            var groupService = _lifeTimeScope.Resolve<IUserGroupService>();
            var group = await groupService.GetGroupById(invitationOrApplication.GroupId);

            if (group == null || group.IsDeleted)
            {
                return;
            }

            var customerService = _lifeTimeScope.Resolve<ICustomerService>();
            var creator = customerService.GetCustomerById(invitationOrApplication.CreatorId);

            var emailService = _lifeTimeScope.Resolve<IOurWorkflowMessageService>();

            if (invitationOrApplication.IsAppliedByRegisteredCustomer())
            {
                // Send to owner
                var groupOwner = customerService.GetCustomerById(group.AdminId);
                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    creator, groupOwner, groupOwner.Email,
                    GroupEmailTemplateID.ApplicationCreatedNoty2Owner, languageId);

                // Send to customer
                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    creator,
                    creator,
                    creator.Email,
                    GroupEmailTemplateID.ApplicationCreatedNoty2Customer, languageId);
            }
            else if (invitationOrApplication.IsTargetRegisteredCustomer())
            {
                // In this case, a group owner or member invited a registered customer.
                // We need to send out an email to the invited customer.
                var invitedCustomer = customerService.GetCustomerById(invitationOrApplication.UserId);

                // todo: User provided message
                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    invitedCustomer, // target user
                    invitedCustomer, // also the email receiver
                    invitedCustomer.Email, GroupEmailTemplateID.InvitationCreatedNoty2Customer, languageId);
            }
            else if (invitationOrApplication.IsTargetJustEmail())
            {
                var userService = _lifeTimeScope.Resolve<IUserIdentityService>();
                var invitedEmailUser = await userService.GetEmailIdentityById(invitationOrApplication.UserId);

                var templateId = GroupEmailTemplateID.InvitationCreatedNoty2EmailUser;

                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    null, // no invitation target
                    null, // no receiver customer
                    invitedEmailUser.Email, templateId, languageId);
            }

            // Remember the date we have sent out the invitation
            var attributeService = _lifeTimeScope.Resolve<IGenericAttributeService>();
            var tyInvitation = invitationOrApplication as DataModeling.UserIdentity.Domain.UserGroupInvitation;
            attributeService.SaveAttribute(tyInvitation, Extensions.SystemAttributeExtension.GroupEmailNotificationLastSentDate, DateTime.UtcNow);
        }

    }
}
