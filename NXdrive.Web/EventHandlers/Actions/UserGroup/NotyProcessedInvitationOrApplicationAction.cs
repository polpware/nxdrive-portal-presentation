﻿using Autofac;
using Nop.Services.Common;
using Nop.Services.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.NopExt.Messaging;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Extensions;
using System.Threading.Tasks;
using IOurWorkflowMessageService = NXdrive.NopExt.Messaging.IWorkflowMessageService;

namespace NXdrive.Web.EventHandlers.Actions.UserGroup
{
    public class NotyProcessedInvitationOrApplicationAction : EventActionBase
    {
        public static void Invoke4DeniedOne(int invitationId, int languageId)
        {
            Task.Run(async () =>
            {
                using (var instance = new NotyProcessedInvitationOrApplicationAction())
                {
                    await instance.OnInvitationOrApplicationDenied(invitationId, languageId);
                }
            });
        }

        public static void Invoke4AcceptedOne(int membershipId, int languageId)
        {
            Task.Run(async () =>
            {
                using (var instance = new NotyProcessedInvitationOrApplicationAction())
                {
                    await instance.OnInvitationOrApplicationAccepted(membershipId, languageId);
                }
            });
        }

        /// <summary>
        /// Sends emails when an application is declined or an invitation is denied.
        /// </summary>
        /// <param name="invitationOrApplicationId">Invitation/Application Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Task</returns>
        private async Task OnInvitationOrApplicationDenied(int invitationOrApplicationId, int languageId)
        {
            var invitationService = _lifeTimeScope.Resolve<IMemberInvitationService>();
            var invitationOrApplication = await invitationService.GetInvitationOrApplicationById(invitationOrApplicationId);
            if (invitationOrApplication == null || invitationOrApplication.IsDeleted)
            {
                return;
            }

            var groupService = _lifeTimeScope.Resolve<IUserGroupService>();
            var group = await groupService.GetGroupById(invitationOrApplication.GroupId);
            // todo: Never happen
            if (group == null || group.IsDeleted)
            {
                return;
            }

            var customerService = _lifeTimeScope.Resolve<ICustomerService>();
            var creator = customerService.GetCustomerById(invitationOrApplication.CreatorId);

            var emailService = _lifeTimeScope.Resolve<IOurWorkflowMessageService>();

            if (invitationOrApplication.IsAppliedByRegisteredCustomer())
            {
                // Notify the applicant (the creator)
                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    creator,   // invitation target
                    creator, // email receiver
                    creator.Email,
                    invitationOrApplication.Declined ? GroupEmailTemplateID.ApplicationDeclinedNoty2Customer
                    : GroupEmailTemplateID.ApplicationApprovedNoty2Customer,
                    languageId);
            }
            else
            {
                // Invitation either by an owner or a member
                // Notify the creator of the result
                Nop.Core.Domain.Customers.Customer targetCustomer = null;
                if (invitationOrApplication.IsTargetRegisteredCustomer())
                {
                    targetCustomer = customerService.GetCustomerById(invitationOrApplication.UserId);
                }

                emailService.SendMessageOnGroupInvitation(group, invitationOrApplication, creator,
                    targetCustomer,
                    creator,
                    creator.Email,
                    invitationOrApplication.Declined ? GroupEmailTemplateID.InvitationDeniedNoty2Createor
                    : GroupEmailTemplateID.InvitationAcceptedNoty2Creator,
                    languageId);
            }
        }

        /// <summary>
        /// Sends emails when an application is approved or an invitation is accepted.
        /// </summary>
        /// <param name="membershipId">New membership Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Task</returns>
        private async Task OnInvitationOrApplicationAccepted(int membershipId, int languageId)
        {
            var memberService = _lifeTimeScope.Resolve<IGroupMemberService>();
            var membership = await memberService.GetGroupMemberById(membershipId);
            if (membership == null || membership.IsDeleted)
            {
                return;
            }

            var groupService = _lifeTimeScope.Resolve<IUserGroupService>();
            var group = await groupService.GetGroupById(membership.GroupId);
            // todo: Never happen
            if (group == null || group.IsDeleted)
            {
                return;
            }

            var customerService = _lifeTimeScope.Resolve<ICustomerService>();

            // Figure out the corresponding invitation or application id
            var originInvitationOrApplicationId =
                (membership as UserGroupMember).GetAttribute<int>(Extensions.SystemAttributeExtension.GroupMemberResultingFrom);

            IUserGroupInvitation latestInvitation = null;
            if (originInvitationOrApplicationId > 0)
            {
                var invitationService = _lifeTimeScope.Resolve<IMemberInvitationService>();
                latestInvitation =  await invitationService.GetInvitationOrApplicationById(originInvitationOrApplicationId);
            }

            if (latestInvitation == null)
            {
                return;
            }

            var newMember = customerService.GetCustomerById(membership.CustomerId);

            var emailService = _lifeTimeScope.Resolve<IOurWorkflowMessageService>();
            if (latestInvitation.IsAppliedByRegisteredCustomer())
            {
                emailService.SendMessageOnGroupMember(group, membership, newMember,
                    newMember,  // the new group member receives email
                    newMember.Email,
                    GroupEmailTemplateID.ApplicationApprovedNoty2Customer,
                    languageId);
            }
            else
            {
                // Otherwise, an invitation. Notify of the invitation owner
                var invitationCreatorId = latestInvitation.CreatorId;   // might be group admin or member
                var invitationCreator = customerService.GetCustomerById(invitationCreatorId);

                emailService.SendMessageOnGroupMember(group, membership, newMember,
                    invitationCreator,
                    invitationCreator.Email,
                    GroupEmailTemplateID.InvitationAcceptedNoty2Creator,
                    languageId);
            }
        }
    }
}
