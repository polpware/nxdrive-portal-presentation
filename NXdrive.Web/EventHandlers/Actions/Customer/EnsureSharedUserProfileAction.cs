﻿using Autofac;
using Nop.Services.Common;
using Nop.Services.Customers;
using NXdrive.NopExt.ServicePlans;
using Polpware.NopExt.SubsPlan.Orders;
using System.Threading.Tasks;

namespace NXdrive.Web.EventHandlers.Actions.Customer
{
    public class EnsureSharedUserProfileAction : EventActionBase
    {
        public static void Invoke(int customerId)
        {
            Task.Run(() =>
            {
                using (var instance = new EnsureSharedUserProfileAction())
                {
                    instance.EnsureSharedUserProfile(customerId);
                }
            });
        }

        private void EnsureSharedUserProfile(int customerId)
        {
            var customerService = _lifeTimeScope.Resolve<ICustomerService>();
            var customer = customerService.GetCustomerById(customerId);

            // Nothing to do
            var substOrientedOrderAccessService = _lifeTimeScope.Resolve<SubstOrientedOrderAccessService>();
            var order = substOrientedOrderAccessService.EffectiveOrder(customer);
            if (null == order)
            {
                return;
            }

            var orderFactorService = _lifeTimeScope.Resolve<OrderFactoryService>();
            var plan = orderFactorService.CreateBaseOrder(order);
            var specservice = _lifeTimeScope.Resolve<IPlanSpecificationService>();
            var spec = specservice.GetPlanSpecification(plan.Product);

            var genericAttributeService = _lifeTimeScope.Resolve<IGenericAttributeService>();

            // capacity
            var capacityInGig = customer.GetAttribute<int>(Extensions.SystemAttributeExtension.CapacityInGig, genericAttributeService);
            if (capacityInGig != spec.Capacity)
            {
                genericAttributeService.SaveAttribute<int>(customer, Extensions.SystemAttributeExtension.CapacityInGig, spec.Capacity);
            }

            // upload limit
            var uploadFileLimitInMB = customer.GetAttribute<int>(Extensions.SystemAttributeExtension.UploadFileLimitInMB, genericAttributeService);
            if (uploadFileLimitInMB != spec.SingleFileUpperLimit)
            {
                genericAttributeService.SaveAttribute<int>(customer, Extensions.SystemAttributeExtension.UploadFileLimitInMB, spec.SingleFileUpperLimit);
            }

            // todo: top security
        }


    }
}
