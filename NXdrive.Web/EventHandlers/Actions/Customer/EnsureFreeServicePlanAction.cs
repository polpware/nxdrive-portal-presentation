﻿using Autofac;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Polpware.NopExt.SubsPlan.Orders;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.EventHandlers.Actions.Customer
{
    using IPolpwareOrderProcessingService = Polpware.NopExt.SubsPlan.Orders.IOrderProcessingService;
    using ProcessPaymentRequest = Polpware.NopExt.SubsPlan.Payments.ProcessPaymentRequest;

    public class EnsureFreeServicePlanAction : EventActionBase
    {
        public static void Invoke(int customerId, int storeId)
        {
            Task.Run(() =>
            {
                using (var instance = new EnsureFreeServicePlanAction())
                {
                    instance.EnsureFreeServicePlan(customerId, storeId);
                }
            });
        }

        private void EnsureFreeServicePlan(int customerId, int storeId)
        {
            var customerService = _lifeTimeScope.Resolve<ICustomerService>();
            var customer = customerService.GetCustomerById(customerId);

            var genericAttributeService = _lifeTimeScope.Resolve<IGenericAttributeService>();
            // Get attributes 
            var everAssignedService = customer.GetAttribute<bool>(Extensions.SystemAttributeExtension.HasEverAssignedFreeService, genericAttributeService);
            if (everAssignedService)
            {
                return;
            }

            // TODO: If such a customer does not exist, what to do?
            var substOrientedOrderAccessService = _lifeTimeScope.Resolve<SubstOrientedOrderAccessService>();
            if (null != substOrientedOrderAccessService.EffectiveOrder(customer))
            {
                return;
            }

            // *******************************
            // Find the free service and push it into the shopping cart
            // *******************************
            var productService = _lifeTimeScope.Resolve<IProductService>();
            // FIXME: We hardcode the criteria as the product price to be 0.
            var product = productService.GetAllProductsDisplayedOnHomePage()
                .FirstOrDefault(x => x.Price == Decimal.Zero);

            // TODO: If such a product does not exist, what to do?

            // Clean up the shopping cart for this customer
            var shoppingCartServce = _lifeTimeScope.Resolve<IShoppingCartService>();
            shoppingCartServce.DeleteExpiredShoppingCartItems(DateTime.Now);

            // Add one item
            shoppingCartServce.AddToCart(customer, product, Nop.Core.Domain.Orders.ShoppingCartType.ShoppingCart, storeId);

            // Make a payment request to proceed to check out, payment, and ordering systems
            var processPaymentRequest = new ProcessPaymentRequest();

            processPaymentRequest.StoreId = storeId;
            processPaymentRequest.CustomerId = customer.Id;

            // Place order
            var orderProcessingService = _lifeTimeScope.Resolve<IPolpwareOrderProcessingService>();
            var placeOrderResult = orderProcessingService.PlaceOrderWrapper(processPaymentRequest);

            // TODO: What if placing such an order fails??

            // Persistent...
            genericAttributeService.SaveAttribute<bool>(customer, Extensions.SystemAttributeExtension.HasEverAssignedFreeService, true);
        }

    }
}
