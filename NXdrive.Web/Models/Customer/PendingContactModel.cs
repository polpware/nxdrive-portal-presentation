﻿using System.Collections.Generic;

namespace NXdrive.Web.Models.Customer
{
    public class PendingContactModel
    {
        public string Email { get; set; }
    }
}
