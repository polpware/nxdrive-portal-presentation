﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Models.Customer
{
    public class NotificationModels 
    {
        public List<NotificationModel> inboxNotifications { get; set; }

        public List<NotificationModel> sentNotifications { get; set; }

        public NotificationModels()
        {
            inboxNotifications = new List<NotificationModel>();
            sentNotifications = new List<NotificationModel>();
        }
    }


}
