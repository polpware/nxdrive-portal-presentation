﻿namespace NXdrive.Web.Models.Customer
{
    public class CustomerAvatarModel : Polpware.NopWeb.Models.Customer.CustomerAvatarModel
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
    }
}
