﻿using Nop.Web.Framework.Models;

namespace NXdrive.Web.Models.Customer
{
    public class StorageUsagePerUserModel : BaseNopModel
    {
        public string CurrentServicePlan { get; set; }

        public long TotalCapacity { get; set; }
        public int UsedRation { get; set; }   // percentage
        public int FileCount { get; set; }
        public long RemainingSize { get; set; }
        public int ImageCount { get; set; }
        public int DocumentCount { get; set; }
        public long TrashSize { get; set; }
        public int TrashCount { get; set; }

    }
}
