﻿using FluentValidation.Attributes;
using NXdrive.Web.Validators.Customer;
using System;
using System.Collections.Generic;

namespace NXdrive.Web.Models.Customer
{
    [Validator(typeof(PreSignValidator))]
    public class PreSignModel {

        public PreSignModel()
        {
            Errors = new List<Tuple<string, string,string>>();
        }

        public string NextStep { get; set; }
        public string Email { get; set; }
        public string ReturnUrl { get; set; }

        public bool DisplayCaptcha { get; set; }

        /// <summary>
        /// item1: error reason,
        /// item2: text on the html element which user click on the route to item3
        /// item3: suggested action url
        /// </summary>
        public List<Tuple<string, string, string>> Errors { get; private set; }
    }
}
