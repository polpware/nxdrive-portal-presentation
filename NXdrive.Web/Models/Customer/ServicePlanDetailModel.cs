﻿using Nop.Web.Framework.Models;
using Polpware.NopWeb.Models.Catalog;
using System.Collections.Generic;

namespace NXdrive.Web.Models.Customer
{
    public class ServicePlanDetailModel : ProductOverviewModel
    {
        // public string Name { get; set; }
        public int SingleFileUpperLimit { get; set; }
        public bool IsTwoLayerEncryption { get; set; }
        public int RelibilityIndicatorLower { get; set; }
        public int RelibilityIndicatorUpper { get; set; }
        public int SecurityIndicatorNLower { get; set; }
        public int SecurityIndicatorNUppper { get; set; }
        public int SecurityIndicatorOther { get; set; }
        public short PrivacyTrustworthinessId { get; set; }
        public bool IsFileShareAllowed { get; set; }
        public bool IsVersionControlEnabled { get; set; }
        public bool IsAdvertisementAllowed { get; set; }
        public bool IsFileSynchroizationEnabled { get; set; }
        public ushort BandwidthUpperLimit { get; set; }
        public int Capacity { get; set; }
        public string ServiceGroup { get; set; }
        public bool ServiceGroupRepresentative { get; set; }

        public bool IsEnrolled { get; set; }    // if customer is enrolled in this plan

        public bool IsUpgradeOption { get; set; }   // if not enrolled, is this plan an upgrade option to current plan
        // public List<string> Hightlights { get; set; }   // features that we want to empathize
    }

}
