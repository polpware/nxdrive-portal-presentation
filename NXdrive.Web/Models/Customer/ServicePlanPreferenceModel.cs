﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Models.Customer
{
    public class ServicePlanPreferenceModel : BaseNopModel
    {
        public ServicePlanPreferenceModel()
        {
            AvailableReliabilityAndSecurityPairs = new List<SelectListItem>();

            AvailableStorageCodings = new List<SelectListItem>();

            // TODO: Remove the code below into controller
            //AvailableStorageCodings.Add(new SelectListItem()
            //    {
            //        Text = "Jerasure",
            //        Value = ((int)StorageCodingType.JerasureCoding).ToString()
            //    });
            //AvailableStorageCodings.Add(new SelectListItem()
            //{
            //    Text = "Reed",
            //    Value = ((int)StorageCodingType.ReedSolomonCoding).ToString()
            //});

        }

        public string StorageCodingTypePrettyName
        {
            get
            {
                var item = AvailableStorageCodings.Where(x => int.Parse(x.Value) == StorageCodingTypeId).FirstOrDefault();
                return item == null ? String.Empty : item.Text;
            }
        }

        public string ReliabilityAndSecurityPairPrettyName
        {
            get
            {
                var item = AvailableReliabilityAndSecurityPairs.Where(x => int.Parse(x.Value) == ReliabilityAndSecurityPairId).FirstOrDefault();
                return item == null ? String.Empty : item.Text;
            }

        }

        [NopResourceDisplayName("Account.Fields.StorageCoding")]
        public int StorageCodingTypeId { get; set; }
        public IList<SelectListItem> AvailableStorageCodings { get; set; }

        [NopResourceDisplayName("Account.Fields.RealibilityAndSecurityIndex")]
        public int ReliabilityAndSecurityPairId { get; set; }
        public IList<SelectListItem> AvailableReliabilityAndSecurityPairs { get; set; }

        [NopResourceDisplayName("Account.Fields.IsTopSecurity")]
        public bool IsTopSecurity { get; set; }

    }

}
