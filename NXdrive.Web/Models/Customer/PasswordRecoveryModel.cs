﻿using FluentValidation.Attributes;
using Polpware.NopWeb.Validators.Customer;

namespace NXdrive.Web.Models.Customer
{
    using BaseModel = Polpware.NopWeb.Models.Customer.PasswordRecoveryModel;

    [Validator(typeof(PasswordRecoveryValidator))]
    public class PasswordRecoveryModel : BaseModel
    {
        public bool Success { get; set; }

        public PasswordRecoveryModel() : base() { }
        public PasswordRecoveryModel(BaseModel source) : base()
        {
            this.Email = source.Email;
            this.Result = source.Result;
        }
    }
}
