﻿using Nop.Web.Framework.Models;
using System;

namespace NXdrive.Web.Models.Customer
{
    public partial class CustomerServicePlanModel : BaseNopModel
    {
        public CustomerServicePlanModel()
        {
            //TODO: delete preference model, not used here any more
            PlanPreferenceModel = new ServicePlanPreferenceModel();
            UsageModel = new StorageUsagePerUserModel();
            CurrentPlanDetailModel = new ServicePlanDetailModel();
            NextPlanDetailModel = new ServicePlanDetailModel();
        }

        public String CreatedOn { get; set; }

        public bool IsCancelled { get; set; }

        public String EndOn { get; set; }

        public ServicePlanDetailModel NextPlanDetailModel { get; set; } 
        public ServicePlanDetailModel CurrentPlanDetailModel { get; set; }
        public ServicePlanPreferenceModel PlanPreferenceModel { get; set; }
        public StorageUsagePerUserModel UsageModel { get; set; }

        public enum StorageCodingType : int
        {
            JerasureCoding = 0,
            ReedSolomonCoding = 1
        }

        public class ReliabilityAndSecurityPair
        {
            public int Id { get; set; }
            public int nParam { get; set; }
            public int kParam { get; set; }
        }
    }
}
