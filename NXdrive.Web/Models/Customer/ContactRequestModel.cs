﻿using System.Collections.Generic;

namespace NXdrive.Web.Models.Customer
{
    public class ContactRequestModel
    {
        public string PersonalMessage { get; set; }
        public string[] Emails { get; set; }
    }
}
