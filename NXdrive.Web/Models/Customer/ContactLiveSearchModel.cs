﻿namespace NXdrive.Web.Models.Customer
{
    /// <summary>
    ///  Note that we intentionally use lower case, as
    ///  this model is desinged for working with JS.
    /// </summary>
    public class ContactLiveSearchModel
    {

        public ContactLiveSearchModel()
        {
            pageNumber = 0;
            pageSize = 15;
        }

        public string name { get; set; }
        public bool onlyUser { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
    }
}
