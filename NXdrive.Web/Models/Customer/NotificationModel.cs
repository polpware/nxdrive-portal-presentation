﻿using Nop.Core.Domain.Forums;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Models.Customer
{
    public class NotificationModel : PrivateMessage
    {
        public int priority { get; set; }

        public string actionUrl { get; set; }

        public string category { get; set; }

        public NotificationModel(PrivateMessage privateMessage)
        {
            StoreId = privateMessage.StoreId;
            FromCustomerId = privateMessage.FromCustomerId;
            ToCustomerId = privateMessage.ToCustomerId;
            Subject = privateMessage.Subject;
            Text = privateMessage.Text;
            IsRead = privateMessage.IsRead;
            IsDeletedByAuthor = privateMessage.IsDeletedByAuthor;
            IsDeletedByRecipient = privateMessage.IsDeletedByRecipient;
            CreatedOnUtc = privateMessage.CreatedOnUtc;
            FromCustomer = privateMessage.FromCustomer;
            ToCustomer = privateMessage.ToCustomer;
        }
    }
}
