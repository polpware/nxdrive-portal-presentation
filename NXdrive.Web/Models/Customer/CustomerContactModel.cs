﻿using System;

namespace NXdrive.Web.Models.Customer
{
    public class CustomerContactModel
    {
        public CustomerContactModel()
        {
            AvatarURL = string.Empty;
            Abbr = string.Empty;
            Comment = string.Empty;
        }

        public bool IsNXdriveUser { get; set; }   // indicate whether this is nxdrive customer or not

        public string Source { get; set; }          // might be deleted in the future

        public string FullName { get; set; }    // might be deleted in the future

        public string Email { get; set; }

        public string Abbr { get; set; }  // computed from email

        public string AvatarURL { get; set; }

        public DateTime CreatedDate { get; set; }

        public int IdentityId { get; set; }    // email identity that the this contact points to

        public int ConnectionId { get; set; }

        public int CustomerId { get; set; }

        public string Comment { get; set; }
    }
}
