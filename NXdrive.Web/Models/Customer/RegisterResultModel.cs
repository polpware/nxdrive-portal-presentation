﻿namespace NXdrive.Web.Models.Customer
{
    using BaseModel = Polpware.NopWeb.Models.Customer.RegisterResultModel;
    public class RegisterResultModel : BaseModel
    {
        public string Email { get; private set; }

        public RegisterResultModel(string email, string result) : base()
        {
            this.Email = email;
            this.Result = result;
        }

        public static RegisterResultModel Build(BaseModel source, string email)
        {
            return new RegisterResultModel(email, source.Result);
        }
    }
}
