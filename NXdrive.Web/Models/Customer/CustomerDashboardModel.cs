﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Models.Customer
{
    public class CustomerDashboardModel 
    {
        public string groupSummary { get; set; }
        public string fileSystemSummary { get; set; }

        public bool actionNeeded { get; set; }
        public ServicePlanDetailModel servicePlan { get; set; }

        public StorageUsagePerUserModel UsageModel { get; set; }
    }
}
