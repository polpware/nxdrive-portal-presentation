﻿using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Models.Customer
{
    public class TransactionModel : Order
    {      
        public string PlanName { get; set; }

        public bool MonthSubscription { get; set; }

        public string PaymentMethod { get; set; }

        [Obsolete("")]
        public TransactionModel(Order order)
        {
            Customer = order.Customer;
            BillingAddress = order.BillingAddress;
            ShippingAddress = order.ShippingAddress;
            PickupAddress = order.PickupAddress;
            RedeemedRewardPointsEntry = order.RedeemedRewardPointsEntry;
            DiscountUsageHistory = order.DiscountUsageHistory;
            GiftCardUsageHistory = order.GiftCardUsageHistory;
            OrderNotes = order.OrderNotes;
            OrderItems = order.OrderItems;
            Shipments = order.Shipments;
            OrderStatus = order.OrderStatus;
            PaymentStatus = order.PaymentStatus;
            ShippingStatus = order.ShippingStatus;
            CustomerTaxDisplayType = order.CustomerTaxDisplayType;

            OrderGuid = order.OrderGuid;
            StoreId = order.StoreId;
            CustomerId = order.CustomerId;
            BillingAddressId = order.BillingAddressId;
            ShippingAddressId = order.ShippingAddressId;
            PickupAddressId = order.PickupAddressId;
            PickUpInStore = order.PickUpInStore;
            OrderStatusId = order.OrderStatusId;
            ShippingStatusId = order.ShippingStatusId;
            PaymentStatusId = order.PaymentStatusId;
            PaymentMethodSystemName = order.PaymentMethodSystemName;
            CustomerCurrencyCode = order.CustomerCurrencyCode;
            CurrencyRate = order.CurrencyRate;
            CustomerTaxDisplayTypeId = order.CustomerTaxDisplayTypeId;
            VatNumber = order.VatNumber;
            OrderSubtotalInclTax = order.OrderSubtotalInclTax;
            OrderSubtotalExclTax = order.OrderSubtotalExclTax;
            OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountInclTax;
            OrderSubTotalDiscountExclTax = order.OrderSubTotalDiscountExclTax;
            OrderShippingInclTax = order.OrderShippingInclTax;
            OrderShippingExclTax = order.OrderShippingExclTax;
            PaymentMethodAdditionalFeeInclTax = order.PaymentMethodAdditionalFeeInclTax;
            PaymentMethodAdditionalFeeExclTax = order.PaymentMethodAdditionalFeeExclTax;
            TaxRates = order.TaxRates;
            OrderTax = order.OrderTax;
            OrderDiscount = order.OrderDiscount;
            OrderTotal = order.OrderTotal;
            RefundedAmount = order.RefundedAmount;
            RewardPointsHistoryEntryId = order.RewardPointsHistoryEntryId;
            CheckoutAttributeDescription = order.CheckoutAttributeDescription;
            CheckoutAttributesXml = order.CheckoutAttributesXml;
            CustomerLanguageId = order.CustomerLanguageId;
            AffiliateId = order.AffiliateId;
            CustomerIp = order.CustomerIp;
            AllowStoringCreditCardNumber = order.AllowStoringCreditCardNumber;
            
            MaskedCreditCardNumber = order.MaskedCreditCardNumber;

        }
    }
}
