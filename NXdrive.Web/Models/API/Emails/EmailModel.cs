﻿namespace NXdrive.Web.Models.API.Emails
{
    public class EmailModel
    {
        public int EmailTypeId { get; set; }
        public EmailTypeEnum EmailType
        {
            get
            {
                return (EmailTypeEnum)EmailTypeId;
            }
        }
        public string Title { get; set; }
        private string _receiver;
        public string Receiver
        {
            get
            {
                return _receiver;
            }
            set
            {
                _receiver = value;
                ReceiverAsList = GetAsList(value);
            }
        }
        private string _carbon;
        public string Carbon
        {
            get
            {
                return _carbon;
            }
            set
            {
                _carbon = value;
                CarbonAsList = GetAsList(value);
            }
        }
        private string _blindCarbon;
        public string BlindCarbon
        {
            get
            {
                return _blindCarbon;
            }

            set
            {
                _blindCarbon = value;
                BlindCarbonAsList = GetAsList(value);
            }
        }
        public string Message { get; set; }
        public string[] ReceiverAsList { get; set; }
        public string[] CarbonAsList { get; set; }
        public string[] BlindCarbonAsList { get; set; }
        private static string[] GetAsList(string str)
        {
            if (str == null)
            {
                return new string[0];
            }
            return str.Split(',');
        }
    }
}
