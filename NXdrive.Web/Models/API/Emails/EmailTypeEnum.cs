﻿namespace NXdrive.Web.Models.API.Emails
{
    public enum EmailTypeEnum
    {
        SendShareLink = 1,
        GroupEmail = 2
    }
}
