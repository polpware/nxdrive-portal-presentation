﻿namespace NXdrive.Web.Models.API.Emails
{
    public class EmailShareLinkModel : EmailModel
    {
        public string ShareLinkURL { get; set; }
        public string ItemName { get; set; }
        public bool IsFolder { get; set; }
    }
}
