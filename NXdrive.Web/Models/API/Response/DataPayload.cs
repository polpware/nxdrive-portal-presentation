﻿using Newtonsoft.Json;

namespace NXdrive.Web.Models.API.Response
{
    public class DataPayload<T> : StatusReport where T : class
    {
        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        public T Data { get; private set; }

        public void SetData(T data)
        {
            this.Ok = true;
            this.Data = data;
        }
    }
}
