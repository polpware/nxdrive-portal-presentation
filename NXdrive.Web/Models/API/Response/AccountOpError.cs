﻿using Newtonsoft.Json;

namespace NXdrive.Web.Models.API.Response
{
    public class AccountOpError
    {
        [JsonProperty("code")]
        public CodeEnum Code { get; set; }
        [JsonProperty("message")]
        public string Msg { get; set; }

        public AccountOpError(CodeEnum code, string msg = "")
        {
            this.Code = code;
            this.Msg = msg;
        }


        /// <summary>
        /// This code represents the benign result.
        /// </summary>
        public enum CodeEnum
        {
            // Default
            Default = 0,

            // Registration
            Inactive = 1,
            Registered = 2,

            // Password Recovery
            EmailNotFound = 10
        }
    }
}
