﻿using Newtonsoft.Json;

namespace NXdrive.Web.Models.API.Response
{
    public class GeneralError
    {
        [JsonProperty("code")]
        public CodeEnum Code { get; set; }
        [JsonProperty("message")]
        public string Msg { get; set; }

        public GeneralError(CodeEnum code, string msg)
        {
            this.Code = code;
            this.Msg = msg;
        }

        /// <summary>
        /// This code represents the action failure result.
        /// </summary>
        public enum CodeEnum
        {
            Unknown = 1,
            InvalidModel = 2,
            RepoInsertErr = 3,
            RepoDeleteErr = 4
        }
    }
}
