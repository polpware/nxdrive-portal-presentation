﻿using Newtonsoft.Json;

namespace NXdrive.Web.Models.API.Response
{
    public class StatusReport
    {
        [JsonProperty("version")]
        public int Version { get; set; }
        [JsonProperty("isGoodState")]
        public bool Ok { get; protected set; }

        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public GeneralError Error { get; protected set; }

        public void SetError(GeneralError.CodeEnum code, string msg)
        {
            this.Ok = false;
            Error = new GeneralError(code, msg);
        }

        public void SetOk()
        {
            this.Ok = true;
            Error = null;
        }
    }
}
