﻿using Nop.Web.Framework.Models;

namespace NXdrive.Web.Models.UserGroup
{
    public class MemberPrivateMessageModel : BaseNopModel
    {
        public int MemberId { get; set; }

        public string Subject { get; set; }

        public string Text { get; set; }

    }
}