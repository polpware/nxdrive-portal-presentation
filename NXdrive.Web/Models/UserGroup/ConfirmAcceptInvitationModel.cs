﻿using Nop.Web.Framework.Models;
using System;

namespace NXdrive.Web.Models.UserGroup
{
    public class ConfirmAcceptInvitationModel : BaseNopEntityModel
    {
        public GroupModel Group { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatorName { get; set; }
        public string CreatorId { get; set; }
        public string CreatorEmail { get; set; }
    }
}
