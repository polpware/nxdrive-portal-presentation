﻿using Polpware.NopWeb.Models.Customer;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupAvatarModel : CustomerAvatarModel
    {
        public int GroupId { get; } 

        public GroupAvatarModel(int groupId)
        {
            GroupId = groupId;
        }
    }
}