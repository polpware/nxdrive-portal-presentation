﻿using System.Collections.Generic;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupNewInvitationModel : GroupModel
    {
        /// <summary>
        /// model for user to create a new invitation page
        /// provide enough information to user, including current members and existing invitations
        /// </summary>
        public GroupNewInvitationModel()
        {
            Members = new List<MemberModel>();
            Invitations = new List<InvitationModel>();
        }

        public bool GroupFull { get; set; }

        public IList<MemberModel> Members { get; set; } // existing members

        public IList<InvitationModel> Invitations { get; set; } // existing invitations
    }
}