﻿using Nop.Web.Framework.Models;

namespace NXdrive.Web.Models.UserGroup
{
    public class InvitePeopleIntoGroupModel : BaseNopModel
    {
        public int GroupId { get; set; }
        public string[] EmailList { get; set; }
        public string Content { get; set; }
    }
}
