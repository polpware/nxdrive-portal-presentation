﻿using System.Collections.Generic;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupMemberListModel : GroupModel
    {
        public GroupMemberListModel()
        {
            Members = new List<MemberModel>();
            Invitations = new List<InvitationModel>();
            Applications = new List<InvitationModel>();
        }

        public bool GroupFull { get; set; }

        public IList<MemberModel> Members { get; set; }  // list of real members

        public IList<InvitationModel> Invitations { get; set; }  // list of invitations, either from owner or from other members

        public IList<InvitationModel> Applications { get; set; }    // list of applications from non-members
    }
}