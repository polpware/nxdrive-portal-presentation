﻿using Nop.Web.Framework.Models;
using NXdrive.DataModeling.Internal.UserIdentity;
using System;

namespace NXdrive.Web.Models.UserGroup
{
    public class MemberModel : BaseNopEntityModel
    {
        public DateTime CreatedOn { get; set; }
        public int CustomerId { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerName {get;set;}
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int GroupId { get; set; }
        public bool AllowDelete { get; set; }
        public bool IsAboutMe { get; set; }
    }
}