﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NXdrive.Web.Models.UserGroup
{
    public class UnregisteredMemberListModel : BaseNopEntityModel
    {

        public string email { get; set; }   // email of the unregistered user

        public string ownerName { get; set;  }  // group's owner name

        public string ownerEmail { get; set; }  // owner's email

        public int groupId { get; set; }    // group id

        public string groupName { get; set; }   // group name

        public UnregisteredMemberListModel()
        {
            // empty list 
            Members = new List<MemberModel>();
        }

        public IList<MemberModel> Members { get; set; }
    }
}