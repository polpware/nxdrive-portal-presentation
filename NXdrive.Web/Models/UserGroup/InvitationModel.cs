﻿using Nop.Web.Framework.Models;
using System;

namespace NXdrive.Web.Models.UserGroup
{
    public class InvitationModel : BaseNopEntityModel
    {
        public int GroupId { get; set; }
        public int OwnerId { get; set; }

        public bool Agree { get; set; }

        public InvitationType Type { get; set; }  // this field depends on creatorId, groupOwner and UserId

        public int CreatorId { get; set; }

        public string CreatorName { get; set; } 

        public string CreatorEmail { get; set; }

        public short ProviderId { get; set; }

        public int UserId { get; set; }

        public string UserEmail { get; set; }

        public string UserDisplayName { get; set; }

        public bool Declined { get; set; }  // an application that has been declined by owner

        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ExpiredOn { get; set; }

        public bool AllowAcceptOrDeny { get; set; } // if true, allow current user to accept or deny this invitation/application

        public bool AllowDelete { get; set; }   // if true, allow current user to delete this invitation/application

        public string GroupName { get; set; }

        public int AvatarImageId { get; set; }  // obsolete

        public string AvatarUrl { get; set; }

    }

    public enum InvitationType
    {
        GroupOwnerInvitation,
        GroupMemberInvitation,
        NonMemberApplicant
    }
}
