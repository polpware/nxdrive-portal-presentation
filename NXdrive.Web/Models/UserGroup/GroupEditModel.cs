﻿using Nop.Web.Framework.Models;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupEditModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}
