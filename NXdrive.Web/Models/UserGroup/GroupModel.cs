﻿using Nop.Web.Framework.Models;
using System;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupModel : BaseNopEntityModel
    {
        // Mapping from persistence table
        public DateTime CreatedOn { get; set; }

        public int CreatorId { get; set; }

        public bool IsDeleted { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }    // for the timebeing, used to descript who is in the group, later on should let user define this field

        public bool IsPublic { get; set; }    // whether this group can be found by other people's search

        public bool IsFull { get; set; }

        public string AvatarUrl { get; set; }

        public int MemberCount { get; set; }

        public int ImpendingMemberCount { get; set; }

        // The following are user-specific information.
        public int AssociationId { get; set; }

        // the followings are extensions
        public bool IsAdmin { get; set; }   // administrator is also a member, exclusive to IsInvited

        public bool IsMember { get; set; }  // member might not be admin, exclusive to IsInvited

        public bool IsInvited { get; set; } // IsInvited field is exclusive to IsAdmin, IsMember and IsApplied

        public bool IsApplied { get; set; } // IsApplied field is exclusive to IsAdmin, IsMember and IsInvited


        public bool NewGroup { get; set; }

    }
}