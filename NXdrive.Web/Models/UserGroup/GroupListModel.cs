﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace NXdrive.Web.Models.UserGroup
{
    public class GroupListModel : BaseNopModel
    {
        public GroupListModel()
        {
            JoinedGroups = new List<GroupModel>();
            InvitedGroups = new List<InvitationModel>();
            OwnedGroups = new List<GroupModel>();
        }

        public IList<GroupModel> JoinedGroups { get; set; }

        public IList<InvitationModel> InvitedGroups { get; set; }

        public IList<GroupModel> OwnedGroups { get; set; }
    }
}