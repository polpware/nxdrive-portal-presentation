﻿using System;
using System.Collections.Generic;

namespace NXdrive.Web.Models.Shared
{
    public class AlertBoxModel : Nop.Web.Framework.Models.BaseNopModel
    {
        public AlertBoxModel()
        {
            ActionList = new List<Tuple<string, string>>();
        }

        public AlertBoxModel(IEnumerable<Tuple<string, string>> actions)
        {
            ActionList = actions;
        }

        public string Title { get; set; }

        public AlertTypeEnum AlertType { get; set; }

        public string Opening { get; set; }
        public string Message { get; set; }

        public IEnumerable<Tuple<string, string>> ActionList { get; set; }

        public enum AlertTypeEnum
        {
            Success = 1,
            Info = 2,
            Warning = 3,
            Error = 4
        }
    }
}
