using DotNetOpenAuth.OAuth2.Messages;

namespace NXdrive.Web.Models.OAuth
{
    public class AccountAuthorizeModel
    {
        public string ClientApp { get; set; }

        public string Scope { get; set; }

        public EndUserAuthorizationRequest AuthorizationRequest { get; set; }

        public string YesUrl { get; set; }
        public string NoUrl { get; set; }
    }
}