﻿using Nop.Core.Domain.Customers;

namespace NXdrive.Web.CustomEvents.Customers
{
    public class AccountActivationSuccessEvent
    {
        public Customer Entity { get; private set; }
        public AccountActivationSuccessEvent(Customer customer)
        {
            Entity = customer;
        }
    }
}
