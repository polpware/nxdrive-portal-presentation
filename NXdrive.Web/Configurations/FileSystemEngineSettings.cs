﻿namespace NXdrive.Web.Configurations
{
    public class FileSystemEngineSettings
    {
        public string Host { get; set; }
        public string WebAppPath { get; set; }
    }
}
