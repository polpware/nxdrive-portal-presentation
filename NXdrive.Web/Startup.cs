﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Web.Framework.Infrastructure.Extensions;

using Hangfire;

namespace Nop.Web
{
    /// <summary>
    /// Represents startup class of application
    /// </summary>
    public class Startup
    {
        #region Properties

        /// <summary>
        /// Get configuration root of the application
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        #endregion

        #region Ctor

        public Startup(IHostingEnvironment environment)
        {

            //create configuration
            Configuration = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        #endregion

        /// <summary>
        /// Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Enable Cors
#if DEBUG
            services.AddCors(options =>
            {
                options.AddPolicy("AllowLocalHost",
                    builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });
#endif

            // Hangfire setup
            var provider = services.BuildServiceProvider();
            var hostingEnvironment = provider.GetRequiredService<IHostingEnvironment>();
            var fileProvider = new Core.Infrastructure.NopFileProvider(hostingEnvironment);

            var hangfirePath = fileProvider.MapPath("~/App_Data/hangfireSettings.json");
            var hangfireSettings = Core.Data.DataSettingsManager.LoadSettings(hangfirePath, true, fileProvider);
            services.AddHangfire(x => x.UseSqlServerStorage(hangfireSettings.DataConnectionString));

            // Configure settings
            services.ConfigureStartupConfig<NXdrive.Web.Configurations.FileSystemEngineSettings>(Configuration.GetSection("FileSystemEngine"));

            var ret = services.ConfigureApplicationServices(Configuration);
            return ret;
        }

        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseHangfireServer();

            application.ConfigureRequestPipeline();
        }
    }
}
