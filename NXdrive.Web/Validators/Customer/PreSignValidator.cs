﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using NXdrive.Web.Models.Customer;

namespace NXdrive.Web.Validators.Customer
{
    public class PreSignValidator : BaseNopValidator<PreSignModel>
    {
        public PreSignValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
        }
    }
}
