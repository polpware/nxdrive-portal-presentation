﻿using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Services.Affiliates;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Extensions
{
    public class SendEmailService : Nop.Services.Messages.WorkflowMessageService
    {
        private  readonly IStoreContext _storeContext;
        private readonly ILanguageService _languageService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IEventPublisher _eventPublisher;

        public SendEmailService(IMessageTemplateService messageTemplateService,
            IQueuedEmailService queuedEmailService,
            ILanguageService languageService, ITokenizer tokenizer,
            IEmailAccountService emailAccountService, IMessageTokenProvider messageTokenProvider,
            IStoreService storeService, IStoreContext storeContext, CommonSettings commonSettings,
            EmailAccountSettings emailAccountSettings, IEventPublisher eventPublisher, IAffiliateService affiliateService)
            : base(messageTemplateService, queuedEmailService, languageService, tokenizer, emailAccountService,
                  messageTokenProvider, storeService, storeContext, commonSettings, emailAccountSettings, eventPublisher, affiliateService)
        {
            _languageService = languageService;
            _storeContext = storeContext;
            _messageTokenProvider = messageTokenProvider;
            _eventPublisher = eventPublisher;
        }

        /// <summary>
        /// invite a visitor to become a nxdrive user
        /// </summary>
        /// <param name="customer">user who send out the invitation</param>
        /// <param name="friendEmail">receiver's email</param>
        /// <param name="token">token for later use</param>
        /// <param name="personalMessage">personal message that will show up in the email</param>
        /// <param name="emailTemplateId">email template id</param>
        /// <param name="languageId">language id</param>
        /// <returns></returns>
        public IList<int> SendFriendInvitation(Customer customer, string friendEmail, string token,
            string personalMessage,
            string emailTemplateId, int languageId)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var store = _storeContext.CurrentStore;

            var messageTemplates = GetActiveMessageTemplates(emailTemplateId, store.Id);
            if (!messageTemplates.Any())
                return new List<int>();

            //tokens
            var commonTokens = new List<Token>();
            _messageTokenProvider.AddCustomerTokens(commonTokens, customer);
            commonTokens.Add(new Token("EmailAFriend.PersonalMessage", personalMessage, true));
            commonTokens.Add(new Token("EmailAFriend.Token", token, true));
            commonTokens.Add(new Token("EmailAFriend.ReceiverEmail", friendEmail, true));

            return messageTemplates.Select(messageTemplate =>
            {
                //email account
                var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

                var tokens = new List<Token>(commonTokens);
                _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);

                //event notification
                _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

                return SendNotification(messageTemplate, emailAccount, languageId, tokens, friendEmail, string.Empty);
            }).ToList();
        }

        
        public IList<int> SendSharelink2Visitor(Customer customer, string visitorEmail, string sharelinkURL, 
            int languageId, string itemName, bool isFolder, string personalMessage = "")
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));
            string itemType = "file";
            if (isFolder)
                itemType = "folder";

            var store = _storeContext.CurrentStore;
            var messageTemplates = GetActiveMessageTemplates(MessageTemplateIDs.ShareLinkEmailNonNXdriveUser, store.Id);
            if (!messageTemplates.Any())
                return new List<int>(); // todo: throw a exception indicate template not found

            var commonTokens = new List<Token>();
            _messageTokenProvider.AddCustomerTokens(commonTokens, customer);

            // add sharelink specific tokens
            commonTokens.Add(new Token("EmailAFriend.PersonalMessage", personalMessage, true));
            commonTokens.Add(new Token("EmailAFriend.ReceiverEmail", visitorEmail, true));
            commonTokens.Add(new Token("ShareLink.URL", sharelinkURL, true));
            commonTokens.Add(new Token("ShareLink.ItemType", itemType, true));
            commonTokens.Add(new Token("ShareLink.ItemName", itemName, true));

            return messageTemplates.Select(messageTemplate =>
            {
                //email account
                var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

                var tokens = new List<Token>(commonTokens);
                _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);

                //event notification
                _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

                return SendNotification(messageTemplate, emailAccount, languageId, tokens, visitorEmail, string.Empty);
            }).ToList();
        }

        public IList<int> SendSharelink2User(Customer customer, Customer friend, string sharelinkURL,
            int languageId, string itemName, bool isFolder, string personalMessage = "")
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));
            string itemType = "file";
            if (isFolder)
                itemType = "folder";

            var store = _storeContext.CurrentStore;
            var messageTemplates = GetActiveMessageTemplates(MessageTemplateIDs.ShareLinkEmailFriend, store.Id);
            if (!messageTemplates.Any())
                return new List<int>(); // todo: throw a exception indicate template not found

            var commonTokens = new List<Token>();
            _messageTokenProvider.AddCustomerTokens(commonTokens, customer);

            // add sharelink specific tokens
            commonTokens.Add(new Token("EmailAFriend.PersonalMessage", personalMessage, true));
            commonTokens.Add(new Token("EmailAFriend.ReceiverEmail", friend.Email, true));
            commonTokens.Add(new Token("ShareLink.URL", sharelinkURL, true));
            commonTokens.Add(new Token("ShareLink.ItemType", itemType, true));
            commonTokens.Add(new Token("ShareLink.ItemName", itemName, true));
            commonTokens.Add(new Token("Receiver.Name", friend.GetFullName(), true));

            return messageTemplates.Select(messageTemplate =>
            {
                //email account
                var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

                var tokens = new List<Token>(commonTokens);
                _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);

                //event notification
                _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

                return SendNotification(messageTemplate, emailAccount, languageId, tokens, friend.Email, string.Empty);
            }).ToList();
        }
    }
}
