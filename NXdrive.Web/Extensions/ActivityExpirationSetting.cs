﻿using System;

namespace NXdrive.Web.Extensions
{
    public static class ActivityExpirationSetting
    {
        public static bool IsExpired(this DateTime createdOnUtc, TimeSpan span)
        {
            return DateTime.UtcNow - createdOnUtc > span;
        }

        public static TimeSpan OneHourPeriod => new TimeSpan(0, 1, 0, 0);
        public static TimeSpan OneDayPeriod => new TimeSpan(1, 0, 0, 0);
        public static TimeSpan OneMonthPeriod => new TimeSpan(30, 0, 0, 0);
       
    }
}
