﻿namespace NXdrive.Web.Extensions
{
    static class MessageTemplateIDs
    {
        //Service setting
        public static string CustomerNetworkFriendInvitation => "Customer.Network.Friend.Invitation";

        public static string CustomerReferNXdrive2Friend => "Customer.Network.Friend.Refer";

        public static string ShareLinkEmailFriend => "ShareLink.EmailAFriend";

        public static string ShareLinkEmailNonNXdriveUser => "ShareLink.EmailNonNXdriveUser";
    }
}
