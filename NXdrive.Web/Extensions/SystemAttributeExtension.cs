﻿namespace NXdrive.Web.Extensions
{
    static class SystemAttributeExtension
    {
        //Service setting
        public static string HasEverAssignedFreeService { get { return "HasEverAssignedFreeService"; } }
        public static string HasEverLoggedBefore { get { return "HasEverLoggedBefore"; } }

        // Per-plan settings
        public static string IsTopSecurityChosed { get { return "IsTopSecurityChosed"; } }
        public static string CapacityInGig { get { return "CapacityInGig"; } }
        public static string UploadFileLimitInMB { get { return "UploadFileLimitInMB"; } }

        //Payment early status
        public static string PaymentEarlyStatus { get { return "PaymentEarlyStatus"; } }
        //Tag for ignorable registered user by system
        public static string IgnorableRegistration { get { return "IgnorableRegistration"; } }
        //Payment subscription
        public static string ProcessPaymentRequest { get { return "ProcessPaymentRequest"; } }
        public static string ProcessPaymentResult { get { return "ProcessPaymentResult"; } }

        // Assoicated file info
        // TODO: When two databases are combined, we shall not need the following properties.
        public static string AccountId { get { return "AccountId"; } }
        public static string RootDirId { get { return "RootDirId"; } }

        // Group settings
        public static string GroupEmailNotificationLastSentDate {  get { return "EmailNotificationLastSentDate";  } }
        public static string GroupMemberResultingFrom { get { return "GroupMemberResultingFrom"; } }

        // The imagative group for customer contact candidate
        public static string CustomerContactCandidate => "CustomerContactCandidate";

        // Attributes for Connection
        public static string CustomerContactName => "CustomerContactName";
        public static string CustomerContactComment => "CustomerContactComment";
    }
}
