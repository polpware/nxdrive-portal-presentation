﻿using Nop.Services.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;

namespace NXdrive.Web.Extensions
{
    public static class EntityExtensions
    {
        public static bool IsTargetRegisteredCustomer(this IUserGroupInvitation invitationOrApplication)
        {
            return invitationOrApplication.ProviderId == (short)PredefinedUserProvider.NXdriveAccount;
        }

        public static bool IsTargetJustEmail(this IUserGroupInvitation invitationOrApplication)
        {
            return invitationOrApplication.ProviderId == (short)PredefinedUserProvider.Email;
        }

        public static bool IsAppliedByRegisteredCustomer(this IUserGroupInvitation invitationOrApplication)
        {
            return invitationOrApplication.IsTargetRegisteredCustomer() &&
                invitationOrApplication.UserId == invitationOrApplication.CreatorId;
        }

        public static bool IsInvitedByOwner(this IUserGroupInvitation invitationOrApplication, int groupOwnerId)
        {
             return invitationOrApplication.CreatorId == groupOwnerId;
        }

        public static string UserName(this IEmailIdentity identity, ICustomerService customerService)
        {
            if (identity.DerivedProviderId == (short)PredefinedUserProvider.NXdriveAccount)
            {
                var customer = customerService.GetCustomerById(identity.DerivedUserId);
                if (customer != null)
                {
                    return customer.GetFullName();
                }
            }

            return string.Empty;
        }
    }
}
