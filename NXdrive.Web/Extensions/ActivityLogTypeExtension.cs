﻿namespace NXdrive.Web.Extensions
{
    public static class ActivityLogTypeExtension
    {
        public static string CustomerEmailFriendRequest => "EmailFriendRequest";
        public static string CustomerReferFriendService => "ReferFriendService";
        public static string CustomerSendPrivateMessage => "SendPrivateMessage";
    }
}
