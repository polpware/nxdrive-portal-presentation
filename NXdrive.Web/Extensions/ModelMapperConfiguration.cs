﻿using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.NopExt.ServicePlans;
using NXdrive.Web.Models.Customer;
using NXdrive.Web.Models.UserGroup;

namespace NXdrive.Web.Extensions
{
    public class ModelMapperConfiguration : Profile, IMapperProfile
    {
        public ModelMapperConfiguration()
        {
            CreateMap<IUserGroup, GroupModel>();
        }

        // Expected to be initialized last
        public int Order => 100;
    }
}
