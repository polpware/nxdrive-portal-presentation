﻿namespace NXdrive.Web.Extensions
{
    public static class ResourceStrings
    {
        // User group
        public const string UserGroup_QuoteFull = @"Maximum number of group has been reached. 
Please upgrade your service plan.";

        public const string UserGroup_UserNotFound = @"User {0} not found in group {1}";

        public const string UserGroup_NameNotUnique = @"{0} has been used. Please pick up another one.";

        public const string UserGroup_NameEmpty = @"Group name cannot be empty.";

        public const string UserGroup_Invalid = @"Group name is invalid.";

        public const string UserGroup_NameTooShort = @"Group name should contain at least 3 charactors";

        public const string UserGroup_NameTooLong = @"Group name cannot contain more than 64 charactors";

        public const string UserGroup_CreateFailure = @"Something went wrong. 
We cannot create group at this moment.";

        public const string UserGroup_CreateSuccess = @"Group {0} has been created successfully. 
You may start to invite people into this group now.";

        public const string UserGroup_NotOwnedByUser = @"Group {0} doesn't belong to you.";

        public const string UserGroup_NameInvalid = @"Invalid group name: {0}.";

        public const string UserGroup_InvitationTokenInvalid = @"Invalid invitation token.";

        public const string UserGroup_ApplicationTokenInvalid = @"Invalid application token";

        public const string UserGroup_InvitationNotFound = @"Invitation not found.";

        public const string UserGroup_OnlyOwnerCanApproveOthers = @"Please login as the group owner.";

        public const string UserGroup_GroupNotFound = @"Group with Id {0} is not found.";

        public const string UserGroup_UpdateSuccess = @"Group {0} has been updated successfully.";

        public const string UserGroup_InvitationSent = @"Invitation has been sent to";

        public const string UserGroup_CurrentCustomerNotInvited = @"Please re-login as the invited user.";

        public const string UserGroup_DeleteMemberSuccess = @"Member {0} has been removed from group {1}!";

        public const string UserGroup_GroupHasBeenDeleted = @"Sorry, group has been deleted.";

        public const string UserGroup_PeopleAlreadyJoined = @"{0} is already a member of this group.";

        public const string UserGroup_UserHasAlreadyJoined = @"You have already joined group {0}.";

        public const string UserGroup_YouJoinedTooManyGroups = @"You have joined too many groups";

        public const string UserGroup_UserJoinedTooManyGroups = @"Sorry, user have joined too many groups";

        public const string UserGroup_GroupInvitationInvalid = @"Sorry, invitation is no longer valid.";

        public const string UserGroup_GroupFull = @"Sorry, group {0} is already full.";

        public const string UserGroup_MembershipNotFound = @"This user is not in group yet";

        public const string UserGroup_CannotRemoveOwner = @"Cannot delete owner of a group";

        public const string UserGroup_OnlyOwnerCanDeleteOthers = @"Only group owner can remove other members";

        public const string UserGroup_MemberCannotViewGroupInvitations = @"Only group owner can view group invitations";

        public const string UserGroup_AppliedForGroupSucceeded = @"Successfully applied for group {0}.";

        public const string UserGroup_OwnerAcceptedUser = @"You accepted user's application for group.";

        public const string UserGroup_AlreadyApplied = @"You have already applied for group {0}.";

        public const string UserGroup_ApplicationTakenBack = @"Applicant has taken back the application.";

        public const string UserGroup_ApplicationDenied = @"Application has been denied by group owner.";

        public const string UserGroup_AlreadyInvited = @"{0} has already been invited.";

        public const string UserGroup_InvitationDeclined = @"User {0} has declined your invitation.";

        public const string UserGroup_RevokeDeclination = @"You have revoked the declination.";

        public const string UserGroup_CannotTakeBackDeclinedStuff = @"You cannot take back a declined application/invitation.";

        public const string UserGroup_ResendEmailTooOften = @"Email Resend too often, try again tomorrow";

        /// <summary>
        /// translate from error code to error message
        /// unclassified error code will return "Error"
        /// </summary>
        /// <param name="code">error code</param>
        /// <returns>readable message that the error code represents</returns>
        public static string ErrorCodeToMessage(int code)
        {
            switch (code){
                case 10001: return UserGroup_NameEmpty;
                case 10002: return UserGroup_NameTooShort;
                case 10003: return UserGroup_GroupNotFound;
                case 10004: return UserGroup_NameTooLong;
                case 10005: return UserGroup_NameNotUnique;
                case 10006: return UserGroup_NotOwnedByUser;
                case 10007: return UserGroup_CannotRemoveOwner;
                case 10008: return UserGroup_CannotTakeBackDeclinedStuff;
                case 10009: return UserGroup_GroupFull;
                case 10010: return UserGroup_PeopleAlreadyJoined;
                case 10011: return UserGroup_UserHasAlreadyJoined;
                case 10012: return UserGroup_AlreadyInvited;
                case 10013: return UserGroup_YouJoinedTooManyGroups;
                case 10014: return UserGroup_GroupInvitationInvalid;
                case 10015: return UserGroup_GroupHasBeenDeleted;
                case 10016: return UserGroup_InvitationTokenInvalid;
                case 10017: return UserGroup_InvitationNotFound;
                case 10018: return UserGroup_MemberCannotViewGroupInvitations;
                case 10019: return UserGroup_AlreadyApplied;
                case 10020: return UserGroup_MembershipNotFound;
                case 10021: return UserGroup_UserJoinedTooManyGroups;
                case 10022: return UserGroup_NameInvalid;
                case 10023: return UserGroup_ResendEmailTooOften;
                default: return "Error";
            }
        }

        /// <summary>
        /// translate from error code message to error code
        /// unclassified error message will return 10000
        /// </summary>
        /// <param name="errorEnum">error message</param>
        /// <returns>code</returns>
        public static int MessageToErrorCode(string errorEnum)
        {
            switch (errorEnum)
            {
                case UserGroup_NameEmpty: return 10001;
                case UserGroup_NameTooShort: return 10002;
                case UserGroup_GroupNotFound: return 10003;
                case UserGroup_NameTooLong: return 10004;
                case UserGroup_NameNotUnique: return 10005;
                case UserGroup_NotOwnedByUser: return 10006;
                case UserGroup_CannotRemoveOwner: return 10007;
                case UserGroup_CannotTakeBackDeclinedStuff: return 10008;
                case UserGroup_GroupFull: return 10009;
                case UserGroup_PeopleAlreadyJoined: return 10010;
                case UserGroup_UserHasAlreadyJoined: return 10011;
                case UserGroup_AlreadyInvited: return 10012;
                case UserGroup_YouJoinedTooManyGroups: return 10013;
                case UserGroup_GroupInvitationInvalid: return 10014;
                case UserGroup_GroupHasBeenDeleted: return 10015;
                case UserGroup_InvitationTokenInvalid: return 10016;
                case UserGroup_InvitationNotFound: return 10017;
                case UserGroup_MemberCannotViewGroupInvitations: return 10018;
                case UserGroup_AlreadyApplied: return 10019;
                case UserGroup_MembershipNotFound: return 10020;
                case UserGroup_UserJoinedTooManyGroups: return 10021;
                case UserGroup_NameInvalid: return 10022;
                case UserGroup_ResendEmailTooOften: return 10023;
                default: return 10000;
            }
        }
    }
}
