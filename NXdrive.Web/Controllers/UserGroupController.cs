﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Constraints;
using NXdrive.Web.Extensions;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.UserGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    [EnableCors("AllowLocalHost")]
    public partial class UserGroupController : BaseController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IUserGroupService _userGroupService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IGroupMemberService _groupMemberService;

        private readonly CustomerSettings _customerSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IMemberInvitationService _memberInvitationService;

        // Constraint service
        private readonly IUserGroupConstraints _userGroupConstraints;
        private readonly IUserGroupModelFactory _userGroupModelFactory;
        private readonly IUserGroupMemberModelFactory _userGroupMemberModelFactory;

        // fields to improve performance of listing groups
        public int maxPreviewMemberCount = 3;  // maximum number of group members that shows in the member preview tool tip
        private int maxPreviewGroupCount = 5;   // if there are more than a certain groups that user is in, only show the owner in member preview tool tip

        // protected readonly IWorkflowMessageService _workflowMessageService;

        public UserGroupController(IWorkContext workContext,
            IStoreContext storeContext,
            IUserGroupService userGroupService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            ICustomerService customerService,
            // IWorkflowMessageService workflowMessageService,
            CustomerSettings customerSettings,
            MediaSettings mediaSettings,
            IGroupMemberService groupMemberService,
            IMemberInvitationService memberInvitationService,
            IUserGroupConstraints userGroupConstraints,
            IUserGroupModelFactory userGroupModelFactory,
            IUserGroupMemberModelFactory userGroupMemberFactory)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._userGroupService = userGroupService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._memberInvitationService = memberInvitationService;
            // this._workflowMessageService = workflowMessageService;

            this._customerSettings = customerSettings;
            this._mediaSettings = mediaSettings;
            this._groupMemberService = groupMemberService;

            this._userGroupConstraints = userGroupConstraints;
            this._userGroupModelFactory = userGroupModelFactory;
            this._userGroupMemberModelFactory = userGroupMemberFactory;
        }

        #region entry of the webapp
        [HttpGet("user-group", Name = "UserGroupIndexDefault")]
        [HttpGet("user-group/index", Name = "UserGroupIndex")]
        public IActionResult Index()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            return View("/wwwroot/user-group-app/index.html");
        }

        #endregion


        #region group query/search
        [HttpPost("user-group/check-name")]
        // [PublicAntiForgery]
        public async Task<ActionResult> CheckGroupNameAvailability(string name)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (string.IsNullOrWhiteSpace(name))
            {
                return Json(new
                {
                    nameOk = false,
                    errorInfo = ResourceStrings.UserGroup_NameEmpty,
                    errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_NameEmpty)
                });
            }

            name = name.Trim();
            if (name.Length < 3)
            {
                return Json(new
                {
                    nameOk = false,
                    errorInfo = ResourceStrings.UserGroup_NameTooShort,
                    errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_NameTooShort)
                });
            }

            var relevantNames = await _userGroupService.CheckGroupName(name);
            bool unique = !relevantNames.Any(x => x.Equals(name));

            return Json(new { nameOk = unique });
        }

        [HttpPost("user-group/search")]
        // [PublicAntiForgery]
        public async Task<ActionResult> SearchGroup(string name, int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            name = string.IsNullOrEmpty(name) ? string.Empty : name.Trim();
            var customer = _workContext.CurrentCustomer;

            var groups = await _userGroupService.SearchGroups(name,
                ignorePrivate: true, ignoreDeleted: true,
                pageIndex: pageIndex, pageSize: pageSize);

            var invitations = await _memberInvitationService.FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);

            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            var models = new List<GroupModel>();

            foreach(var group in groups)
            {
                var gm = new GroupModel();
                _userGroupModelFactory.PrepareGroupModel(group, customer, gm, invitations, joinedGroups);
                models.Add(gm);
            }

            return Json(new
            {
                total = groups.TotalCount,
                offset = groups.PageIndex * groups.PageSize,
                data = models
            });
        }

        [HttpGet("user-group/find/{id:min(1)}")]
        public async Task<ActionResult> FindGroupById(int id)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var group = await _userGroupService.GetGroupById(id);
            var invitations = await _memberInvitationService.FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);
            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            if (group == null)
            {
                return new JsonResult(new
                {
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, id.ToString()),
                    errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_GroupNotFound)
                })
                {
                    StatusCode = 404
                };
            }

            var model = new GroupModel();
             this._userGroupModelFactory.PrepareGroupModel(group, customer, model, invitations, joinedGroups);

            return Json(model);
        }

        #endregion


        #region group list
        [HttpPost("user-group/list-owned-groups")]
        public async Task<ActionResult> ListOwnedGroups(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var groups = await _userGroupService.SearchGroups(string.Empty,
                ignorePrivate: false,
                ignoreDeleted: true,
                customerId: customer.Id, pageIndex: pageIndex, pageSize: pageSize);

            var models = new List<GroupModel>();
            foreach (var group in groups)
            {
                var gm = new GroupModel();
                 this._userGroupModelFactory.PrepareGroupModel(group, customer, gm);
                gm.IsMember = true;
                models.Add(gm);
            }

            return Json(new
            {
                total = groups.TotalCount,
                offset = groups.PageIndex * groups.PageSize,
                data = models
            });
        }

        [HttpPost("user-group/list-joined-groups")]
        public async Task<ActionResult> ListJoinedGroups(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var groups = await _userGroupService.FindAssociatedGroups(customer.Id,
                ignoreDeleted: true,
                pageIndex: pageIndex, pageSize: pageSize);
            var invitations = await _memberInvitationService.FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);
            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            var models = new List<GroupModel>();
            foreach (var group in groups)
            {
                var gm = new GroupModel();
                 this._userGroupModelFactory.PrepareGroupModel(group, customer, gm, invitations, joinedGroups);
                models.Add(gm);
            }

            return Json(new
            {
                total = groups.TotalCount,
                offset = groups.PageIndex * groups.PageSize,
                data = models
            });
        }

        // todo: Maybe not needed any more
        [HttpPost("user-group/list-impending-groups")]
        public async Task<ActionResult> ListPendingGroups(string keyword, int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            // Each invitation corresponds to a unique group,
            // as at most one invitation for a customer is allowed in a group
            var invitations = await _memberInvitationService.FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id,
                ignoreDeleted: true, pageIndex: pageIndex, pageSize: pageSize);

            var models = new List<InvitationModel>();
            foreach (var y in invitations)
            {
                var im = new InvitationModel();
                var group = await this._userGroupService.GetGroupById(y.GroupId);
                await this._userGroupMemberModelFactory.PrepareGroupInvitationModel(group, y, im);
                models.Add(im);
            }

            return Json(new
            {
                data = models,
                total = invitations.TotalCount,
                offset = invitations.PageIndex * invitations.PageSize
            });
        }
        #endregion

        /*
        private static string TranslateErrorCode(ConstraintCode code, string name)
        {
            switch (code)
            {
                case ConstraintCode.UserGroup_NameEmpty:
                    return ResourceStrings.UserGroup_NameEmpty;

                // new name cannot be longer than 64
                case ConstraintCode.UserGroup_NameTooLong:
                    return ResourceStrings.UserGroup_NameTooLong;

                case ConstraintCode.UserGroup_NameInvalid:
                    return string.Format(ResourceStrings.UserGroup_NameInvalid, name);

                // new name must be unique in the whole system
                case ConstraintCode.UserGroup_NameNotUnique:
                    return String.Format(ResourceStrings.UserGroup_NameNotUnique, name);

                case ConstraintCode.UserGroup_NotOwnedByUser:
                    return String.Format(ResourceStrings.UserGroup_NotOwnedByUser, name);

                default:
                    return "Error";
            }
        }*/


        #region Dashboard (counters)
        [HttpPost("user-group/dashboard")]
        public async Task<ActionResult> Dashboard()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            // Each invitation corresponds to a unique group,
            // as at most one invitation for a customer is allowed in a group
            var owneredCounter = await _userGroupService.CountOwnedGroupForCustomer(customer.Id);
            var joinedCounter = await _userGroupService.CountJoinedGroupForCustomer(customer.Id);

            var receivedApply = await _memberInvitationService.CountReceivedApplicationsForCustomer(customer.Id);
            var sentApply = await _memberInvitationService.CountSentApplicationsForCustomer(customer.Id);
            var sentInvite = await _memberInvitationService.CountSentInvitationsForCustomer(customer.Id);
            var receivedInvite = await _memberInvitationService.CountReceivedInvitationsForCustomer(customer.Id);

            return Json(new
            {
                OwnedGroups = owneredCounter,
                JoinedGroups = joinedCounter,
                ReceivedApplications = receivedApply,
                ReceivedInvitations = receivedInvite,
                SentApplications = sentApply,
                SentInvitations = sentInvite
            });
        }
        #endregion


        #region edit/create group
        // [PublicAntiForgery]
        [HttpPost("user-group/create-group")]
        public async Task<ActionResult> CreateGroup(GroupModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // todo: validate other group attributes other than name
            if (ModelState.IsValid)
            {
                var customer = _workContext.CurrentCustomer;

                // Validate if the current user is allowed to create a roup
                var code = await _userGroupConstraints.CanCreateGroup(customer);

                if (code != GroupConstraintCode.NoError)
                {
                    return new JsonResult(new
                    {
                        errorInfo = ResourceStrings.ErrorCodeToMessage((int)code),
                        errorCode = (int)code
                    })
                    {
                        StatusCode = 422
                    };
                }

                // Normaize inputs
                model.Name = model.Name?.Trim();
                model.Desc = model.Desc?.Trim();

                code = await _userGroupConstraints.IsValidGroupName(model, customer, false);

                if (code != GroupConstraintCode.NoError)
                {
                    return new JsonResult(new
                    {
                        errorCode = (int)code,
                        errorInfo = string.Format(ResourceStrings.ErrorCodeToMessage((int)code), model.Name)
                    })
                    {
                        StatusCode = 422
                    };
                }

                // case: creating one group
                var group = await _userGroupService.CreateGroup(customer.Id, model.Name, model.Desc, !model.IsPublic);

                if (group == null)
                {
                    if (code != GroupConstraintCode.NoError)
                    {
                        return new JsonResult(new
                        {
                            // todo: Define othe error codes for operation failure such as server down
                            errorCode = GroupConstraintCode.Error,
                            errorInfo = ResourceStrings.ErrorCodeToMessage((int)code)
                        })
                        {
                            StatusCode = 500
                        };
                    }
                }

                // add creator into group and update modified timestamp
                // Some questions: What if the group is created but the owner is not added?
                // Validate a group on starting to use this group, e.g., sending invitations
                // todo: Make sure that this operation will not fail the whole op
                await _groupMemberService.AddMemberIntoGroup(group.Id, customer.Id);

                var newModel = new GroupModel();
                this._userGroupModelFactory.PrepareGroupModel(group, customer, newModel);
                newModel.IsMember = true;

                return Json(newModel);
            }

            return new JsonResult(new
            {
                // todo: Define othe error codes for model error
                errorCode = GroupConstraintCode.Error,
                errorInfo = ResourceStrings.ErrorCodeToMessage((int)GroupConstraintCode.Error)
            })
            {
                StatusCode = 400
            };
        }


        /// <summary>
        /// Saving new group model in system
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("user-group/edit-group/{id:min(1)}")]
        // [PublicAntiForgery]
        public async Task<ActionResult> EditGroup(int id, GroupModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // todo: validate other group attributes other than name
            if (ModelState.IsValid)
            {
                var customer = _workContext.CurrentCustomer;

                var group = await _userGroupService.GetGroupById(id);
                if (group == null)
                {
                    return new JsonResult(new
                    {
                        errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_GroupNotFound),
                        errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, model.Id)
                    })
                    {
                        StatusCode = 404
                    };
                }

                var code = _userGroupConstraints.CanEditGroup(group, customer);
                // current customer must the creator of this group
                if (code != GroupConstraintCode.NoError)
                {
                    return new JsonResult(new
                    {
                        errorInfo = string.Format(ResourceStrings.ErrorCodeToMessage((int)code), model.Name),
                        errorCode = (int)code,
                    })
                    {
                        StatusCode = 422
                    };

                }

                // Normaize inputs
                model.Name = model.Name?.Trim();
                model.Desc = model.Desc?.Trim();

                code = await _userGroupConstraints.IsValidGroupName(model, customer, true /* edit */);

                if (code != GroupConstraintCode.NoError)
                {
                    return new JsonResult(new
                    {
                        errorCode = (int)code,
                        errorInfo = string.Format(ResourceStrings.ErrorCodeToMessage((int)code), model.Name)
                    })
                    {
                        StatusCode = 422
                    };
                }

                // case: update one group
                var tyGroup = group as UserGroup;

                // Otherwise good
                // tyGroup.Name = model.Name;
                tyGroup.Desc = model.Desc;

                // todo: Decide if the user can modify privacy based on more logic
                tyGroup.Privacy = !model.IsPublic;

                // todo: also update avatar

                await _userGroupService.UpdateGroup(group);

                var newModel = new GroupModel();
                 this._userGroupModelFactory.PrepareGroupModel(group, customer, newModel);
                newModel.IsMember = true;

                return Json(newModel);
            }

            // failed to create group because group model is invalid
            return new JsonResult(new
            {
                // todo: Define othe error codes for model error
                errorCode = GroupConstraintCode.Error,
                errorInfo = ResourceStrings.ErrorCodeToMessage((int)GroupConstraintCode.Error)
            })
            {
                StatusCode = 400
            };
        }

        [HttpDelete("user-group/delete-group/{id:min(1)}")]
        public async Task<ActionResult> DeleteGroup(int id)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            // find address (ensure that it belongs to the current customer)
            var group = await _userGroupService.GetGroupById(id);
            if (group == null)
            {
                return new JsonResult(new
                {
                    errorCode = 10002,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, id.ToString())
                })
                {
                    StatusCode = 404
                };
            }

            var code = _userGroupConstraints.CanDeleteGroup(group, customer);

            // current user is not the owner of this group
            if (code == GroupConstraintCode.UserGroup_NotOwnedByUser)
            {
                return new JsonResult(new
                {
                    errorCode = code,
                    errorInfo = string.Format(ResourceStrings.UserGroup_NotOwnedByUser, group.Name)
                })
                {
                    StatusCode = 422
                };
            }

            // todo: Do we need to delete all members and all invitations ??

            // remove all members before deleting a group
            var members = await _groupMemberService.GetGroupMembers(id);
            foreach (var mem in members)
            {
                await _groupMemberService.DeleteGroupMember(mem);
            }

            // remove all invitations to this group
            var invitations = await _memberInvitationService.SearchInvitationAndApplicationsByGroup(id);
            foreach (var v in invitations)
            {
                await _memberInvitationService.DeleteInvitationOrApplication(v);
            }

            await _userGroupService.DeleteGroup(group);

            return Ok();
        }
        #endregion


        #region avatar
        // [PublicAntiForgery]
        [HttpPost("user-group/update-group-avatar")]
        public async Task<ActionResult> UpdateGroupAvatar(int groupId, IFormFile uploadedFile)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            var group = await _userGroupService.GetGroupById(groupId);
            if (group == null)
            {
                return new JsonResult(new
                {
                    errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_GroupNotFound),
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, groupId)
                })
                {
                    StatusCode = 404
                };
            }

            var code = _userGroupConstraints.CanEditGroup(group, customer);
            // current customer must the creator of this group
            if (code != GroupConstraintCode.NoError)
            {
                return new JsonResult(new
                {
                    errorInfo = string.Format(ResourceStrings.ErrorCodeToMessage((int)code), group.Name),
                    errorCode = (int)code,
                })
                {
                    StatusCode = 422
                };

            }

            if ((uploadedFile == null) || (String.IsNullOrEmpty(uploadedFile.FileName)))
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_AvatarUploadError,
                    errorInfo = ResourceStrings.ErrorCodeToMessage((int)GroupConstraintCode.UserGroup_AvatarUploadError)
                })
                {
                    StatusCode = 422
                };
            }

            try
            {
                int avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                if (uploadedFile.Length > avatarMaxSize)
                {
                    return new JsonResult(new
                    {
                        errorCode = (int)GroupConstraintCode.UserGroup_AvatarTooBig,
                        errorInfo = ResourceStrings.ErrorCodeToMessage((int)GroupConstraintCode.UserGroup_AvatarTooBig)
                    })
                    {
                        StatusCode = 422
                    };
                }
                byte[] customerPictureBinary = uploadedFile.GetPictureBits();
                if (group.AvatarImageId > 0)
                {
                    _pictureService.UpdatePicture(group.AvatarImageId, customerPictureBinary, uploadedFile.ContentType, null);
                }
                else
                {
                    var picture = _pictureService.InsertPicture(customerPictureBinary, uploadedFile.ContentType, null);
                    // Update group
                    group.AvatarImageId = picture.Id;
                    // Update group avatar id
                    await _userGroupService.UpdateGroup(group);
                }

                var url = _pictureService.GetPictureUrl(group.AvatarImageId, _mediaSettings.AvatarPictureSize, false);

                return new JsonResult(new
                {
                    groupId = groupId,
                    // todo:
                    avatarUrl = url
                });
            }
            catch (Exception e)
            {

            }

            return new JsonResult(new
            {
                errorCode = GroupConstraintCode.Error,
                errorInfo = ResourceStrings.ErrorCodeToMessage((int)GroupConstraintCode.Error)
            })
            {
                StatusCode = 500
            };

        }
        #endregion
    }
}