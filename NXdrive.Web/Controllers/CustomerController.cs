﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;

namespace NXdrive.Web.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Nop.Core;
    using Nop.Core.Domain;
    using Nop.Core.Domain.Common;
    using Nop.Core.Domain.Customers;
    using Nop.Core.Domain.Forums;
    using Nop.Core.Domain.Gdpr;
    using Nop.Core.Domain.Localization;
    using Nop.Core.Domain.Media;
    using Nop.Core.Domain.Tax;
    using Nop.Core.Infrastructure;
    using Nop.Services.Authentication;
    using Nop.Services.Authentication.External;
    using Nop.Services.Common;
    using Nop.Services.Customers;
    using Nop.Services.Directory;
    using Nop.Services.Events;
    using Nop.Services.ExportImport;
    using Nop.Services.Gdpr;
    using Nop.Services.Helpers;
    using Nop.Services.Localization;
    using Nop.Services.Logging;
    using Nop.Services.Media;
    using Nop.Services.Messages;
    using Nop.Services.Orders;
    using Nop.Services.Tax;
    using Nop.Web.Framework.Controllers;
    using Nop.Web.Framework.Security.Captcha;
    using NXdrive.Web.AOP.Customers;
    using NXdrive.Web.Factories;
    using NXdrive.Web.Models.Customer;
    using Polpware.NopWeb.Factories;
    using Polpware.NopWeb.Models.Customer;
    using System;
    using BaseCustomerController = Polpware.NopWeb.Controllers.CustomerController;

    [HttpsRequirement(SslRequirement.Yes)]
    public partial class CustomerController : BaseCustomerController
    {

        private readonly ICustomerModelFactory _customerModelFactory;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly CustomerSettings _customerSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IAddressService _addressService;
        private readonly IPictureService _pictureService;
        private readonly CaptchaSettings _captchaSettings;

        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IEventPublisher _eventPublisher;
        private readonly StoreInformationSettings _storeInformationSettings;

        private readonly INopFileProvider _fileProvider;

        // Factories
        private readonly IServicePlanModelFactory _servicePlanModelFactory;

        public CustomerController(IAddressModelFactory addressModelFactory,
            ICustomerModelFactory customerModelFactory,
            IAuthenticationService authenticationService,
            DateTimeSettings dateTimeSettings,
            TaxSettings taxSettings,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ICustomerService customerService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerAttributeService customerAttributeService,
            IExportManager exportManager,
            IGdprService gdprService,
            IGenericAttributeService genericAttributeService,
            ICustomerRegistrationService customerRegistrationService,
            ITaxService taxService,
            CustomerSettings customerSettings,
            AddressSettings addressSettings,
            ForumSettings forumSettings,
            IAddressService addressService,
            ICountryService countryService,
            IOrderService orderService,
            IPictureService pictureService,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IShoppingCartService shoppingCartService,
            IExternalAuthenticationService externalAuthenticationService,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            IEventPublisher eventPublisher,
            MediaSettings mediaSettings,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            CaptchaSettings captchaSettings,
            GdprSettings gdprSettings,
            StoreInformationSettings storeInformationSettings,
            INopFileProvider nopFileProvider,
            // followed are factories
            IServicePlanModelFactory servicePlanModelFactory
            ) : base(addressModelFactory,
                customerModelFactory, authenticationService, dateTimeSettings, taxSettings,
                localizationService, workContext, storeContext, customerService, customerAttributeParser, customerAttributeService,
                exportManager, gdprService,
                genericAttributeService, customerRegistrationService, taxService, customerSettings,
                addressSettings, forumSettings, addressService, countryService, orderService, pictureService,
                newsLetterSubscriptionService, shoppingCartService, externalAuthenticationService, webHelper,
                customerActivityService, addressAttributeParser, addressAttributeService, eventPublisher, mediaSettings,
                workflowMessageService, localizationSettings, captchaSettings, gdprSettings, storeInformationSettings)
        {
            this._customerService = customerService;
            this._workContext = workContext;
            this._customerModelFactory = customerModelFactory;
            this._workflowMessageService = workflowMessageService;
            this._localizationService = localizationService;
            this._genericAttributeService = genericAttributeService;
            this._customerAttributeParser = customerAttributeParser;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
            this._customerSettings = customerSettings;
            this._addressService = addressService;
            _captchaSettings = captchaSettings;

            // Some settings
            _customerActivityService = customerActivityService;
            _authenticationService = authenticationService;
            _eventPublisher = eventPublisher;
            _storeInformationSettings = storeInformationSettings;

            _fileProvider = nopFileProvider;

            // Factories
            this._servicePlanModelFactory = servicePlanModelFactory;
        }

        #region email input
        [Route("presign", Name = "PreSign")]
        [AcceptVerbs("GET")]
        public IActionResult PreSign(string next)
        {
            var model = new PreSignModel();
            model.NextStep = next;

            return View(model);
        }

        [Route("presign", Name = "PreSign")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        [ValidateCaptcha]
        public IActionResult PreSign(PreSignModel model, bool captchaValid)
        {
            // todo: Maybe avoid using model.DisplayCaptcha
            if (_captchaSettings.Enabled && model.DisplayCaptcha && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));

                return View(model);
            }

            var isSignup = string.Equals(model.NextStep, "signup", StringComparison.CurrentCultureIgnoreCase);

            if (ModelState.IsValid)
            {
                var email = model.Email;
                var customer = _customerService.GetCustomerByEmail(email);
                if(customer == null)
                {
                    // customer doesn't exsit, might have typed the wrong email or wants to register
                    if (!isSignup)
                    {
                        // Case 1: user wants to login but typed a non-existing email
                        model.Errors.Add(Tuple.Create("Customer doesn't exist. Check your email address.",
                            "", ""));   // only display the error reason in this case, there is no action system can help user do
                        return View(model);
                    }
                }
                else if(!customer.Active)
                {
                    // Case 2: user entered an email which is registered but not activated yet
                    var resendUrl = Url.RouteUrl("ResendEmailValidation",new { email = model.Email });
                    model.Errors.Add(Tuple.Create("This account is not activated yet.",
                        "Click here to resend email activation.", resendUrl));   // if user click on the link, route them to page to resend email activation
                    return View(model);
                }
                else if(isSignup)
                {
                    // Case 3: this email address is already registered and activated, and user wants to signup with it
                    model.Errors.Add(Tuple.Create("This email address is already in use.",
                        "", ""));   // if user click on the link, route them to page to resend email activation
                    return View(model);
                }

                if (!isSignup)
                {
                    // Case 4: email is already in use and current user wish to sign-in
                    return RedirectToRoute("Login", new { email = model.Email, returnurl = model.ReturnUrl });
                }
                else
                {
                    // Case 5: email is not in used and user wants to register
                    return RedirectToRoute("Register", new { email = model.Email, returnurl = model.ReturnUrl });
                }
            }

            return View(model);
        }

        #endregion

        #region Register and Validation
        [Route("register", Name = "LegacyRegister")]
        [Route("signup", Name = "Register")]
        [AcceptVerbs("GET")]
        public override IActionResult Register()
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                // Redirect to account page
                return RedirectToRoute("CustomerDashboard");
            }

            if (!Request.Query.ContainsKey("email"))
            {
                return RedirectToAction("PreSign", new { next = "signup", returnUrl = Request.Query["returnUrl"] });
            }

            return base.Register();
        }

        [Route("signup", Name = "Register")]
        [AcceptVerbs("POST")]
        [ValidateCaptcha]
        [PublicAntiForgery]
        public override IActionResult Register(RegisterModel model, string returnUrl, bool captchaValid)
        {
            // todo: Maybe avoid using model.DisplayCaptcha
            if (_captchaSettings.Enabled && model.DisplayCaptcha && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));

                return View(model);
            }

            return base.Register(model, returnUrl, captchaValid);
        }

        [Route("register-result", Name = "RegisterResult")]
        [AcceptVerbs("GET")]
        public override IActionResult RegisterResult(int resultId)
        {
            var justRegisteredCustomer = _workContext.CurrentCustomer;
            var source = _customerModelFactory.PrepareRegisterResultModel(resultId);
            var model = Models.Customer.RegisterResultModel.Build(source, justRegisteredCustomer.Email);

            return View(model);
        }

        [Route("resend-email-validation", Name = "ResendEmailValidation")]
        [AcceptVerbs("Get")]
        public IActionResult ResendEmailValidation(string email)
        {
            Models.Customer.PasswordRecoveryModel validateModel = new Models.Customer.PasswordRecoveryModel();
            validateModel.Email = email;
            return View(validateModel);
        }

        [Route("resend-email-validation", Name = "ResendEmailValidation")]
        [AcceptVerbs("POST")]
        public IActionResult ResendEmailValidation(Models.Customer.PasswordRecoveryModel model)
        {
            // todo: limit the frequency that user resend email verification
            var customer = _customerService.GetCustomerByEmail(model.Email);
            if (customer == null)
            {
                model.Success = false;
                return View(model);
            }
            if(customer.Active)
            {
                model.Success = false;
                return View(model);
            }
            _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);
            model.Success = true;
            model.Result = "Email has been sent.";
            return View(model);
        }

        [Route("customer/activation", Name = "AccountActivation")]
        // [AccountActivationNoty] // note that we now assign a service plan on the first login rather than the moment the activation happens
        public override IActionResult AccountActivation(string token, string email)
        {
            return base.AccountActivation(token, email);
        }

        [Route("customer/revalidation", Name = "EmailRevalidation")]
        public override IActionResult EmailRevalidation(string token, string email)
        {
            return base.EmailRevalidation(token, email);
        }

        #endregion

        #region Login in and out
        [Route("login", Name = "LegacyLogin")]
        [Route("signin", Name = "Login")]
        [AcceptVerbs("GET")]
        public virtual IActionResult Login()
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                // Redirect to account page
                return RedirectToRoute("CustomerDashboard");
            }

            if (!Request.Query.ContainsKey("email"))
            {
                return RedirectToAction("PreSign", new { next = "signin", returnUrl = Request.Query["returnUrl"] });
            }

            return base.Login(false);
        }

        [Route("signin", Name = "Login")]
        [AcceptVerbs("POST")]
        [ValidateCaptcha]
        [PublicAntiForgery]
        public override IActionResult Login(LoginModel model, string returnUrl, bool captchaValid)
        {
            // todo: Maybe avoid using model.DisplayCaptcha
            if (_captchaSettings.Enabled && model.DisplayCaptcha && !captchaValid)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));

                return View(model);
            }

            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = Url.RouteUrl("CustomerDashboard");
            }

            if (!model.DisplayCaptcha)
            {
                var customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer != null)
                {
                    _captchaSettings.ShowOnLoginPage = customer.FailedLoginAttempts > 1;
                }
            }

            return base.Login(model, returnUrl, captchaValid);
        }

        [HttpGet("logout", Name = "LegacyLogout")]
        [HttpGet("signout", Name = "Logout")]
        public override IActionResult Logout()
        {
            return base.Logout();
        }

        [HttpGet("switch-account", Name = "SwitchAccount")]
        public IActionResult SwitchAccount()
        {
            //Finish logout logic first
            //activity log
            _customerActivityService.InsertActivity(_workContext.CurrentCustomer, "PublicStore.Logout",
                _localizationService.GetResource("ActivityLog.PublicStore.Logout"), _workContext.CurrentCustomer);

            //standard logout
            _authenticationService.SignOut();

            //raise logged out event
            _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

            //EU Cookie
            if (_storeInformationSettings.DisplayEuCookieLawWarning)
            {
                //the cookie law message should not pop up immediately after logout.
                //otherwise, the user will have to click it again...
                //and thus next visitor will not click it... so violation for that cookie law..
                //the only good solution in this case is to store a temporary variable
                //indicating that the EU cookie popup window should not be displayed on the next page open (after logout redirection to homepage)
                //but it'll be displayed for further page loads
                TempData["nop.IgnoreEuCookieLawWarning"] = true;
            }

            // Then forward to login
            var model = _customerModelFactory.PrepareLoginModel(false);
            return View("Login", model);
        }

        #endregion

        #region Password Recovery
        [Route("password-recovery", Name = "PasswordRecovery")]
        [AcceptVerbs("GET")]
        public override IActionResult PasswordRecovery()
        {
            var inter = _customerModelFactory.PreparePasswordRecoveryModel();
            var model = new Models.Customer.PasswordRecoveryModel(inter);
            return View(model);
        }


        [Route("password-recovery", Name = "PasswordRecovery")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public IActionResult PasswordRecovery(Models.Customer.PasswordRecoveryModel model)
        {
            if (ModelState.IsValid)
            {
                var customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer != null && customer.Active && !customer.Deleted)
                {
                    //save token and current date
                    var passwordRecoveryToken = Guid.NewGuid();
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.PasswordRecoveryToken,
                        passwordRecoveryToken.ToString());
                    DateTime? generatedDateTime = DateTime.UtcNow;
                    _genericAttributeService.SaveAttribute(customer,
                        SystemCustomerAttributeNames.PasswordRecoveryTokenDateGenerated, generatedDateTime);

                    //send email
                    _workflowMessageService.SendCustomerPasswordRecoveryMessage(customer,
                        _workContext.WorkingLanguage.Id);

                    model.Result = _localizationService.GetResource("Account.PasswordRecovery.EmailHasBeenSent");
                    model.Success = true;

                }
                else
                {
                    model.Success = false;
                    model.Result = _localizationService.GetResource("Account.PasswordRecovery.EmailNotFound");
                }

                return View(model);
            }

            model.Result = string.Empty;
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [Route("password-recovery/confirm", Name = "PasswordRecoveryConfirm")]
        [AcceptVerbs("GET")]
        public override IActionResult PasswordRecoveryConfirm(string token, string email)
        {
            return base.PasswordRecoveryConfirm(token, email);
        }

        [Route("password-recovery/confirm", Name = "PasswordRecoveryConfirm")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public IActionResult PasswordRecoveryConfirm(string token, string email, PasswordRecoveryConfirmModel model)
        {
            return base.PasswordRecoveryConfirmPOST(token, email, model);
        }

        #endregion

        #region Wishlist
        // Note that we on purpose have such an url, so that
        // our code from nopCommerce will not break.
        // In other words, this is for the compatibility with nopCommerce.
        [HttpGet("customer/wishlist", Name = "Wishlist")]
        public virtual IActionResult WishList()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            return Content("");
        }
        #endregion


        [Route("customer/dashboard", Name = "CustomerDashboard")]
        [AcceptVerbs("GET")]
        public virtual IActionResult CustomerDashboard()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();
            var model = new CustomerDashboardModel();
            // var model = new OurDashboardModel();
            // _customerModelFactory.PrepareCustomerInfoModel(model, _workContext.CurrentCustomer, false);

            return View(model);
        }

        [PublicAntiForgery]
        [Route("customer/info", Name = "CustomerInfo")]
        [AcceptVerbs("POST")]
        public override IActionResult Info(CustomerInfoModel model)
        {
            return base.Info(model);
        }

        [Route("customer/info", Name = "CustomerInfo")]
        [AcceptVerbs("GET")]
        public override IActionResult Info()
        {
            return base.Info();
        }

        #region avatar

        private const string THUMBS_PATH = @"images\thumbs";
        private const string DEFAULT_AVATAR = @"images\default-avatar.jpg";

        [Route("customer/avatar/{id:guid}", Name = "CustomerLoadAvatar")]
        [AcceptVerbs("GET")]
        public IActionResult LoadAvatar(string id)
        {
            if (Guid.TryParse(id, out Guid guid))
            {
                var customer = _customerService.GetCustomerByGuid(guid);

                var url = _pictureService.GetPictureUrl(
                            customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId),
                            _mediaSettings.AvatarPictureSize,
                            false);
                if (!string.IsNullOrEmpty(url))
                {
                    var thumbsDirectoryPath = _fileProvider.GetAbsolutePath(THUMBS_PATH);
                    var fileName = _fileProvider.GetFileName(url);
                    var thumbFilePath = _fileProvider.Combine(thumbsDirectoryPath, fileName);
                    string mimeType = System.Web.MimeMapping.GetMimeMapping(fileName);
                    return new PhysicalFileResult(thumbFilePath, mimeType);
                }
            }

            var defaultPath = _fileProvider.GetAbsolutePath(DEFAULT_AVATAR);
            var fname = _fileProvider.GetFileName(defaultPath);
            string m = System.Web.MimeMapping.GetMimeMapping(fname);

            return new PhysicalFileResult(defaultPath, m);
        }

        [Route("customer/avatar/upload", Name = "CustomerUploadAvatar")]
        [AcceptVerbs("POST")]
        //[PublicAntiForgery]
        [FormValueRequired("upload-avatar")]
        public override IActionResult UploadAvatar(Polpware.NopWeb.Models.Customer.CustomerAvatarModel model, IFormFile uploadedFile)
        {
            // todo: Something
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            string error = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    var customerAvatar = _pictureService.GetPictureById(customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId));
                    if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
                    {
                        var avatarMaxSize = _customerSettings.AvatarMaximumSizeBytes;
                        if (uploadedFile.Length > avatarMaxSize) {
                            throw new NopException(string.Format(_localizationService.GetResource("Account.Avatar.MaximumUploadedFileSize"), avatarMaxSize));
                        }

                        var ticks = DateTime.UtcNow.Ticks;

                        var customerPictureBinary = uploadedFile.GetPictureBits();
                        if (customerAvatar != null)
                        {
                            customerAvatar = _pictureService.UpdatePicture(customerAvatar.Id, customerPictureBinary, uploadedFile.ContentType, null, titleAttribute: ticks.ToString());
                        }
                        else
                        {
                            customerAvatar = _pictureService.InsertPicture(customerPictureBinary, uploadedFile.ContentType, null, titleAttribute: ticks.ToString());
                        }
                    }

                    var customerAvatarId = 0;
                    if (customerAvatar != null)
                        customerAvatarId = customerAvatar.Id;

                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AvatarPictureId, customerAvatarId);

                    model.AvatarUrl = _pictureService.GetPictureUrl(
                        customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId),
                        _mediaSettings.AvatarPictureSize,
                        false);

                    return RedirectToRoute("CustomerInfo");

                }
                catch (NopException exc)
                {
                    error = exc.Message;
                    ModelState.AddModelError("", exc.Message);
                }
                catch (Exception exc)
                {
                    error = exc.Message;
                    ModelState.AddModelError("", exc.Message);
                }
            } else
            {
                error = "Something went wrong!";
            }

            AddNotification(Nop.Web.Framework.UI.NotifyType.Error, error, true);

            //If we got this far, something failed, redisplay form
            return RedirectToRoute("CustomerInfo");
        }


        [Route("customer/avatar/remove", Name = "CustomerRemoveAvatar")]
        [AcceptVerbs("POST")]
        //[PublicAntiForgery]
        [FormValueRequired("remove-avatar")]
        public override IActionResult RemoveAvatar(Polpware.NopWeb.Models.Customer.CustomerAvatarModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var customerAvatar = _pictureService.GetPictureById(customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId));
            if (customerAvatar != null)
                _pictureService.DeletePicture(customerAvatar);

            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AvatarPictureId, 0);

            return RedirectToRoute("CustomerInfo");
        }



        #endregion

        [Route("customer/changepassword", Name = "CustomerChangePassword")]
        [AcceptVerbs("GET")]
        public override IActionResult ChangePassword()
        {
            return base.ChangePassword();
        }

        [Route("customer/changepassword", Name = "CustomerChangePassword")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public override IActionResult ChangePassword(ChangePasswordModel model)
        {
            return base.ChangePassword(model);
        }

        // todo:
        [HttpGet("customer/payment", Name = "PaymentMethod")]
        public virtual IActionResult Payment()
        {
            // todo: add payment model

            return View();
        }

        [Route("customer/myplan", Name = "CustomerPlanUsage")]
        [AcceptVerbs("GET")]
        public IActionResult CustomerPlanUsage()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // CustomerServicePlanModel customerSerivcePlanModel = new CustomerServicePlanModel();

            var customerSerivcePlanModel = _servicePlanModelFactory.PrepareCustomerServicePlanModel(_workContext.CurrentCustomer);

            return View(customerSerivcePlanModel);
        }

        

        [Route("customer/activity", Name = "CustomerActivities")]
        [AcceptVerbs("GET")]
        public IActionResult CustomerActivities()
        {
            //if (!_workContext.CurrentCustomer.IsRegistered())
            //    return Challenge();

          
                return View();
            }



        [Route("customer/privacy", Name = "Privacy")]
        [AcceptVerbs("GET")]
        public virtual IActionResult Privacy()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();
            PrivacyModel model = new PrivacyModel();
            return View(model);
        }

        // todo: slider button for cookie setting
        // if cookie was allowed, disable it, vice versa
        [Route("customer/privacy/cookie", Name = "EnableDisableCookie")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public IActionResult EnableDisableCookie()
        {
            return Ok();
        }

        // todo: slider button for file sync setting
        // if file sync was allowed, disable it, vice versa
        [Route("customer/privacy/synchronization", Name = "Synchronization")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public IActionResult Synchronization()
        {
            return Ok();
        }
    }
}