﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Vendors;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Framework.Themes;
using Polpware.NopWeb.Factories;
using Polpware.NopWeb.Models.Common;

namespace NXdrive.Web.Controllers
{
    public class CommonController : Polpware.NopWeb.Controllers.CommonController
    {
        public CommonController(ICommonModelFactory commonModelFactory,
            ILanguageService languageService,
            ICurrencyService currencyService, ILocalizationService localizationService,
            IWorkContext workContext, IStoreContext storeContext, IThemeContext themeContext,
            IGenericAttributeService genericAttributeService, ICustomerActivityService customerActivityService,
            IVendorService vendorService, IWorkflowMessageService workflowMessageService,
            ILogger logger, StoreInformationSettings storeInformationSettings, CommonSettings commonSettings,
            LocalizationSettings localizationSettings, CaptchaSettings captchaSettings, VendorSettings vendorSettings)
            : base(commonModelFactory, languageService, currencyService, localizationService, workContext,
                  storeContext, themeContext, genericAttributeService, customerActivityService, vendorService,
                  workflowMessageService, logger, storeInformationSettings, commonSettings, localizationSettings,
                  captchaSettings, vendorSettings)
        {
        }

        // Page not found
        [HttpGet("page-not-found", Name ="PageNotFound")]
        public override IActionResult PageNotFound()
        {
            return base.PageNotFound();
        }

        // Coming soon page
        [HttpGet("/pending-page", Name = "PendingPage")]
        public IActionResult PendingPage()
        {
            return base.PageNotFound();
        }

        // contact us
        [HttpGet("contactus", Name="ContactUs")]
        [HttpsRequirement(SslRequirement.Yes)]
        public override IActionResult ContactUs()
        {
            return base.ContactUs();
        }

        [HttpPost, ActionName("ContactUs")]
        [PublicAntiForgery]
        [ValidateCaptcha]
        public override IActionResult ContactUsSend(ContactUsModel model, bool captchaValid)
        {
            return base.ContactUsSend(model, captchaValid);
        }

        [HttpGet("aboutus", Name = "AboutUs")]
        public IActionResult Aboutus()
        {
            return View();
        }

        [HttpGet("career", Name = "Career")]
        public IActionResult Career()
        {
            return View();
        }


        [HttpGet("career-directormarketing", Name = "DirectorMarketing")]
        public IActionResult DirectorMarketing()
        {
            return View();
        }

        [HttpGet("career-marketing", Name = "MarketingSpecialist")]
        public IActionResult MarketingSpecialist()
        {
            return View();
        }

        [HttpGet("career-rscientist", Name = "ResearchScientist")]
        public IActionResult ResearchScientist()
        {
            return View();
        }

        [HttpGet("career-srscientist", Name = "SoftwareScientist")]
        public IActionResult SoftwareScientist()
        {
            return View();
        }

        [HttpGet("career-engineer", Name = "SoftwareEngineer")]
        public IActionResult SoftwareEngineer()
        {
            return View();
        }

        [HttpGet("business", Name = "Business")]
        public IActionResult Business()
        {
            return View();
        }

        [HttpGet("help-center", Name = "HelpCenter")]
        public IActionResult HelpCenter()
        {
            return View();
        }

        [HttpGet("download-center", Name = "DownloadCenter")]
        public IActionResult DownloadCenter()
        {
            return View();
        }

        // sitemap page
        [HttpGet("sitemap.xml", Name ="sitemap.xml")]
        [HttpsRequirement(SslRequirement.No)]
        public override IActionResult Sitemap(SitemapPageModel pageModel)
        {
            return base.Sitemap(pageModel);
        }

        [HttpGet("sitemap-{Id:min(0)}.xml", Name = "sitemap-indexed.xml")]
        [HttpsRequirement(SslRequirement.No)]
        public override IActionResult SitemapXml(int? id)
        {
            return base.SitemapXml(id);
        }

        [HttpPost]
        [Route("EuCookieLawAccept", Name = "eucookielawaccept")]
        public override IActionResult EuCookieLawAccept()
        {
            return base.EuCookieLawAccept();
        }

        //robots.txt file
        //available even when a store is closed
        [HttpGet("robots.txt", Name = "robots.txt")]
        public override IActionResult RobotsTextFile()
        {
            return base.RobotsTextFile();
        }

        public override IActionResult GenericUrl()
        {
            return base.GenericUrl();
        }

        [HttpGet("storeclosed", Name = "StoreClosed")]
        public override IActionResult StoreClosed()
        {
            return base.StoreClosed();
        }

        // Helper method
        public override IActionResult InternalRedirect(string url, bool permanentRedirect)
        {
            return base.InternalRedirect(url, permanentRedirect);
        }
    }
}
