using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Catalog;
using NXdrive.NopExt.ServicePlans;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.Customer;
using Polpware.NopExt.SubsPlan.Orders;
using Polpware.NopWeb.Factories;
using Polpware.NopWeb.Models.Catalog;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Controllers
{
    public partial class ProductController : Nop.Web.Framework.Controllers.BaseController
    {

        private readonly IProductService _productService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IServicePlanModelFactory _servicePlanModelFactory;
        private readonly IPlanSpecificationService _planSpecificationService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;

        public ProductController(IProductService productService,
            IProductModelFactory productModelFactory,
            IWorkContext workContext,
            IServicePlanModelFactory servicePlanModelFactory,
            SubstOrientedOrderAccessService substOrientedOrderAccessService,
            IPlanSpecificationService planSpecificationService) : base()
        {
            _productService = productService;
            _productModelFactory = productModelFactory;
            _workContext = workContext;
            _servicePlanModelFactory = servicePlanModelFactory;
            _planSpecificationService = planSpecificationService;
            _substOrientedOrderAccessService = substOrientedOrderAccessService;
        }

        [HttpGet("plan-comparision", Name = "ServicePlans")]
//        [HttpGet("/pricing")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("plans", Name = "ProductPlans")]
        public IActionResult Products()
        {
            Customer currentCustomer = null;
            Product currentProduct = null;
            if (_workContext.CurrentCustomer.IsRegistered())
                currentCustomer = _workContext.CurrentCustomer;
            if (currentCustomer != null)
            {
                var order = _substOrientedOrderAccessService.EffectiveOrder(currentCustomer);
                if (order != null && order.OrderItems != null && order.OrderItems.Count() > 0)
                {
                    var currentOrder = order.OrderItems.FirstOrDefault();
                    currentProduct = _productService.GetProductById(currentOrder.ProductId);
                }
            }

            var products = _productService.SearchProducts();
            var models = new List<ServicePlanDetailModel>();
            foreach (var product in products)
            {
                var customerProductModel = _servicePlanModelFactory.PrepareServicePlanDetailModel(product, currentProduct);
                models.Add(customerProductModel);
            }

            // todo: can order by other fields in the future
            models = models.OrderBy(x => x.ProductPrice.PriceValue).ToList();

            return View(models);
        }

        [HttpGet("feature1", Name = "Security")]
        //        [HttpGet("/security")]
        public IActionResult FeatureX()
        {
            return View();
        }

        [HttpGet("features", Name = "Features")]
        //        [HttpGet("/security")]
        public IActionResult Feature()
        {
            return View();
        }

        [HttpGet("feature2", Name = "Group")]
        public IActionResult FeatureY()
        {
            return View();
        }

        [HttpGet("feature3")]
        public IActionResult FeatureZ()
        {
            return View("FeatureZ.cshtml");
        }

        [HttpGet("feature4")]
        public IActionResult FeatureW()
        {
            return View("FeatureW.cshtml");
        }

        [HttpGet("term", Name = "Term")]
        public IActionResult Term()
        {
            return View();
        }
    }
}