using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Polpware.NopWeb.Models.Customer;

namespace NXdrive.Web.Controllers
{
    partial class CustomerController
    {
        [Route("customer/addresses", Name = "CustomerAddresses")]
        [AcceptVerbs("GET")]
        public override IActionResult Addresses()
        {
            return base.Addresses();
        }

        [Route("customer/addresses/create", Name = "CustomerAddAddress")]
        [AcceptVerbs("GET")]
        public override IActionResult AddressAdd()
        {
            return base.AddressAdd();
        }

        [Route("customer/addresses/create", Name = "CustomerAddAddress")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public override IActionResult AddressAdd(CustomerAddressEditModel newModel)
        {
            return base.AddressAdd(newModel);
        }

        [Route("customer/addresses/edit/{addressId:min(0)}", Name = "CustomerEditAddress")]
        [AcceptVerbs("GET")]
        public override IActionResult AddressEdit(int addressId)
        {
            return base.AddressEdit(addressId);
        }

        [Route("customer/addresses/edit/{addressId:min(0)}", Name = "CustomerEditAddress")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public override IActionResult AddressEdit(CustomerAddressEditModel newModel, int addressId)
        {
            return base.AddressEdit(newModel, addressId);
        }

        [Route("customer/addresses/delete", Name = "CustomerDeleteAddress")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public override IActionResult AddressDelete(int addressId)
        {
            return base.AddressDelete(addressId);
        }

    }

}