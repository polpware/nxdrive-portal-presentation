﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;

namespace NXdrive.Web.Controllers
{
    public partial class HomeController : Nop.Web.Framework.Controllers.BaseController
    {
        [HttpsRequirement(SslRequirement.Yes)]
        [HttpGet("", Name = "HomePage")]
        public IActionResult Index()
        {
            return View();
        }
    }

}