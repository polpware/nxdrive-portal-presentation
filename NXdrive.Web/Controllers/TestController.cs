﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Autofac;

namespace NXdrive.Web.Controllers
{
    public class TestController : Nop.Web.Framework.Controllers.BaseController
    {
        class SomeThing {
            public void DoWork()
            {
                var engine = Nop.Core.Infrastructure.EngineContext.Current as Nop.Core.Infrastructure.NopEngine;

                using (var scope = engine.Container.BeginLifetimeScope())
                {
                    var service = scope.Resolve<Nop.Services.Messages.IWorkflowMessageService>();
                    service.SendTestEmail(1, "xxlongtang@gmail.com", new List<Nop.Services.Messages.Token>(), 1);
                }
            }
        }

        [HttpGet("/ping", Name = "TestPing")]
        public IActionResult Ping()
        {
            BackgroundJob.Enqueue(() => new SomeThing().DoWork());

            return Content("Message sent out");
        }
    }
}
