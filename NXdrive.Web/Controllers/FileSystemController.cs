﻿using Autofac;
using DotNetOpenAuth.OAuth2.Messages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Polpware.OAuth2.Services;
using System;
using System.Collections.Generic;

namespace NXdrive.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    public class FileSystemController : Nop.Web.Framework.Controllers.BaseController
    {
        private const string ClientIdentifier = "nxdriveapp";
        private const int DefaultDurationHours = 24 * 30;

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IUserResourceAuthServerHost _authHost;
        private readonly Configurations.FileSystemEngineSettings _fileSystemEngineSettings;

        public FileSystemController(IWorkContext workContext, 
            IStoreContext storeContext, 
            IUserResourceAuthServerHost authHost,
            Configurations.FileSystemEngineSettings fileSystemEngineSettings)
        {
            _workContext = workContext;
            _storeContext = storeContext;
            _authHost = authHost;
            _fileSystemEngineSettings = fileSystemEngineSettings;
        }

        [HttpGet("filesystem/explorer", Name = "FileSystemExplorer")]
        public IActionResult FileExplorer(string redirectUrl = "")
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return RedirectToRoute("Login", new { returnUrl = redirectUrl });
            }
            var url = $"{_fileSystemEngineSettings.Host}/{_fileSystemEngineSettings.WebAppPath}";
            if (!string.IsNullOrEmpty(redirectUrl))
            {
                url = url + "?redirectUrl=" + redirectUrl;
            }

            var baseUri = Url.RouteUrl("OAuthAuthorizeRet");
            baseUri = QueryHelpers.AddQueryString(baseUri, "client_id", ClientIdentifier);
            baseUri = QueryHelpers.AddQueryString(baseUri, "redirect_uri", url);
            baseUri = QueryHelpers.AddQueryString(baseUri, "scope", "all");
            baseUri = QueryHelpers.AddQueryString(baseUri, "response_type", "code");
            baseUri = QueryHelpers.AddQueryString(baseUri, "isApproved", "true");

            return Redirect(baseUri);
        }

        public class AccessTokenRequest : IAccessTokenRequest
        {
            public AccessTokenRequest(string clientId, bool authenticated, string userName)
            {
                ClientIdentifier = clientId;
                ClientAuthenticated = authenticated;
                UserName = UserName;

                ExtraData = new Dictionary<string, string>();
                Scope = new HashSet<string>();
            }

            public Version Version => new Version(2, 0);

            public IDictionary<string, string> ExtraData { get; private set; }

            public bool ClientAuthenticated { get; private set; }

            public string ClientIdentifier { get; private set; }

            public string UserName { get; private set; }

            public HashSet<string> Scope { get; }

            public void EnsureValidMessage()
            {
                return;
            }
        }
    }
}
