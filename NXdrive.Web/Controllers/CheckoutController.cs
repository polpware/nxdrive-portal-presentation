﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Http.Extensions;
using System;
using System.Linq;

namespace NXdrive.Web.Controllers
{
    using Nop.Core;
    using Nop.Core.Domain.Common;
    using Nop.Core.Domain.Customers;
    using Nop.Core.Domain.Orders;
    using Nop.Core.Domain.Payments;
    using Nop.Core.Domain.Shipping;
    using Nop.Services.Authentication;
    using Nop.Services.Catalog;
    using Nop.Services.Common;
    using Nop.Services.Customers;
    using Nop.Services.Directory;
    using Nop.Services.Localization;
    using Nop.Services.Logging;
    using Nop.Services.Messages;
    using Nop.Services.Orders;
    using Nop.Services.Payments;
    using Nop.Services.Plugins;
    using Nop.Services.Shipping;
    using Nop.Web.Framework.Mvc.Filters;
    using Nop.Web.Framework.Security;
    using NXdrive.NopExt.ServicePlans;
    using Polpware.NopExt.SubsPlan.Orders;
    using Polpware.NopExt.Presentation.SubsPlan;
    using Polpware.NopWeb.Factories;
    using Polpware.NopWeb.Models.Checkout;
    using BaseController = Polpware.NopWeb.Controllers.CheckoutController;
    using BaseProcessPaymentRequest = Nop.Services.Payments.ProcessPaymentRequest;
    using IPolpwareOrderProcessingService = Polpware.NopExt.SubsPlan.Orders.IOrderProcessingService;
    using ProcessPaymentRequest = Polpware.NopExt.SubsPlan.Payments.ProcessPaymentRequest;
    using NXdrive.Web.Models.Customer;

    [HttpsRequirement(SslRequirement.Yes)]
    public class CheckoutController : BaseController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly OrderSettings _orderSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IProductService _productService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IPaymentService _paymentService;
        private readonly IWebHelper _webHelper;
        private readonly ICheckoutModelFactory _checkoutModelFactory;

        private readonly IPolpwareOrderProcessingService _orderProcessingService;
        private readonly IPlanSpecificationService _planSpecificationService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;

        public CheckoutController(ICheckoutModelFactory checkoutModelFactory,
            IWorkContext workContext,
            IStoreContext storeContext,
            IShoppingCartService shoppingCartService,
            ILocalizationService localizationService,
            IProductAttributeParser productAttributeParser,
            IProductService productService,
            IPolpwareOrderProcessingService orderProcessingService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService,
            IPaymentService paymentService,
            IPluginFinder pluginFinder,
            ILogger logger,
            IOrderService orderService,
            IWebHelper webHelper,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            OrderSettings orderSettings,
            RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings,
            ShippingSettings shippingSettings,
            AddressSettings addressSettings,
            CustomerSettings customerSettings,
            IWorkflowMessageService workFlowMessageService,
            IAuthenticationService authenticationService,
            IPlanSpecificationService planSpecificationService,
            SubstOrientedOrderAccessService substOrientedOrderAccessService) :
            base(checkoutModelFactory, workContext,
                storeContext, shoppingCartService,
                localizationService, productAttributeParser,
                productService, orderProcessingService,
                customerService, genericAttributeService,
                countryService, stateProvinceService,
                shippingService, paymentService,
                pluginFinder, logger,
                orderService, webHelper,
                addressAttributeParser, addressAttributeService,
                orderSettings, rewardPointsSettings,
                paymentSettings, shippingSettings,
                addressSettings, customerSettings)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._orderSettings = orderSettings;
            this._genericAttributeService = genericAttributeService;
            this._productService = productService;
            this._paymentService = paymentService;
            this._webHelper = webHelper;
            this._checkoutModelFactory = checkoutModelFactory;

            this._orderProcessingService = orderProcessingService;
            this._workflowMessageService = workFlowMessageService;
            this._authenticationService = authenticationService;
            this._planSpecificationService = planSpecificationService;

            this._substOrientedOrderAccessService = substOrientedOrderAccessService;
        }

        [Route("checkout", Name = "CheckoutOnePage")]
        //[Route("purchase", Name = "ShoppingCart")]
        [AcceptVerbs("GET")]
        public override IActionResult OnePageCheckout()
        {
            //Very important to clear session information.
            //Start a new guest or signed in user.
            //Ensure the following steps apply to a fresh new guest user or the current signed in user.
            // HttpContext.Session.Remove("GuestEmail");

            // todo: Only allow signed in user
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == Nop.Core.Domain.Orders.ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            if (!cart.Any())
            {
                return RedirectToRoute("ProductPlans");
            }

            // ServicePlanDetailModel servicePlanDetailModel;
            var model = _checkoutModelFactory.PrepareOnePageCheckoutModel(cart);

            // Refine model
            var isPaymentRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
            if (isPaymentRequired)
            {
                return View("OnePageCheckout", model);
            }

            return View("OnePageCheckoutWithoutPayment", model);
        }

        [Route("checkout/save-billing-address", Name = "CheckoutSaveBilling")]
        [AcceptVerbs("POST")]
        public override IActionResult OpcSaveBilling(CheckoutBillingAddressModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();
            return base.OpcSaveBilling(model);
        }

        [Route("checkout/save-payment-method", Name = "CheckoutSavePaymentMethod")]
        [AcceptVerbs("POST")]
        public override IActionResult OpcSavePaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();
            return base.OpcSavePaymentMethod(paymentmethod, model);
        }

        [Route("checkout/save-payment-info", Name = "CheckoutSavePaymentInfo")]
        [AcceptVerbs("POST")]
        public override IActionResult OpcSavePaymentInfo(IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();
            return base.OpcSavePaymentInfo(form);
        }

        [Route("checkout/confirm-order", Name = "CheckoutConfirmOrder")]
        [AcceptVerbs("POST")]
        public override IActionResult OpcConfirmOrder()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            try
            {
                var effectiveCustomer = this._workContext.CurrentCustomer;

                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == Nop.Core.Domain.Orders.ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                if (!cart.Any())
                {
                    return RedirectToRoute("ProductPlans");
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(effectiveCustomer))
                {
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));
                }

                //place order
                var baseProcessPaymentRequest = HttpContext.Session.Get<BaseProcessPaymentRequest>("OrderPaymentInfo");
                var processPaymentRequest = baseProcessPaymentRequest.SafelyAs();
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered.");
                    }

                    processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = effectiveCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = effectiveCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);

                // Possibly we have to update the guest, if the current user is a guest. As
                // we may have already changed this guest's shopping items and addresses.
                // TODO:
                //var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                // processPaymentRequest.Credit = (decimal)_customerService.GetCustomerEffectiveOrderCredit(customer);

                // will create either a monthly order or yearly order
                var placeOrderResult = _orderProcessingService.PlaceOrderWrapper(processPaymentRequest);

                // TODO: We do not clear cart at all.
                if (placeOrderResult.Success)
                {
                    HttpContext.Session.Remove("OrderPaymentInfo");
                    // Note that we do not need the following post payment processing

                    /*
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod != null)
                    {
                        if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                        {
                            //Redirection will not work because it's AJAX request.
                            //That's why we don't process it here (we redirect a user to another page where he'll be redirected)
                            //redirect
                            return Json(new
                            {
                                // todo: What is this???
                                redirect = $"{_webHelper.GetStoreLocation()}checkout/OpcCompleteRedirectionPayment"
                            });
                        }

                        _paymentService.PostProcessPayment(postProcessPaymentRequest);
                        //success
                        return Json(new { success = 1 });
                    } */

                    //payment method could be null if order total is 0
                    // When a free user reaches this point, we have placed an order for him. Next,
                    // we need to send out an activiation email for him.
                    //if (isGuest && isFreeService)
                    //{
                    //    return RedirectToRoute("SendEmailValidation");
                    //}

                    //success
                    return Json(new { success = 1 });
                }

                // At this point, some error happened
                var confirmOrderModel = new CheckoutConfirmModel();
                foreach (var error in placeOrderResult.Errors)
                {
                    confirmOrderModel.Warnings.Add(error);
                }

                // Render confirm order again
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });

            }
            catch (Exception exc)
            {
                // _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [Route("checkout/completed", Name = "CheckoutCompleted")]
        [AcceptVerbs("GET")]
        public override IActionResult Completed(int? orderId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (orderId == null || orderId <= 0)
            {
                // Figure out the recent order
                var orders = _substOrientedOrderAccessService.ActiveOrders(_workContext.CurrentCustomer);
                var order = orders.OrderByDescending(x => x.CreatedOnUtc).FirstOrDefault();
                if (order != null)
                {
                    orderId = order.Id;
                }
            }

            return base.Completed(orderId);
        }
    }
}
