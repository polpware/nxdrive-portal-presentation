using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Common;
using Nop.Services.Orders;
using System.Linq;

namespace NXdrive.Web.Controllers
{
    using Nop.Core.Domain.Customers;
    using Polpware.NopExt.Presentation.SubsPlan.Models;
    using Polpware.NopExt.Presentation.SubsPlan.Services;
    using Polpware.NopExt.SubsPlan.Orders;
    using System.Collections.Generic;
    using BaseOrderController = Polpware.NopExt.Presentation.SubsPlan.Controllers.OrderController;

    public partial class OrderController : BaseOrderController
    {
        private readonly IWorkContext _workContext;
        private readonly PrepareOrderModelService _prepareModelService;
        private readonly IOrderService _orderService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;

        public OrderController(IWorkContext workContext,
            IWebHelper webHelper,
            IPdfService pdfService,
            IOrderService orderService,
            SubstOrientedOrderAccessService substOrientedOrderAccessService,
            PrepareOrderModelService prepareModelService)
            : base(workContext, webHelper, pdfService, orderService, prepareModelService)
        {
            _workContext = workContext;
            _prepareModelService = prepareModelService;
            _orderService = orderService;

            _substOrientedOrderAccessService = substOrientedOrderAccessService;
        }

        // todo: return view of customer's transaction history
        [Route("customer/billing", Name = "OrderBilling")]
        [AcceptVerbs("GET")]
        public virtual IActionResult Billing()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            List<OrderDetailsModel> models = new List<OrderDetailsModel>();

            var orders = _substOrientedOrderAccessService.AllOrders(_workContext.CurrentCustomer);

            if (orders != null && orders.Count() > 0)
            {
                foreach (var order in orders)
                {
                    var model = _prepareModelService.PrepareOrderDetailsModel(order);
                    // model.Items.FirstOrDefault().ProductName;
                    var plan = order.OrderItems.FirstOrDefault();
                    if (plan.UnitPriceInclTax != 0)
                        models.Add(model);
                }
            }

            return View(models);
        }

        [HttpGet("invoice", Name = "Invoice")]
        //        [HttpGet("/invoice")]
        public IActionResult Invoice()
        {
            return View();
        }

        private new IActionResult Details(int orderId)
        {
            return base.Details(orderId);
        }

        private new IActionResult PrintOrderDetails(int orderId)
        {
            return base.PrintOrderDetails(orderId);
        }

        private new IActionResult GetPdfInvoice(int orderId)
        {
            return base.GetPdfInvoice(orderId);
        }


    }
}