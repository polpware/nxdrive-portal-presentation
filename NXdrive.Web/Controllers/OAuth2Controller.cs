﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.WebApiCompatShim;
using Microsoft.AspNetCore.WebUtilities;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Customers;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using NXdrive.Web.Models.OAuth;
using Polpware.EntityFramework;
using Polpware.OAuth2.DataModeling.Domain;
using Polpware.OAuth2.Services;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NXdrive.Web.Controllers
{
    [AllowCrossSiteJson]
    [HttpsRequirement(SslRequirement.Yes)]
    public class OAuth2Controller : BaseController
    {

        const int DefaultBufferSize = 16 * 1024;

        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly AuthorizationServer _userResourceAuthorizationServer;
        private readonly AuthorizationServer _publicResourceAuthorizationServer;
        private readonly IUserResourceAuthServerHost _userResourceAuthServerHost;
        private readonly IRepositoryLike<OAuthAuthorization> _authorizationRepository;

        static OAuth2Controller()
        {
        }

        public OAuth2Controller(IUserResourceAuthServerHost userAuthorizationServer,
            IPublicResourceAuthServerHost publicAuthorizationServer,
            ICustomerService customerService,
            IRepositoryLike<OAuthAuthorization> authorizationRepository,
            IWorkContext workContext)
        {
            _userResourceAuthServerHost = userAuthorizationServer;

            _userResourceAuthorizationServer = new AuthorizationServer(userAuthorizationServer);
            _publicResourceAuthorizationServer = new AuthorizationServer(publicAuthorizationServer);

            _authorizationRepository = authorizationRepository;
            _customerService = customerService;
            this._workContext = workContext;
        }


        /// <summary>
        /// Prompts the user to authorize a client to access the user's private data.
        /// </summary>
        /// <returns>The browser HTML response that prompts the user to authorize the client.</returns>
        [Route("oauth/authorize", Name = "OAuthAuthorize")]
        [HttpPost]
        [HttpGet]
        // todo fix
        // [HttpHeader("x-frame-options", "SAMEORIGIN")] // mitigates click jacking
        public async Task<IActionResult> Authorize()
        {
            // User must have been authenticated.
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            // Construct a http request base
            var httpRequestFeature = new HttpRequestMessageFeature(HttpContext);

            var pendingRequest =
                await this._userResourceAuthorizationServer
                .ReadAuthorizationRequestAsync(httpRequestFeature.HttpRequestMessage, HttpContext.RequestAborted);

            if (pendingRequest == null)
            {
                return new BadRequestResult();
            }

            // See if we can auto auth, only if the client is trusted.
            var authServer = (OAuth2AuthorizationServerBase)_userResourceAuthorizationServer.AuthorizationServerServices;

            // It is a bit complex when MVC 5 and MVC 6 are both supported. We currently do not
            // support the following feature to avoid formatting different outputs directly.

            /*
            if (authServer.CanBeAutoApproved(pendingRequest))
            {
                var approval = this._userResourceAuthorizationServer.PrepareApproveAuthorizationRequest(pendingRequest, HttpContext.User.Identity.Name);
                var response = await this._userResourceAuthorizationServer.Channel.PrepareResponseAsync(approval, HttpContext.RequestAborted);

                // Translate
                var outputFormatter = new HttpResponseMessageOutputFormatter();
                var writerContext = new OutputFormatterWriteContext(HttpContext, null, null, response);

                await outputFormatter.WriteAsync(writerContext);
            } */

            var requestingClient = authServer.GetClientById(pendingRequest.ClientIdentifier);
            if (requestingClient == null)
            {
                return new BadRequestResult();
            }

            var model = new AccountAuthorizeModel
            {
                ClientApp = requestingClient.Name,
                Scope = OAuthUtilities.JoinScopes(pendingRequest.Scope),
                AuthorizationRequest = pendingRequest
            };

            var baseUri = Url.RouteUrl("OAuthAuthorizeRet");
            baseUri = QueryHelpers.AddQueryString(baseUri, "client_id", model.AuthorizationRequest.ClientIdentifier);
            baseUri = QueryHelpers.AddQueryString(baseUri, "redirect_uri", model.AuthorizationRequest.Callback.ToString());
            baseUri = QueryHelpers.AddQueryString(baseUri, "scope", model.Scope);
            baseUri = QueryHelpers.AddQueryString(baseUri, "response_type", "code");

            var yesUrl = QueryHelpers.AddQueryString(baseUri, "isApproved", "true");

            var noUrl = QueryHelpers.AddQueryString(baseUri, "isApproved", "false");

            model.YesUrl = yesUrl;
            model.NoUrl = noUrl;


            return View(model);
        }

        /// <summary>
        /// Processes the user's response as to whether to authorize a Client to access his/her private data.
        /// </summary>
        /// <param name="isApproved">if set to <c>true</c>, the user has authorized the Client; <c>false</c> otherwise.</param>
        /// <returns>HTML response that redirects the browser to the Client.</returns>
        // [HttpPost]
        // todo:
        // [ValidateAntiForgeryToken]
        [Route("oauth/authorize-result", Name = "OAuthAuthorizeRet")]
        public async Task<IActionResult> AuthorizeResponse(bool isApproved)
        {
            // User must have been authenticated.
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return BadRequest();
            }

            // Construct a http request base
            var httpRequestFeature = new HttpRequestMessageFeature(HttpContext);

            var pendingRequest = await this._userResourceAuthorizationServer
                .ReadAuthorizationRequestAsync(httpRequestFeature.HttpRequestMessage, HttpContext.RequestAborted);

            if (pendingRequest == null)
            {
                return BadRequest();
            }

            IDirectedProtocolMessage response;

            if (isApproved)
            {
                var authServer = (OAuth2AuthorizationServerBase)_userResourceAuthorizationServer.AuthorizationServerServices;
                authServer.InsertAuthorizationRecord(pendingRequest.ClientIdentifier, OAuthUtilities.JoinScopes(pendingRequest.Scope));
                response = _userResourceAuthorizationServer.PrepareApproveAuthorizationRequest(pendingRequest, _workContext.CurrentCustomer.Email);
            }
            else
            {
                response = _userResourceAuthorizationServer.PrepareRejectAuthorizationRequest(pendingRequest);

                var model = new AccountAuthorizeModel
                {
                    ClientApp = pendingRequest.ClientIdentifier
                };

                return View(model);
            }

            var preparedResponse = await this._userResourceAuthorizationServer.Channel.PrepareResponseAsync(response, HttpContext.RequestAborted);
            if (preparedResponse.StatusCode == HttpStatusCode.Found)
            {
                var redirectUrl = string.Empty;
                // Exception: localhost/#/test?....
                // QueryHelpers.AddQueryString does not work property; it does not preserve fragment
                var location = preparedResponse.Headers.Location;
                if (!string.IsNullOrEmpty(location.Fragment))
                {
                    redirectUrl = location.Scheme + "://" + location.Host + (location.IsDefaultPort ? string.Empty : ":" + location.Port) + location.AbsolutePath + location.Fragment;
                    if (redirectUrl.Contains("?"))
                    {
                        redirectUrl = redirectUrl + "&" + location.Query.Substring(1);
                    }
                    else
                    {
                        redirectUrl = redirectUrl + location.Query;
                    }
                }
                else
                {
                    redirectUrl = location.AbsoluteUri;
                }

                return Redirect(redirectUrl);
            }

            return BadRequest();
        }


        [Route("oauth/authorize-share-link-result", Name = "AuthorizeShareLinkResponse")]
        public async Task<IActionResult> AuthorizeShareLinkResponse(string email, bool isApproved)
        {
            // todo: Scope must be share

            // User must have been authenticated.
            if (string.IsNullOrEmpty(email))
            {
                return BadRequest();
            }

            var customer = _customerService.GetCustomerByEmail(email);
            if (customer == null || !customer.Active)
            {
                return BadRequest();
            }

            // Construct a http request base
            var httpRequestFeature = new HttpRequestMessageFeature(HttpContext);

            var pendingRequest = await this._userResourceAuthorizationServer
                .ReadAuthorizationRequestAsync(httpRequestFeature.HttpRequestMessage, HttpContext.RequestAborted);

            if (pendingRequest == null)
            {
                return BadRequest();
            }

            IDirectedProtocolMessage response;

            if (isApproved)
            {
                // Note that we cannot use the authorization server to insert a record, 
                // as the customer in question is not authenticated yet in current session.
                var authServer = (OAuth2AuthorizationServerBase)_userResourceAuthorizationServer.AuthorizationServerServices;
                var client = authServer.GetClientById(pendingRequest.ClientIdentifier);
                // todo: Verify if the client exists or not
                _authorizationRepository.Insert(new OAuthAuthorization()
                {
                    CustomerId = customer.Id,
                    OAuthClientId = client.Id,
                    Scope = OAuthUtilities.JoinScopes(pendingRequest.Scope),
                    IssueDate = DateTime.UtcNow
                });
                response = _userResourceAuthorizationServer.PrepareApproveAuthorizationRequest(pendingRequest, customer.Email);
            }
            else
            {
                response = _userResourceAuthorizationServer.PrepareRejectAuthorizationRequest(pendingRequest);

                var model = new AccountAuthorizeModel
                {
                    ClientApp = pendingRequest.ClientIdentifier
                };

                return View(model);
            }

            var preparedResponse = await this._userResourceAuthorizationServer.Channel.PrepareResponseAsync(response, HttpContext.RequestAborted);
            if (preparedResponse.StatusCode == HttpStatusCode.Found)
            {
                var redirectUrl = string.Empty;
                // Exception: localhost/#/test?....
                // QueryHelpers.AddQueryString does not work property; it does not preserve fragment
                var location = preparedResponse.Headers.Location;
                if (!string.IsNullOrEmpty(location.Fragment))
                {
                    redirectUrl = location.Scheme + "://" + location.Host + (location.IsDefaultPort ? string.Empty : ":" + location.Port) + location.AbsolutePath + location.Fragment;
                    if (redirectUrl.Contains("?"))
                    {
                        redirectUrl = redirectUrl + "&" + location.Query.Substring(1);
                    }
                    else
                    {
                        redirectUrl = redirectUrl + location.Query;
                    }
                }
                else
                {
                    redirectUrl = location.AbsoluteUri;
                }

                return Redirect(redirectUrl);
            }

            return BadRequest();
        }

        [Route("oauth/token", Name = "OAuthToken")]
        public async Task Token()
        {
            // Construct a http request base
            var httpRequestFeature = new HttpRequestMessageFeature(HttpContext);

            var response = await _userResourceAuthorizationServer
                .HandleTokenRequestAsync(httpRequestFeature.HttpRequestMessage, HttpContext.RequestAborted);

            // Translate
            Func<Stream, Encoding, TextWriter> writer = (stream, encoding) => new HttpResponseStreamWriter(stream, encoding, DefaultBufferSize);

            var outputFormatter = new HttpResponseMessageOutputFormatter();
            var writerContext = new OutputFormatterWriteContext(HttpContext, writer, typeof(string), response);

            await outputFormatter.WriteAsync(writerContext);
        }

        [Route("oauth/client-token", Name = "OAuthClientToken")]
        public async Task ClientToken()
        {
            // Construct a http request base
            var httpRequestFeature = new HttpRequestMessageFeature(HttpContext);

            var response = await _publicResourceAuthorizationServer
                .HandleTokenRequestAsync(httpRequestFeature.HttpRequestMessage, HttpContext.RequestAborted);

            // Translate
            Func<Stream, Encoding, TextWriter> writer = (stream, encoding) => new HttpResponseStreamWriter(stream, encoding, DefaultBufferSize);

            var outputFormatter = new HttpResponseMessageOutputFormatter();
            var writerContext = new OutputFormatterWriteContext(HttpContext, writer, typeof(string), response);

            await outputFormatter.WriteAsync(writerContext);
        }

        [Route("oauth/nonce-login", Name = "OAuthNonceLogin")]
        public ActionResult NonceLogin(string token, string redirect)
        {
            string username;
            var ok = _userResourceAuthServerHost.IsOnceTokenValid(token, out username);
            if (ok)
            {
                var authService = EngineContext.Current.Resolve<IAuthenticationService>();
                var customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
                var customerService = EngineContext.Current.Resolve<ICustomerService>();
                // Sign in user
                var customer = customerSettings.UsernamesEnabled ? customerService.GetCustomerByUsername(username) : customerService.GetCustomerByEmail(username);
                if (customer == null)
                {
                    return RedirectToRoute("Login");
                }

                authService.SignIn(customer, false);
                if (string.IsNullOrWhiteSpace(redirect))
                {
                    return RedirectToRoute("CustomerInfo");
                }
                return RedirectToRoute(redirect);
            }
            return RedirectToRoute("Login");
        }

    }
}