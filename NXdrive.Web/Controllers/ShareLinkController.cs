﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;

namespace NXdrive.Web.Controllers
{
    public class ShareLinkController : Nop.Web.Framework.Controllers.BaseController
    {
        private const string ClientIdentifier = "nxdriveapp";

        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly Configurations.FileSystemEngineSettings _fileSystemEngineSettings;

        public ShareLinkController(ICustomerService customerService,
            IWorkContext workContext,
            Configurations.FileSystemEngineSettings fileSystemEngineSettings)
        {
            _customerService = customerService;
            _workContext = workContext;
            _fileSystemEngineSettings = fileSystemEngineSettings;
        }

        [HttpGet("share-link/{owner:GUID}/{guid:GUID}/{fileName}", Name = "ShareLinkAccess")]
        public IActionResult FileExplorer(string owner, string guid, string fileName)
        {
            var ownerUser = _customerService.GetCustomerByGuid(new System.Guid(owner));
            if (ownerUser == null)
            {
                // todo: An error page
                return BadRequest();
            }

            // We do not verify the given share link guid is valid or not here 
            // Instead, we rely on the file system to perform that check.

            var redirectUrl = "/share-link/" + owner + "/" + guid;
            var url = $"{_fileSystemEngineSettings.Host}/{_fileSystemEngineSettings.WebAppPath}";
            url = url + "?redirectUrl=" + redirectUrl;

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                var baseUri = Url.RouteUrl("OAuthAuthorizeRet");
                baseUri = QueryHelpers.AddQueryString(baseUri, "client_id", ClientIdentifier);
                baseUri = QueryHelpers.AddQueryString(baseUri, "redirect_uri", url);
                baseUri = QueryHelpers.AddQueryString(baseUri, "response_type", "code");
                baseUri = QueryHelpers.AddQueryString(baseUri, "scope", "all");
                baseUri = QueryHelpers.AddQueryString(baseUri, "isApproved", "true");

                return Redirect(baseUri);
            }
            else
            {
                var baseUri = Url.RouteUrl("AuthorizeShareLinkResponse");
                baseUri = QueryHelpers.AddQueryString(baseUri, "client_id", ClientIdentifier);
                baseUri = QueryHelpers.AddQueryString(baseUri, "redirect_uri", url);
                baseUri = QueryHelpers.AddQueryString(baseUri, "response_type", "code");
                baseUri = QueryHelpers.AddQueryString(baseUri, "scope", $"sharelink__{guid}_{_workContext.CurrentCustomer.Id}");
                // Extra ones
                baseUri = QueryHelpers.AddQueryString(baseUri, "email", ownerUser.Email);
                baseUri = QueryHelpers.AddQueryString(baseUri, "isApproved", "true");

                return Redirect(baseUri);
            }

        }
    }
}