using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Services.Customers;
using Nop.Services.Forums;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Nop.Core.Domain.Media;
    using Nop.Services.Common;
    using Nop.Services.Localization;
    using Nop.Services.Logging;
    using Nop.Services.Media;
    using Nop.Web.Framework.Security;
    using Polpware.NopWeb.Factories;
    using Polpware.NopWeb.Models.PrivateMessages;
    using BasePrivateMessageController = Polpware.NopWeb.Controllers.PrivateMessagesController;

    [HttpsRequirement(SslRequirement.Yes)]
    public class PrivateMessagesController : BasePrivateMessageController
    {
        private readonly IWorkContext _workContext;
        private readonly IForumService _forumService;
        private readonly ICustomerService _customerService;
        private readonly IStoreContext _storeContext;
        private readonly ForumSettings _forumSettings;
        private readonly IPrivateMessagesModelFactory _privateMessagesModelFactory;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;

        public PrivateMessagesController(IPrivateMessagesModelFactory privateMessagesModelFactory,
            IForumService forumService, ICustomerService customerService, ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ForumSettings forumSettings,
            MediaSettings mediaSettings,
            IPictureService pictureService
            )
            : base(privateMessagesModelFactory, forumService, customerService,
                  customerActivityService, localizationService, workContext, storeContext, forumSettings)
        {
            _forumService = forumService;
            _customerService = customerService;
            _workContext = workContext;
            _storeContext = storeContext;
            _forumSettings = forumSettings;

            _privateMessagesModelFactory = privateMessagesModelFactory;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;

            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
        }

        [Route("customer/messages", Name = "PrivateMessages")]
        [AcceptVerbs("GET")]
        public override IActionResult Index(int? pageNumber, string tab)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            var model = _privateMessagesModelFactory.PreparePrivateMessageIndexModel(pageNumber, tab);

            return View(model);
        }

        [Route("customer/messages/inbox/delete", Name = "PrivateMessageDeleteInbox")]
        [AcceptVerbs("POST")]
        public IActionResult DeleteInboxPMByFormData(IFormCollection formCollection)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            var affectedItems = new Dictionary<int, bool>();

            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("pm", StringComparison.InvariantCultureIgnoreCase))
                {
                    var id = key.Replace("pm", "").Trim();
                    if (int.TryParse(id, out int privateMessageId))
                    {
                        var pm = _forumService.GetPrivateMessageById(privateMessageId);
                        if (pm != null)
                        {
                            if (pm.ToCustomerId == _workContext.CurrentCustomer.Id)
                            {
                                pm.IsDeletedByRecipient = true;
                                _forumService.UpdatePrivateMessage(pm);

                                affectedItems.Add(privateMessageId, true);
                            }
                        }
                    }
                }
            }

            return new JsonResult(new
            {
                Data = affectedItems
            });
        }

        [Route("customer/messages/inbox/mark-unread", Name = "PrivateMessageMarkUnread")]
        [AcceptVerbs("POST")]
        public IActionResult MarkUnreadByFormData(IFormCollection formCollection)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            var affectedItems = new Dictionary<int, bool>();

            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("pm", StringComparison.InvariantCultureIgnoreCase))
                {
                    var id = key.Replace("pm", "").Trim();
                    if (int.TryParse(id, out int privateMessageId))
                    {
                        var pm = _forumService.GetPrivateMessageById(privateMessageId);
                        if (pm != null)
                        {
                            if (pm.ToCustomerId == _workContext.CurrentCustomer.Id)
                            {
                                pm.IsRead = true;
                                _forumService.UpdatePrivateMessage(pm);

                                affectedItems.Add(privateMessageId, true);
                            }
                        }
                    }
                }
            }

            return new JsonResult(new
            {
                Data = affectedItems
            });

        }

        [Route("customer/messages/sent-items/delete", Name = "PrivateMessageDeleteSentPM")]
        [AcceptVerbs("POST")]
        //[PublicAntiForgery]
        public IActionResult DeleteSentPMByFormData(IFormCollection formCollection)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            var affectedItems = new Dictionary<int, bool>();

            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("si", StringComparison.InvariantCultureIgnoreCase))
                {
                    var id = key.Replace("si", "").Trim();
                    if (int.TryParse(id, out int privateMessageId))
                    {
                        var pm = _forumService.GetPrivateMessageById(privateMessageId);
                        if (pm != null)
                        {
                            if (pm.FromCustomerId == _workContext.CurrentCustomer.Id)
                            {
                                pm.IsDeletedByAuthor = true;
                                _forumService.UpdatePrivateMessage(pm);

                                affectedItems.Add(privateMessageId, true);
                            }
                        }
                    }
                }
            }

            return new JsonResult(new
            {
                Data = affectedItems
            });
        }

        [Route("customer/messages/send", Name = "PrivateMessageSendPM")]
        [AcceptVerbs("POST")]
        //[PublicAntiForgery]
        public IActionResult SendPrivateMessage(SendPrivateMessageModel model)
        {

            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            string errorMessage = string.Empty;

            Customer toCustomer;
            var replyToPM = _forumService.GetPrivateMessageById(model.ReplyToMessageId);
            if (replyToPM != null)
            {
                //reply to a previous PM
                if (replyToPM.ToCustomerId == _workContext.CurrentCustomer.Id || replyToPM.FromCustomerId == _workContext.CurrentCustomer.Id)
                {
                    //Reply to already sent PM (by current customer) should not be sent to yourself
                    toCustomer = replyToPM.FromCustomerId == _workContext.CurrentCustomer.Id
                        ? replyToPM.ToCustomer
                        : replyToPM.FromCustomer;
                }
                else
                {
                    return new JsonResult(new {
                        Error = "Something wrong"
                    });
                }
            }
            else
            {
                //first PM
                toCustomer = _customerService.GetCustomerById(model.ToCustomerId);
            }

            if (toCustomer == null || toCustomer.IsGuest())
            {
                return new JsonResult(new
                {
                    Error = "Cannot find the receiver.",
                });
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var subject = model.Subject;
                    if (_forumSettings.PMSubjectMaxLength > 0 && subject.Length > _forumSettings.PMSubjectMaxLength)
                    {
                        subject = subject.Substring(0, _forumSettings.PMSubjectMaxLength);
                    }

                    var text = model.Message;
                    if (_forumSettings.PMTextMaxLength > 0 && text.Length > _forumSettings.PMTextMaxLength)
                    {
                        text = text.Substring(0, _forumSettings.PMTextMaxLength);
                    }

                    var nowUtc = DateTime.UtcNow;

                    var privateMessage = new PrivateMessage
                    {
                        StoreId = _storeContext.CurrentStore.Id,
                        ToCustomerId = toCustomer.Id,
                        FromCustomerId = _workContext.CurrentCustomer.Id,
                        Subject = subject,
                        Text = text,
                        IsDeletedByAuthor = false,
                        IsDeletedByRecipient = false,
                        IsRead = false,
                        CreatedOnUtc = nowUtc
                    };

                    _forumService.InsertPrivateMessage(privateMessage);

                    //activity log
                    _customerActivityService.InsertActivity("PublicStore.SendPM",
                        string.Format(_localizationService.GetResource("ActivityLog.PublicStore.SendPM"), toCustomer.Email), toCustomer);

                    var newModel = _privateMessagesModelFactory.PreparePrivateMessageModel(privateMessage);
                    var avatarUrl = _pictureService.GetPictureUrl(
                        toCustomer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId),
                        _mediaSettings.AvatarPictureSize,
                        false);
                    newModel.CustomProperties.Add("AvatarUrl", avatarUrl);

                    var html = this.RenderPartialViewToString("_SentPrivateMessage", newModel);

                    return new JsonResult(new
                    {
                        Data = html
                    });
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    ModelState.AddModelError("", ex.Message);
                }
            }
            else
            {
                errorMessage = "Invalid data";
            }

            return new JsonResult(new
            {
                Ok = false,
                Error = errorMessage
            });
        }


    }

}