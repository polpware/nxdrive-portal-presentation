using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;

namespace NXdrive.Web.Controllers
{
    //do not inherit it from BasePublicController. otherwise a lot of extra action filters will be called
    //they can create guest account(s), etc
    public partial class KeepAliveController : Controller
    {
        [HttpsRequirement(SslRequirement.NoMatter)]
        [HttpGet("keepalive", Name = "KeepAlive")]
        public virtual IActionResult Index()
        {
            return Content("I am alive!");
        }
    }
}