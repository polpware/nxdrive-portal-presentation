using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping.Date;
using Nop.Services.Tax;
using Nop.Web.Framework.Security.Captcha;
using Polpware.NopExt.Presentation.SubsPlan.Services;
using Polpware.NopExt.SubsPlan.Orders;
using Polpware.NopWeb.Factories;
using System.Linq;

namespace NXdrive.Web.Controllers
{
    public partial class ShoppingCartController : Polpware.NopExt.Presentation.SubsPlan.ShoppingCartController
    {
        private readonly IProductService _productService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public ShoppingCartController(IShoppingCartModelFactory shoppingCartModelFactory,
            IProductService productService,
            IStoreContext storeContext,
            IWorkContext workContext,
            IShoppingCartService shoppingCartService,
            IPictureService pictureService,
            ILocalizationService localizationService, IProductAttributeService productAttributeService,
            IProductAttributeParser productAttributeParser, ITaxService taxService, ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService, IPriceFormatter priceFormatter,
            ICheckoutAttributeParser checkoutAttributeParser, IDiscountService discountService,
            ICustomerService customerService, IGiftCardService giftCardService,
            IDateRangeService dateRangeService, ICheckoutAttributeService checkoutAttributeService,
            IWorkflowMessageService workflowMessageService, IPermissionService permissionService,
            IDownloadService downloadService, IStaticCacheManager cacheManager,
            IWebHelper webHelper, ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService, MediaSettings mediaSettings,
            ShoppingCartSettings shoppingCartSettings, OrderSettings orderSettings,
            CaptchaSettings captchaSettings, CustomerSettings customerSettings,
            INopFileProvider fileProvider, PrepareOrderModelService prepareModelService, 
            SubstOrientedOrderAccessService substOrientedOrderAccessService)
            : base(shoppingCartModelFactory,
                productService, storeContext, workContext, shoppingCartService, pictureService,
                localizationService, productAttributeService, productAttributeParser, taxService,
                currencyService, priceCalculationService, priceFormatter, checkoutAttributeParser,
                discountService, customerService, giftCardService, dateRangeService,
                checkoutAttributeService, workflowMessageService, permissionService,
                downloadService, cacheManager, webHelper, customerActivityService,
                genericAttributeService, mediaSettings, shoppingCartSettings, orderSettings,
                captchaSettings, customerSettings, fileProvider, prepareModelService)
        {
            this._productService = productService;
            this._shoppingCartService = shoppingCartService;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._storeContext = storeContext;

            this._substOrientedOrderAccessService = substOrientedOrderAccessService;
        }

        // Note that the presence of the "ShoppingCart" route is our intention to
        // be compatible with nopCommerce.
        [HttpGet("checkout/review-plan", Name = "ShoppingCart")]
        public IActionResult ReviewAndChooseService()
        {
            return RedirectToRoute("ProductPlans");
        }

        [HttpPost("checkout/select-plan", Name = "AddProductToCart-Catalog")]
        public override IActionResult AddProductToCart_Catalog(int productId, int shoppingCartTypeId = 1, int quantity = 1, bool forceredirection = false)
        {
            // Note: Very important
            // Hardcode the shopping cart type
            shoppingCartTypeId = 1;

            // And hardcode quantity to be 1
            quantity = 1;

            var validationResult = ValidateShoppingCart(productId);
            if(!validationResult.Equals("OK"))
                return Json(new
                {
                    success = false,
                    message = validationResult
                });

            var cartType = (ShoppingCartType)shoppingCartTypeId;

            var product = _productService.GetProductById(productId);

            //get standard warnings without attribute validations
            //first, try to find existing shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var shoppingCartItem = _shoppingCartService.FindShoppingCartItemInTheCart(cart, cartType, product);
            // if we already have the same product in the cart, then use the total quantity to validate
            // var quantityToValidate = shoppingCartItem != null ? shoppingCartItem.Quantity + quantity : quantity;
            // Not as above, instead, we hardcode the quantity to be 1
            var quantityToValidate = quantity;

            var addToCartWarnings = _shoppingCartService
                .GetShoppingCartItemWarnings(_workContext.CurrentCustomer, cartType,
                product, _storeContext.CurrentStore.Id, string.Empty,
                decimal.Zero, null, null, quantityToValidate, false, shoppingCartItem?.Id ?? 0, true, false, false, false);
            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart
                //let's display standard warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            if (shoppingCartItem != null)
            {
                // question
                addToCartWarnings = _shoppingCartService.UpdateShoppingCartItem(customer: _workContext.CurrentCustomer,
                    shoppingCartItemId: shoppingCartItem.Id,
                    attributesXml: string.Empty,
                    customerEnteredPrice: decimal.Zero,
                    quantity: quantity);
            }
            else
            {
                ClearShoppingCart();
                //now let's try adding product to the cart (now including product attribute validation, etc)
                addToCartWarnings = _shoppingCartService.AddToCart(customer: _workContext.CurrentCustomer,
                    product: product,
                    shoppingCartType: cartType,
                    storeId: _storeContext.CurrentStore.Id,
                    attributesXml: string.Empty,
                    quantity: quantity);
            }

            if (addToCartWarnings.Any())
            {
                //cannot be added to the cart
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //activity log
            _customerActivityService.InsertActivity("PublicStore.AddToShoppingCart",
                string.Format(_localizationService.GetResource("ActivityLog.PublicStore.AddToShoppingCart"), product.Name), product);

            // Start to check out
            return Json(new
            {
                redirect = Url.RouteUrl("CheckoutOnePage")
            });
        }

        [HttpPost("checkout/update-plan", Name = "UpdateProductToCart-Catalog")]
        public override ActionResult UpdateProductToCart_Catalog(int productId, int shoppingCartTypeId, int quantity)
        {
            return base.UpdateProductToCart_Catalog(productId, shoppingCartTypeId, quantity);
        }

        /// <summary>
        /// clear shopping cart before making write operations to database when checkout a plan
        /// </summary>
        /// <returns></returns>
        private void ClearShoppingCart()
        {
            // shopping cart should always be empty because add cart and checkout in nxdrive is one step
            var cart = _workContext.CurrentCustomer.ShoppingCartItems.ToList();
            if (cart.Count > 0)
            {
                // Delete old items
                foreach (var item in cart)
                {
                    _shoppingCartService.DeleteShoppingCartItem(item, false);
                }
            }
        }

        /// <summary>
        /// validte the product that customer wants to add to the shopping cart
        /// case 1: product is not found from the first place
        /// case 2: product that they wish to add is already their enrolled plan
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        private string ValidateShoppingCart(int productId)
        {
            Customer currentCustomer = null;
            Product currentProduct = null;

            var product = _productService.GetProductById(productId);
            if (product == null)
            {
                // case : cannot find product using id
                return "Cannot find product with id " + productId;
            }

            if (_workContext.CurrentCustomer.IsRegistered())
                currentCustomer = _workContext.CurrentCustomer;
            if (currentCustomer != null)
            {
                var order =  _substOrientedOrderAccessService.EffectiveOrder(currentCustomer);
                if (order != null && order.OrderItems != null && order.OrderItems.Count() > 0)
                {
                    var currentOrder = order.OrderItems.FirstOrDefault();
                    currentProduct = _productService.GetProductById(currentOrder.ProductId);
                    if (currentProduct.Id == productId)
                        return "Plan is already enrolled"; // case : plan is already enrolled, no need to buy again
                }
            }

            return "OK";
        }
    }
}