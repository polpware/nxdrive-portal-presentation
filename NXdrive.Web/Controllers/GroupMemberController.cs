﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Forums;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Constraints;
using NXdrive.Web.Extensions;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.Shared;
using NXdrive.Web.Models.UserGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    [EnableCors("AllowLocalHost")]
    public partial class GroupMemberController : BaseController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IUserGroupService _userGroupService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IMemberInvitationService _memberInvitationService;
        private readonly IUserIdentityService _userIdentityService;

        private readonly IGroupMemberService _groupMemberService;
        private readonly CustomerSettings _customerSettings;
        private readonly MediaSettings _mediaSettings;

        private readonly IForumService _forumService;

        // Constraint service
        private readonly IUserGroupConstraints _userGroupConstraints;
        private readonly IUserGroupModelFactory _userGroupModelFactory;
        private readonly IUserGroupMemberModelFactory _userGroupMemberModelFactory;

        private readonly IGenericAttributeService _generateAttributeService;

        // fields to improve performance of listing groups
        public int maxPreviewMemberCount = 3;  // maximum number of group members that shows in the member preview tool tip
        private int maxPreviewGroupCount = 5;   // if there are more than a certain groups that user is in, only show the owner in member preview tool tip

        public GroupMemberController(IWorkContext workContext,
            IStoreContext storeContext,
            IUserGroupService userGroupService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            ICustomerService customerService,

            IForumService forumService,

            // IWorkflowMessageService workflowMessageService,
            CustomerSettings customerSettings,
            MediaSettings mediaSettings,
            IUserGroupConstraints userGroupConstraints,
            IUserIdentityService userIdentityService,
            IGroupMemberService groupMemberService,
            IMemberInvitationService memberInvitationService,
            IUserGroupModelFactory userGroupModelFactory,
            IUserGroupMemberModelFactory userGroupMemberFactory,
            IGenericAttributeService generateAttributeService)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._userGroupService = userGroupService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._customerService = customerService;

            this._forumService = forumService;

            this._userIdentityService = userIdentityService;
            this._groupMemberService = groupMemberService;
            // this._workflowMessageService = workflowMessageService;

            this._customerSettings = customerSettings;
            this._mediaSettings = mediaSettings;

            this._userGroupConstraints = userGroupConstraints;
            this._memberInvitationService = memberInvitationService;
            this._userGroupModelFactory = userGroupModelFactory;
            this._userGroupMemberModelFactory = userGroupMemberFactory;

            this._generateAttributeService = generateAttributeService;
        }


        #region group members
        [HttpPost("user-group/list-members")]
        public async Task<ActionResult> ListMembers(int id, int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            var group = await _userGroupService.GetGroupById(id);

            // todo: Can this user view the members of this group

            var members = await _groupMemberService.GetGroupMembers(id, pageIndex: pageIndex, pageSize: pageSize);

            // Models to be built
            var models = new List<MemberModel>();

            foreach (var m in members)
            {
                // todo: Remove isDeleted, as our service will NOT take any deleted member into consideration. ????
                if (m.IsDeleted)
                    continue;

                var memberModel = new MemberModel();
                _userGroupMemberModelFactory.PrepareGroupMemberModel(group, m, memberModel);
                models.Add(memberModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        [HttpDelete("user-group/delete-member/{id:min(1)}")]
        public async Task<ActionResult> DeleteMember(int id)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // Verify if the given member is here
            var membership = await this._groupMemberService.GetGroupMemberById(id);    // get all joined member in the group
            if (membership == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_GroupNotFound,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, id)
                })
                {
                    StatusCode = 404
                };
            }
            var customer = _workContext.CurrentCustomer;

            // Verify if the user is allowed to do so
            var group = await this._userGroupService.GetGroupById(membership.GroupId);
            // At this point, group shall make sense. Thus we do not need to check if it is null

            // cannot delete the owner itself
            if (membership.CustomerId == group.CreatorId)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_CannotRemoveOwner,
                    errorInfo = ResourceStrings.UserGroup_CannotRemoveOwner
                })
                {
                    StatusCode = 401
                };
            }

            // Owner is managing the membership
            if (customer.Id == group.CreatorId)
            {
                // delete the user
                await this._groupMemberService.DeleteGroupMember(membership);

                // todo: Do we need to do so below
                // also delete all invitations of this group to this user
                var allUserInvitations = (await this._memberInvitationService.SearchInvitationAndApplicationsByGroup(group.Id))
                    .Where(x => x.UserId == membership.CustomerId);
                foreach (var inv in allUserInvitations)
                {
                    await this._memberInvitationService.DeleteInvitationOrApplication(inv);
                }
            }
            else if (customer.Id == membership.CustomerId) // The user himself/herself is issuing the deletion op
            {
                await this._groupMemberService.DeleteGroupMember(membership);
            }
            else
            {
                // no permission
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_CannotRemoveOwner,
                    errorInfo = ResourceStrings.UserGroup_CannotRemoveOwner
                })
                {
                    StatusCode = 401
                };
            }

            // We have successfully processed the request
            return Ok();
        }

        #endregion

        #region group invitation and applications
        [HttpPost("user-group/list-impending-members")]
        public async Task<ActionResult> ListImpendingMembers(int id, int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;
            var group = await _userGroupService.GetGroupById(id);
            if (group == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_GroupNotFound,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, id)
                })
                {
                    StatusCode = 404
                };
            }
            // todo: Can this user view the members of this group?

            var members = await _memberInvitationService.SearchInvitationAndApplicationsByGroup(id, pageIndex: pageIndex, pageSize:pageSize);

            // Models to be built
            var models = new List<InvitationModel>();

            foreach (var m in members)
            {
                // todo: Remove isDeleted, as our service will NOT take any deleted member into consideration. ????
                if (m.IsDeleted)
                    continue;

                var memberModel = new InvitationModel();
                await _userGroupMemberModelFactory.PrepareGroupInvitationModel(group, m, memberModel);
                models.Add(memberModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        [HttpDelete("user-group/delete-impending-member/{id:min(1)}")]
        public async Task<ActionResult> DeleteImpendingMember(int id)
        {
            // this method maybe called by either application sender, or group owner
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var record = await this._memberInvitationService.GetInvitationOrApplicationById(id);
            if (record == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 404
                };
            }

            var group = await this._userGroupService.GetGroupById(record.GroupId);

            // Owner to this group
            if (group.CreatorId == customer.Id)
            {
                // owner can delete people's application, no matter it is decline or not
                await this._memberInvitationService.DeleteInvitationOrApplication(record);
            }
            else if (record.CreatorId == customer.Id) // the one who owns the invitation
            {
                if (record.Declined)
                {
                    // a person cannot take back a declined application
                    // the group owner can delete application
                    return new JsonResult(new
                    {
                        errorCode = (int)GroupConstraintCode.UserGroup_CannotTakeBackDeclinedStuff,
                        errorInfo = ResourceStrings.UserGroup_CannotTakeBackDeclinedStuff
                    })
                    {
                        StatusCode = 422
                    };
                }

                // Go ahead to delete it
                await this._memberInvitationService.DeleteInvitationOrApplication(record);
            } else
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 401
                };
            }

            // Otherwise, Ok
            return Ok();
        }

        #endregion


        #region Classified invitation list for the current customer
        [HttpPost("user-group/list-my-received-applications")]
        public async Task<ActionResult> ListMyReceivedApplications(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var members = await _memberInvitationService.LoadReceivedApplicationsForCustomer(customer.Id, pageIndex, pageSize);

            // Models to be built
            var models = new List<InvitationModel>();

            foreach (var m in members)
            {
                var memberModel = new InvitationModel();
                var group = await _userGroupService.GetGroupById(m.GroupId);
                await _userGroupMemberModelFactory.PrepareGroupInvitationModel(group, m, memberModel);
                if(memberModel.Type == InvitationType.NonMemberApplicant)
                    models.Add(memberModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        [HttpPost("user-group/list-my-received-invitations")]
        public async Task<ActionResult> ListMyReceivedInvitations(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var members = await _memberInvitationService.LoadReceivedInvitationsForCustomer(customer.Id, pageIndex, pageSize);

            // Models to be built
            var models = new List<InvitationModel>();

            foreach (var m in members)
            {
                var memberModel = new InvitationModel();
                var group = await _userGroupService.GetGroupById(m.GroupId);
                await _userGroupMemberModelFactory.PrepareGroupInvitationModel(group, m, memberModel);
                if(memberModel.Type == InvitationType.GroupMemberInvitation ||
                    memberModel.Type == InvitationType.GroupOwnerInvitation)
                    models.Add(memberModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        [HttpPost("user-group/list-my-sent-invitations")]
        public async Task<ActionResult> ListMySentInvitations(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var members = await _memberInvitationService.LoadSentInvitationsForCustomer(customer.Id, pageIndex, pageSize);

            // Models to be built
            var models = new List<InvitationModel>();

            foreach (var m in members)
            {
                var invitationModel = new InvitationModel();
                var group = await _userGroupService.GetGroupById(m.GroupId);
                await _userGroupMemberModelFactory.PrepareGroupInvitationModel(group, m, invitationModel);
                if(invitationModel.Type == InvitationType.GroupOwnerInvitation ||
                    invitationModel.Type == InvitationType.NonMemberApplicant)
                models.Add(invitationModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        [HttpPost("user-group/list-my-sent-applications")]
        public async Task<ActionResult> ListMySentApplications(int pageIndex = 0, int pageSize = 25)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            var members = await _memberInvitationService.LoadSentApplicationsForCustomer(customer.Id, pageIndex, pageSize);

            // Models to be built
            var models = new List<InvitationModel>();

            foreach (var m in members)
            {
                var memberModel = new InvitationModel();
                var group = await _userGroupService.GetGroupById(m.GroupId);
                await _userGroupMemberModelFactory.PrepareGroupInvitationModel(group, m, memberModel);
                if(memberModel.Type == InvitationType.NonMemberApplicant)
                    models.Add(memberModel);
            }

            return Json(new
            {
                total = members.TotalCount,
                offset = members.PageIndex * members.PageSize,
                data = models
            });
        }

        #endregion

        #region Invitation and application
        /// <summary>
        /// need to parse email address string into a list of emails
        /// so that one call can send out multiple invitations
        /// for the timebeing, don't send emails to non-nxdrive email addresses
        /// In the future: use two templates for email, one for registered users, the other is for non-nxdrive email addresses
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="emailList">huge email list, will be parsed into a list of emails</param>
        /// <param name="content">invitation message that group manager will see</param>
        /// <returns></returns>
        [HttpPost("user-group/invite-friends")]
        public async Task<ActionResult> InviteFriends(InvitePeopleIntoGroupModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // Only registered user has this permission
            var customer = _workContext.CurrentCustomer;

            var group = await this._userGroupService.GetGroupById(model.GroupId);
            if (group == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_GroupNotFound,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, model.GroupId)
                })
                {
                    StatusCode = 404
                };
            }

            // todo: Validate if this group has enough room for new members further
            // group should not be full at the moment we are about to send invitations
            var code = await this._userGroupConstraints.CanInvitePeople(group);
            if (code != GroupConstraintCode.NoError)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_GroupFull,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupFull, group.Name)
                })
                {
                    StatusCode = 422
                };
            }

            var currentMembers = await _groupMemberService.GetGroupMembers(model.GroupId);
            var impendingMembers = await _memberInvitationService
                .SearchInvitationAndApplicationsByGroup(model.GroupId);

            // Validate if we can create invitations for all emails
            var candidateList = new List<Object>();
            foreach (var address in model.EmailList)
            {
                var user = _customerService.GetCustomerByEmail(address);

                if (user != null) // nxdrive user
                {
                    // user is registered user
                    // first check duplicate member
                    var anyMember = currentMembers
                        .Where(x => x.CustomerId == user.Id)
                        .FirstOrDefault();

                    if (anyMember != null)
                    {
                        return new JsonResult(new
                        {
                            errorCode = (int)GroupConstraintCode.UserGroup_PeopleAlreadyJoined,
                            errorInfo = String.Format(ResourceStrings.UserGroup_PeopleAlreadyJoined, user.Username)
                        })
                        {
                            StatusCode = 422
                        };
                    }

                    // Check if we have any invitation for this user
                    var anyInvitation = impendingMembers
                        .Where(y => y.UserId == user.Id
                        && y.ProviderId == (short)PredefinedUserProvider.NXdriveAccount
                        && y.IsDeleted == false)
                        .FirstOrDefault();

                    if (anyInvitation != null)
                    {
                        // differeniate already-invited and invitation denied
                        var message = anyInvitation.Declined ?
                            String.Format(ResourceStrings.UserGroup_InvitationDeclined, address) :
                            String.Format(ResourceStrings.UserGroup_AlreadyInvited, address);

                        var err = anyInvitation.Declined ?
                            GroupConstraintCode.UserGroupMember_DeclinedInvitation :
                            GroupConstraintCode.UserGroup_AlreadyInvited;

                        return new JsonResult(new
                        {
                            errorCode = (int)err,
                            errorInfo =  message
                        })
                        {
                            StatusCode = 422
                        };
                    }

                    // Ok
                    candidateList.Add(user);
                }
                else // Try to use Email
                {
                    // not an address registered to nxdrive, send register and group invitation using another template
                    var emailEntry = await _userIdentityService.FindEmailIdentity(address);
                    if (emailEntry == null)
                    {
                        // Ok
                        candidateList.Add(address);
                        continue;
                    }

                    var anyInvitation = impendingMembers
                        .Where(y => y.UserId == emailEntry.Id
                        && y.ProviderId == (short)PredefinedUserProvider.Email
                        && y.IsDeleted==false)
                        .FirstOrDefault();

                    if (anyInvitation != null)
                    {
                        // differeniate already-invited and invitation denied
                        var message = anyInvitation.Declined ?
                            String.Format(ResourceStrings.UserGroup_InvitationDeclined, address) :
                            String.Format(ResourceStrings.UserGroup_AlreadyInvited, address);

                        return new JsonResult(new
                        {
                            errorCode = (int)GroupConstraintCode.UserGroup_AlreadyInvited,
                            errorInfo = message
                        })
                        {
                            StatusCode = 422
                        };
                    }

                    // Ok
                    candidateList.Add(emailEntry);
                }
            }

            // We are a bit greedy, we try for each ....
            var newInvitationList = new List<IUserGroupInvitation>();
            foreach (var item in candidateList)
            {
                try
                {
                    if (item is Customer)
                    {
                        var user = item as Customer;

                        var newInvitation = await this._memberInvitationService
                            .CreateInvitationForCustomer(customer.Id, model.GroupId, user.Id, model.Content);

                        newInvitationList.Add(newInvitation);
                    }
                    else if (item is IEmailIdentity) // Try to use Email
                    {
                        var emailEntry = item as IEmailIdentity;
                        var newInvitation = await this._memberInvitationService
                            .CreateInvitationForEmail(customer.Id, model.GroupId, emailEntry.Id, model.Content);

                        // todo: right after sending invitation, write a record on server to build relation between email
                        // var actionUrl = Url.RouteUrl("RespondToGroupInvitation", new { groupId = groupId });
                        //var emailSent = this._workflowMessageService.SendGroupInvitationMessage(_workContext.CurrentCustomer, address, actionUrl, group.Name, this._workContext.WorkingLanguage.Id, content);

                        newInvitationList.Add(newInvitation);
                    }
                    else
                    {
                        var address = item as String;
                        var emailEntry = await _userIdentityService.CreateEmailIdentity(address);

                        // todo: Database should not allow the same invitation is created for the same person ...
                        var newInvitation = await this._memberInvitationService
                            .CreateInvitationForEmail(customer.Id, model.GroupId, emailEntry.Id, model.Content);

                        // todo: right after sending invitation, write a record on server to build relation between email
                        // var actionUrl = Url.RouteUrl("RespondToGroupInvitation", new { groupId = groupId });
                        // var emailSent = this._workflowMessageService.SendGroupInvitationMessage(_workContext.CurrentCustomer, address, actionUrl, group.Name, this._workContext.WorkingLanguage.Id, content);

                        // SuccessNotification(string.Format("An invitation has been sent to {0}", email));
                        // return RedirectToAction("MemberList", new { groupId = groupId });

                        newInvitationList.Add(newInvitation);
                    }
                } catch(Exception e)
                {
                    // todo: Return partial result to users
                }
            }

            // no need to send out emails in this controller method

            // create a list of invitation models and use it to refresh
            var models = new List<InvitationModel>();
            foreach(var x in newInvitationList)
            {
                var y = new InvitationModel();
                await this._userGroupMemberModelFactory.PrepareGroupInvitationModel(group, x, y);
                models.Add(y);
            }

            return new JsonResult(new
            {
                data = models
                // todo: errors can be the second field
            });
        }

        [HttpPost("user-group/apply")]
        public async Task<ActionResult> Apply(int groupId, string message)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            // group must exist at this moment
            var group = await _userGroupService.GetGroupById(groupId);
            if (group == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_GroupNotFound,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, groupId)
                })
                {
                    StatusCode = 404
                };
            }

            // user cannot apply if he/she is already a group member
            var alreadyJoined = (await _groupMemberService.GetGroupMembers(groupId)).Any(x => x.CustomerId == customer.Id);
            if (alreadyJoined)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_PeopleAlreadyJoined,
                    errorInfo = String.Format(ResourceStrings.UserGroup_UserHasAlreadyJoined, group.Name)
                })
                {
                    StatusCode = 422
                };
            }

            // user cannot apply if he/she has already applied for this group
            var record = (await _memberInvitationService
                .FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id))
                .Where(x => x.GroupId == group.Id && x.IsDeleted == false).FirstOrDefault();
            if (record != null) // todo: We do not need to check  && alreadyApplied.Declined)
            {
                string errorInfo = "";
                int errorCode = (int)GroupConstraintCode.Error;
                if (record.CreatorId == group.CreatorId)
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_AlreadyInvited;
                    errorInfo = String.Format(ResourceStrings.UserGroup_AlreadyInvited, customer.Username);
                }
                if(record.CreatorId == customer.Id)
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_AlreadyApplied;
                    errorInfo = String.Format(ResourceStrings.UserGroup_AlreadyApplied, group.Name);
                }
                return new JsonResult(new
                {
                    errorCode = errorCode,
                    errorInfo = errorInfo
                })
                {
                    StatusCode = 422
                };
            }

            // user can only join a fixed number of groups based on his plan
            var code = await _userGroupConstraints.CanApplyGroup(customer.Id);
            if (code != GroupConstraintCode.NoError)
            {
                return new JsonResult(new
                {
                    errorCode = code,
                    errorInfo = ResourceStrings.UserGroup_YouJoinedTooManyGroups
                })
                {
                    StatusCode = 422
                };
            }

            // group cannot be full
            code = await _userGroupConstraints.CanInvitePeople(group);
            if (code == GroupConstraintCode.UserGroup_GroupFull)
            {
                return new JsonResult(new
                {
                    errorCode = (int)code,
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupFull, group.Name)
                })
                {
                    StatusCode = 422
                };
            }

            // customer create an invitation by himself
            // later on this record will be handled as a application to group
            var invitation = await _memberInvitationService.CreateApplicationForCustomer(group.Id, customer.Id, message);

            // todo: Remove the following. We do not need to worry about this, as our system is listening to this change
            // BackgroundJob.Enqueue(() => EmailGenerator.UserAppliedForGroup(invitation.Id, _storeContext.CurrentStore.DefaultLanguageId));

            var model = new InvitationModel();
            await this._userGroupMemberModelFactory.PrepareGroupInvitationModel(group, invitation, model);
            return Json(model);
        }

        #endregion

        #region Invitation Processing and State changes


        /// <summary>
        /// internal method to proceed accept/deny an invitation
        /// </summary>
        /// <param name="invitation"></param>
        /// <returns>Five possible return codes:
        /// - group deleted
        /// - invitation not found
        /// - already joined
        /// - successfully joined
        /// - denied invitation
        /// </returns>
        private async Task<GroupConstraintCode> GuardOnAcceptInvitation(IUserGroupInvitation invitation)
        {
            var group = await this._userGroupService.GetGroupById(invitation.GroupId);

            // Group shall be available
            var code = await this._userGroupConstraints.CanInvitePeople(group);
            if (code == GroupConstraintCode.UserGroup_GroupFull)
            {
                return GroupConstraintCode.UserGroup_GroupFull;
            }

            // todo: The following is just fault tolerant. We my remove it.
            // if user has already joined this group, no need for further action
            var joinedGroups = await _userGroupService.FindAssociatedGroups(invitation.UserId);
            var alreadyJoined = joinedGroups.Any(x => x.Id == group.Id);
            if (alreadyJoined)
            {
                return GroupConstraintCode.UserGroup_PeopleAlreadyJoined;
            }

            return GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess;
        }

        private string TranslateErrorCode(GroupConstraintCode code, int groupId)
        {
            if (code == GroupConstraintCode.UserGroup_GroupHasBeenDeleted)
            {
                return ResourceStrings.UserGroup_GroupHasBeenDeleted;
            }

            if (code == GroupConstraintCode.UserGroup_GroupFull)
            {
                return String.Format(ResourceStrings.UserGroup_GroupFull, groupId);
            }

            if (code == GroupConstraintCode.UserGroup_YouJoinedTooManyGroups)
            {
                return ResourceStrings.UserGroup_YouJoinedTooManyGroups;
            }

            if (code == GroupConstraintCode.UserGroup_PeopleAlreadyJoined)
            {
                return String.Format(ResourceStrings.UserGroup_UserHasAlreadyJoined, groupId);
            }

            if (code == GroupConstraintCode.UserGroup_GroupInvitationInvalid)
            {
                return ResourceStrings.UserGroup_GroupInvitationInvalid;
            }

            if (code == GroupConstraintCode.UserGroupMember_DeclinedInvitation)
            {
                return string.Format("You declined group {0}'s invitation.", groupId);
            }

            return "Unknown";
        }

        [HttpPut("user-group/accept-or-deny-invitation/{id:min(1)}")]
        public async Task<ActionResult> AcceptOrDenyInvitation(int id, bool accepted)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var invitation = await this._memberInvitationService.GetInvitationOrApplicationById(id);
            if (invitation == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 404
                };
            }
            var group = await _userGroupService.GetGroupById(invitation.GroupId);

            var customer = _workContext.CurrentCustomer;

            // todo: Verify if the current user has the right permission
            if (invitation.ProviderId != (short)PredefinedUserProvider.NXdriveAccount || invitation.UserId != customer.Id)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_NotOwnedByUser,
                    errorInfo = String.Format(ResourceStrings.UserGroup_NotOwnedByUser, group.Name)
                })
                {
                    StatusCode = 404
                };
            }

            if (accepted)
            {
                var code = await GuardOnAcceptInvitation(invitation);
                if (code != GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess)
                {
                    return new JsonResult(new
                    {
                        errorCode = (int)code,
                        errorInfo = TranslateErrorCode(code, invitation.GroupId)
                    })
                    {
                        StatusCode = 422
                    };
                }

                // Note that the order below matters
                var member = await _groupMemberService.AddMemberIntoGroup(invitation.GroupId, invitation.UserId);

                // Write down the relation between member and invitation
                _generateAttributeService.SaveAttribute<int>(member as UserGroupMember, SystemAttributeExtension.GroupMemberResultingFrom, invitation.Id);

                // delete invitation after adding this user
                await _memberInvitationService.DeleteInvitationOrApplication(invitation);

                var memberModel = new MemberModel();
                _userGroupMemberModelFactory.PrepareGroupMemberModel(group, member, memberModel);

                return new JsonResult(memberModel);
            }

            // Otherwise, delete
            await this._memberInvitationService.DenyInvitationOrDeclineApplication(invitation);

            // todo: Remove the following code, we do not need to handle them directly.
            //BackgroundJob.Enqueue(() => EmailGenerator.SendGroupInvitation(0, _storeContext.CurrentStore.DefaultLanguageId));
            //BackgroundJob.Enqueue(() => EmailGenerator.GroupApplicationProceeded(model.Id, errorModel.Success, _storeContext.CurrentStore.DefaultLanguageId));

            var model = new InvitationModel();
            await this._userGroupMemberModelFactory.PrepareGroupInvitationModel(group, invitation, model);
            return Json(model);
        }

        [HttpPut("user-group/approve-or-decline-application/{id:min(1)}")]
        public async Task<ActionResult> ApproveOrDeclineApplication(int id, bool approved)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var invitation = await this._memberInvitationService.GetInvitationOrApplicationById(id);
            if (invitation == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 404
                };
            }

            var customer = _workContext.CurrentCustomer;

            // todo: Verify if the current user has the right permission
            if (invitation.ProviderId != (short)PredefinedUserProvider.NXdriveAccount)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 403
                };
            }

            var group = await _userGroupService.GetGroupById(invitation.GroupId);
            if (group.AdminId != customer.Id)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_NotOwnedByUser,
                    errorInfo = String.Format(ResourceStrings.UserGroup_NotOwnedByUser, group.Name)
                })
                {
                    StatusCode = 404
                };
            }

            if (approved)
            {
                var code = await GuardOnAcceptInvitation(invitation);
                if (code != GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess)
                {
                    return new JsonResult(new
                    {
                        errorCode = (int)code,
                        errorInfo = TranslateErrorCode(code, invitation.GroupId)
                    })
                    {
                        StatusCode = 422
                    };
                }

                var member = await _groupMemberService.AddMemberIntoGroup(invitation.GroupId, invitation.UserId);

                // Write down the relationship between member and invitation
                _generateAttributeService.SaveAttribute<int>(member as UserGroupMember, SystemAttributeExtension.GroupMemberResultingFrom, invitation.Id);

                // delete invitation after adding this user
                await _memberInvitationService.DeleteInvitationOrApplication(invitation);

                var memberModel = new MemberModel();
                _userGroupMemberModelFactory.PrepareGroupMemberModel(group, member, memberModel);

                return new JsonResult(memberModel);
            }

            // Otherwise, decline
            await this._memberInvitationService.DenyInvitationOrDeclineApplication(invitation);

            var model = new InvitationModel();
            await this._userGroupMemberModelFactory.PrepareGroupInvitationModel(group, invitation, model);
            return Json(model);
        }

        #endregion


        #region Notifications
        [HttpPost("user-group/resend-email-on-impending-member/{id:min(1)}")]
        public async Task<ActionResult> ResendEmailOnImpendingMember(int id)
        {
            // Only registered user has this permission
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var invitation = await this._memberInvitationService.GetInvitationOrApplicationById(id);
            if(invitation == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_InvitationNotFound,
                    errorInfo = ResourceStrings.UserGroup_InvitationNotFound
                })
                {
                    StatusCode = 404
                };
            }

            var tyInvitation = invitation as UserGroupInvitation;
            var lastSentDate = tyInvitation.GetAttribute<DateTime?>(Extensions.SystemAttributeExtension.GroupEmailNotificationLastSentDate);

            if (lastSentDate != null)
            {
                var now = DateTime.UtcNow;
                if (lastSentDate.Value.AddDays(1) < now)
                {
                    // 1 day has not yet passed
                    return new JsonResult(new
                    {
                        errorCode = (int)GroupConstraintCode.UserGroup_EmailResendTooOften,
                        errorInfo = ResourceStrings.UserGroup_ResendEmailTooOften+ $", Last sent date: {lastSentDate.Value}"
                    })
                    {
                        StatusCode = 422
                    };
                }
            }

            var group = await _userGroupService.GetGroupById(invitation.GroupId);
            // todo: No need to check group at this point
            //if (group == null)
            //{
            //    var error = String.Format(ResourceStrings.UserGroup_GroupNotFound, invitation.GroupId);
            //    return Json(new { Success = false, Error = error });
            //}

            var code = await this._userGroupConstraints.CanInvitePeople(group);
            if (code != GroupConstraintCode.NoError)
            {
                return new JsonResult(new
                {
                    errorCode = (int)code,
                    errorInfo = string.Format(ResourceStrings.UserGroup_GroupFull, group.Name)
                })
                {
                    StatusCode = 422
                };
            }

            // Directly send an email
            EventHandlers.Actions.UserGroup.NotyCreatedInvitationOrApplicationAction.Invoke(id, _storeContext.CurrentStore.DefaultLanguageId);

            return Ok();
        }

        [HttpPost("user-group/send-message-to-member")]
        public async Task<ActionResult> SendMessageToMember(MemberPrivateMessageModel model)
        {
            // Only registered user has this permission
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var membership = await _groupMemberService.GetGroupMemberById(model.MemberId);
            if (membership == null)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_MembershipNotFound,
                    errorInfo = ResourceStrings.UserGroup_MembershipNotFound
                })
                {
                    StatusCode = 404
                };
            }

            var customer = _workContext.CurrentCustomer;
            var group = await _userGroupService.GetGroupById(membership.GroupId);

            bool permission = false;
            // todo: Verify who can send out the notification
            if (group.AdminId == customer.Id)
            {
                permission = true;
            } else if (customer.Id == model.MemberId)
            {
                // We do not allow someone to send a message to herself/himself
                permission = false;
            } else
            {
                permission = (await _groupMemberService.GetGroupMembers(group.Id))
                    .Any(x => x.CustomerId == customer.Id);
            }

            if (!permission)
            {
                return new JsonResult(new
                {
                    errorCode = (int)GroupConstraintCode.UserGroup_MemberCannotViewGroupInvitations,
                    errorInfo = ResourceStrings.UserGroup_MemberCannotViewGroupInvitations
                })
                {
                    StatusCode = 403
                };
            }

            var message = new PrivateMessage()
            {
                Subject = model.Subject,
                Text = model.Text,
                FromCustomerId = customer.Id,
                ToCustomerId = membership.CustomerId,
                CreatedOnUtc = DateTime.UtcNow,
                StoreId = _storeContext.CurrentStore.Id
            };

            this._forumService.InsertPrivateMessage(message);

            return Ok();
        }

        #endregion


        #region Accept invitation or Approve application via link with token
        [Route("user-group/accept-invitation/{token}", Name = "UserGroupAcceptInvitationViaLink")]
        [AcceptVerbs("GET")]
        public async Task<ActionResult> AcceptInvitationViaLink(string token)
        {
            // Note that we intentionally does not require that
            // the current customer is a registered user.

            if (string.IsNullOrEmpty(token))    // case: token is null or empty
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid invitation. Please verify if you are using a right invitation."
                });
            }

            var invitation = await _memberInvitationService.FindInvitationOrApplicationByToken(token);
            if (invitation == null || invitation.IsDeleted || invitation.Declined)  // case: cannot find invitation by token
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid/Expired invitation. Please verify if you are using a right invitation."
                });
            }

            // todo: Do we need to verify if the group is deleted
            // is this necessary???
            var group = await _userGroupService.GetGroupById(invitation.GroupId);
            if (group == null && group.IsDeleted)
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid/Expired invitation. Please verify if you are using a right invitation."
                });
            }

            // non registered user
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                if (invitation.ProviderId == (short)PredefinedUserProvider.Email)
                {
                    // only the case for
                    var emailUser = await _userIdentityService.GetEmailIdentityById(invitation.UserId);

                    // todo: Consider if emailUser can be null
                    var registerUrl = Url.RouteUrl("Register");
                    registerUrl = QueryHelpers.AddQueryString(registerUrl, "email", emailUser.Email);

                    return View("Message4LightPage", new AlertBoxModel(new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>("Create my free account", registerUrl)
                    })
                    {
                        AlertType = AlertBoxModel.AlertTypeEnum.Info,
                        Title = "Registration Required",
                        Opening = "Heads Up",
                        Message = "You are required to sign up first to accept the invitation. Do you want to create an account?"
                    });
                }

                // Otherwise, log in is required
                var targetUser = _customerService.GetCustomerById(invitation.UserId);
                var loginUrl = Url.RouteUrl("Login");
                loginUrl = QueryHelpers.AddQueryString(loginUrl, "email", targetUser.Email);

                return View("Message4LightPage", new AlertBoxModel(new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>("Sign in now", loginUrl)
                    })
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Login Required",
                    Opening = "Heads Up",
                    Message = "You are required to sign in first to accept the invitation. Do you want to sign in?"
                });
            }

            // not for me
            if (invitation.ProviderId != (short)PredefinedUserProvider.NXdriveAccount ||
                _workContext.CurrentCustomer.Id != invitation.UserId)
            {
                var switchAccountUrl = Url.RouteUrl("SwitchAccount");
                switchAccountUrl = QueryHelpers.AddQueryString(switchAccountUrl, "returnUrl", this.HttpContext.Request.Path);

                return View("Message4LightPage", new AlertBoxModel(new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>("Switch my account", switchAccountUrl)
                    })
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Wrong Invitation",
                    Opening = "Heads Up",
                    Message = "The invitation is intended for a different user. Do you want to sign in with a different account ? "
                });
            }

            // Run guard, check if already joined
            var code = await GuardOnAcceptInvitation(invitation);
            if (code != GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess)
            {
                var msg = TranslateErrorCode(code, invitation.GroupId);
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Failure to Accept Invitation",
                    Opening = "Heads Up",
                    Message = $"You cannot be accepted into the group now. {msg}"
                });
            }

            var creator = _customerService.GetCustomerById(invitation.CreatorId);

            var model = new GroupModel();
            _userGroupModelFactory.PrepareGroupModel(group, _workContext.CurrentCustomer, model);

            var confirmModel = new ConfirmAcceptInvitationModel
            {
                Id = invitation.Id,
                Group = model,
                CreatorEmail = creator.Email,
                CreatorName = creator.GetFullName(),
                CreatedOn = invitation.CreatedOnUtc
            };

            return View("ConfirmToAcceptInvitation", confirmModel);
        }

        [Route("user-group/accept-invitation-post", Name = "UserGroupAcceptInvitationPost")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> AcceptInvitationRequestPost(int id) {

            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var invitation = await _memberInvitationService.GetInvitationOrApplicationById(id);
            if (invitation == null || invitation.IsDeleted || invitation.Declined)  // case: cannot find invitation by token
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid/Expired invitation. Please verify if you are using a right invitation."
                });
            }

            if (invitation.ProviderId != (short)PredefinedUserProvider.NXdriveAccount ||
             _workContext.CurrentCustomer.Id != invitation.UserId)
            {
                var switchAccountUrl = Url.RouteUrl("SwitchAccount");
                switchAccountUrl = QueryHelpers.AddQueryString(switchAccountUrl, "returnUrl", this.HttpContext.Request.Path);

                return View("Message4LightPage", new AlertBoxModel(new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>("Switch my account", switchAccountUrl)
                    })
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Wrong Invitation",
                    Opening = "Heads Up",
                    Message = "The invitation is intended for a different user. Do you want to sign in with a different account ? "
                });
            }

            var code = await GuardOnAcceptInvitation(invitation);
            if (code != GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess)
            {
                var msg = TranslateErrorCode(code, invitation.GroupId);
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Failure to Accept Invitation",
                    Opening = "Heads Up",
                    Message = $"You cannot be accepted into the group now. {msg}"
                });
            }

            // Accept the invitation
            var member = await _groupMemberService.AddMemberIntoGroup(invitation.GroupId, invitation.UserId);

            // Write down the relationship between member and invitation
            _generateAttributeService.SaveAttribute<int>(member as UserGroupMember, SystemAttributeExtension.GroupMemberResultingFrom, invitation.Id);

            // delete invitation after adding this user
            await _memberInvitationService.DeleteInvitationOrApplication(invitation);

            var group = await _userGroupService.GetGroupById(invitation.GroupId);

            var firstUrl = Url.RouteUrl("UserGroupIndex");

            // todo: Build the second url for accessing files

            return View("Message4LightPage", new AlertBoxModel(new List<Tuple<string, string>>
                    {
                        new Tuple<string, string>("Access my group", firstUrl),
                        new Tuple<string, string>("Share files with this group", firstUrl)
                    })
            {
                AlertType = AlertBoxModel.AlertTypeEnum.Info,
                Title = "Invitation Has Been Accepted",
                Opening = "Congratulation",
                Message = $"You are now a member of the group --- {group.Name}. You may access this group or start to share files with its other members."
            });
        }

        [Route("user-group/approve-application/{token}", Name = "UserGroupApproveApplicationViaLink")]
        [AcceptVerbs("GET")]
        public async Task<ActionResult> ApproveAppliationViaLink(string token)
        {
            if (string.IsNullOrEmpty(token))    // case: token is null or empty
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid invitation. Please verify if you are using a right invitation."
                });
            }

            var application = await _memberInvitationService.FindInvitationOrApplicationByToken(token);
            if (application == null || application.IsDeleted || application.Declined)  // case: cannot find invitation by token
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid/Expired invitation. Please verify if you are using a right invitation."
                });
            }

            // todo: Do we need to verify if the group is deleted
            // is this necessary???
            var group = await _userGroupService.GetGroupById(application.GroupId);
            if (group == null && group.IsDeleted)
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid/Expired invitation. Please verify if you are using a right invitation."
                });
            }

            if (!_workContext.CurrentCustomer.IsRegistered()) {
                return Challenge();
            }
            // The following will never happen
            // application.ProviderId != (short)PredefinedUserProvider.NXdriveAccount)

            // Only admin can do this.
            var customer = _workContext.CurrentCustomer;
            if (customer.Id != group.AdminId)
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Access Denied",
                    Opening = "Oops",
                    Message = "You do not have the permssion to approve the applicaton. Please verify if you are using a right application."
                });
            }

            var code = await GuardOnAcceptInvitation(application);
            if (code != GroupConstraintCode.UserGroupMember_AcceptInvitationSuccess)
            {
                var msg = TranslateErrorCode(code, application.GroupId);
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Info,
                    Title = "Failure to Approve Application",
                    Opening = "Heads Up",
                    Message = $"You cannot approve this application now. {msg}"
                });
            }

            // todo: Compute specfic redirect url for this application

            return RedirectToRoute("UserGroupIndex");
        }
        #endregion
    }
}
