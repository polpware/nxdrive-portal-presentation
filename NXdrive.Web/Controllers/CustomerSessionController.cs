﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using System;

namespace NXdrive.Web.Controllers
{
    partial class CustomerController
    {
        //[HttpsRequirement(SslRequirement.Yes)]
        [Route("customer/ajax-user-session", Name = "AjaxCustomerUserSession")]
        [AcceptVerbs("GET")]
        [EnableCors("AllowLocalHost")]
        public virtual IActionResult AjaxUserSession()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            return Json(_workContext.CurrentCustomer);
        }

        // [HttpsRequirement(SslRequirement.Yes)]
        [Route("customer/ajax-user-info", Name = "AjaxCustomerUserInfo")]
        [AcceptVerbs("GET")]
        [EnableCors("AllowLocalHost")]
        public virtual IActionResult AjaxUserInfo()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            return Json(new
            {
                UserId = customer.Id,
                Email = customer.Email,
                FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                lastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                Username = customer.FormatUserName()
            });
        }

        // Service plan
        #region Service plan

        [Obsolete("implemented in product controller")]
        [HttpsRequirement(SslRequirement.Yes)]
        [Route("customer/service-plan", Name = "CustomerServicePlan")]
        [AcceptVerbs("GET")]
        public virtual IActionResult ServicePlan()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _servicePlanModelFactory.PrepareCustomerServicePlanModel(_workContext.CurrentCustomer);
            return View(model);
        }

        // Note that the above action is only readable.

        #endregion

        [Route("signin-from-token", Name = "LoginFromToken")]
        [AcceptVerbs("GET")]
        [Polpware.OAuth2.Security.Authorization.RegisterUserAuthorization]
        public virtual IActionResult LoginFromToken(string redirect="")
        {
            var email = HttpContext.User.Identity.Name;
            var customer =  _customerService.GetCustomerByEmail(email);
            if (customer == null)
            {
                return RedirectToRoute("Login");
            }
            _authenticationService.SignIn(customer, false);
            if (string.IsNullOrWhiteSpace(redirect))
            {
                return RedirectToRoute("CustomerInfo");
            }
            return RedirectToRoute(redirect);
        }
    }
}
