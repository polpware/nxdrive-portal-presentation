using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Extensions;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.Customer;
using NXdrive.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    public class CustomerContactController : BaseController
    {
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IUserIdentityService _userIdentityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IStoreContext _storeContext;
        private readonly IRepository<GenericAttribute> _genericAttributeRepo;
        private readonly Extensions.SendEmailService _sendEmailService;
        private readonly ICustomerContactModelFactory _customerContactModelFactory;

        public CustomerContactController(ICustomerService customerService,
            ICustomerActivityService customerActivityService,
            IWorkContext workContext,
            IUserIdentityService userIdentityService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IRepository<GenericAttribute> genericAttributeRepo,
            Extensions.SendEmailService sendEmailService,
            ICustomerContactModelFactory customerContactModelFactory)
        {
            _customerService = customerService;
            _customerActivityService = customerActivityService;

            _workContext = workContext;
            _userIdentityService = userIdentityService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _genericAttributeRepo = genericAttributeRepo;
            _sendEmailService = sendEmailService;

            _customerContactModelFactory = customerContactModelFactory;
        }

        [Route("customer/contacts", Name = "CustomerContacts")]
        [AcceptVerbs("GET")]
        public async Task<IActionResult> Index()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            // it should return both the relationship and the email identity
            // Use SearchConnection() without parameters will list this user's full contact list
            var contacts = await _userIdentityService.SearchConnections(_workContext.CurrentCustomer.Id);
            var models = contacts.Select(x =>
            {
                var y = new CustomerContactModel();
                _customerContactModelFactory.BuildContactModel(x.Item1, x.Item2, y);
                return y;
            });

            var query = _genericAttributeRepo.Table;
            var entry = query.Where(x => x.KeyGroup == Extensions.SystemAttributeExtension.CustomerContactCandidate && x.EntityId == _workContext.CurrentCustomer.Id);
            var pendingContacts = entry.Select(p => p.Value);

            return View(new Tuple<IEnumerable<CustomerContactModel>, IEnumerable<string>>(models, pendingContacts));
        }

        [Route("customer/contacts/search", Name = "CustomerContactLiveSearch")]
        [AcceptVerbs("POST")]
        //[PublicAntiForgery]
        [EnableCors("AllowLocalHost")]
        public async Task<IActionResult> LiveSearch(ContactLiveSearchModel model)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
            {
                return Challenge();
            }

            model.name = model.name ?? string.Empty;

            var provider = model.onlyUser ? PredefinedUserProvider.NXdriveAccount : PredefinedUserProvider.Min;

            // Only show the first 15
            var matched = await _userIdentityService.SearchConnections(_workContext.CurrentCustomer.Id, model.name, provider, model.pageNumber, model.pageSize);

            var data = matched.Select(x => new
            {
                value = x.Item2.Id,
                customerId = x.Item2.DerivedUserId,
                text = x.Item2.Email,
                userName = x.Item2.UserName(_customerService),
                disabled = false
            });

            return Json(data);
        }

        [Route("customer/contacts/invite-friends", Name = "CustomerContactInviteFriends")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public async Task<IActionResult> InviteFriends(ContactRequestModel emailRequest)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var models = new List<PendingContactModel>();

            var activityType = _customerActivityService.GetAllActivityTypes().Where(x => x.SystemKeyword == ActivityLogTypeExtension.CustomerEmailFriendRequest).First();

            foreach (var email in emailRequest.Emails)
            {
                try
                {
                    if (_workContext.CurrentCustomer.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    // Trivially true, current user has already built a connection with this email
                    // Trivially true, current user tries to invite himself
                    var alreadyContact = await _userIdentityService.SearchConnections(_workContext.CurrentCustomer.Id, email);
                    if (alreadyContact.Any())
                        continue;

                    var query = _genericAttributeRepo.Table;
                    query = query.Where(x => x.KeyGroup == Extensions.SystemAttributeExtension.CustomerContactCandidate &&
                    x.EntityId == _workContext.CurrentCustomer.Id && x.Value == email);

                    string oldToken = string.Empty;
                    if (query.Any())
                    {
                        // Current user has already invited this email
                        // Next, check the timestamp to prevent from sending too often
                        var firsetEntry = query.First();
                        var activity = _customerActivityService.GetAllActivities(customerId: _workContext.CurrentCustomer.Id, activityLogTypeId: activityType.Id, entityId: firsetEntry.Id).FirstOrDefault();
                        if (activity != null)
                        {
                            if (!activity.CreatedOnUtc.IsExpired(ActivityExpirationSetting.OneDayPeriod))
                            {
                                // anti-spam: Not yet expired, no need to send any more ...
                                continue;
                            }
                            else
                            {
                                oldToken = firsetEntry.Key;
                                // Delete old one
                                _genericAttributeRepo.Delete(firsetEntry);
                            }
                        }
                    }

                    // Attempt to reuse old token.
                    Guid token;
                    if (string.IsNullOrEmpty(oldToken))
                    {
                        token = System.Guid.NewGuid();
                    }
                    else
                    {
                        Guid.TryParse(oldToken, out token);
                    }

                    // Pass token into email
                    _sendEmailService.SendFriendInvitation(_workContext.CurrentCustomer,
                         email, token.ToString(), emailRequest.PersonalMessage, MessageTemplateIDs.CustomerNetworkFriendInvitation, _workContext.WorkingLanguage.Id);

                    // Assoicate email with ID
                    var attribute = new GenericAttribute
                    {
                        EntityId = _workContext.CurrentCustomer.Id, // Email instance id
                        Key = token.ToString(),
                        KeyGroup = Extensions.SystemAttributeExtension.CustomerContactCandidate,
                        StoreId = _storeContext.CurrentStore.Id,
                        Value = email
                    };

                    _genericAttributeService.InsertAttribute(attribute);

                    // Also record this activity, so we can retrieve its timestamp.
                    _customerActivityService.InsertActivity(ActivityLogTypeExtension.CustomerEmailFriendRequest, string.Empty, attribute);

                    models.Add(new PendingContactModel() { Email = email });
                }
                catch (Exception) { }   // for the time-being, we don't care invalid emails
            }

            return new JsonResult(new
            {
                Data = models
            });
        }

        [Route("customer/contacts/confirm/{token:guid}", Name = "CustomerContactConfirmInvitation")]
        [AcceptVerbs("GET")]
        public async Task<IActionResult> ConfirmInvitation(string token)
        {
            var query = _genericAttributeRepo.Table;
            var entry = query.Where(x => x.KeyGroup == Extensions.SystemAttributeExtension.CustomerContactCandidate && x.Key == token)
                .FirstOrDefault();

            if (entry == null)
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid/Expired Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid invitation. Please verify if you are using a right invitation."
                });
            }

            //todo: More logic to verify if the token is expired or not

            var fromCustomerId = entry.EntityId;
            var fromCustomer = _customerService.GetCustomerById(fromCustomerId);

            if (fromCustomer == null)
            {
                return View("Message4LightPage", new AlertBoxModel()
                {
                    AlertType = AlertBoxModel.AlertTypeEnum.Error,
                    Title = "Invalid Invitation",
                    Opening = "Oops",
                    Message = "You are accessing an invalid invitation. Please verify if you are using a right invitation."
                });
            }

            var toEmail = entry.Value;
            var fromEmail = fromCustomer.Email;

            // sender must be nxdrive customer, receiver can be either customer for emailIdentity
            var fromEmailIdentity = await _userIdentityService.FindEmailIdentity(fromEmail);
            var toEmailIdentity = await _userIdentityService.FindEmailIdentity(toEmail);
            var toEmailCustomer = _customerService.GetCustomerByEmail(toEmail);

            if (toEmailIdentity == null)
            {
                toEmailIdentity = await _userIdentityService.CreateEmailIdentity(toEmail);
            }

            // Ensure that toEmailIdentity points to toEmailCustomer
            if (toEmailCustomer != null && toEmailIdentity.DerivedProviderId != (short)PredefinedUserProvider.NXdriveAccount)
            {
                var to = toEmailIdentity as EmailIdentity;
                to.DerivedProviderId = (short)PredefinedUserProvider.NXdriveAccount;
                to.DerivedUserId = toEmailCustomer.Id;
                await _userIdentityService.UpdateEmailIdentity(to);
            }

            // build a connection from fromCustomer to invited user direction.
            await _userIdentityService.BuildConnection(fromCustomer.Id, toEmailIdentity.Id);

            // if toCustomer is nxdrive customer, build connection for fromCustomer, else do nothing
            if (toEmailCustomer != null)
            {
                if (fromEmailIdentity == null)
                {
                    fromEmailIdentity = await _userIdentityService.CreateEmailIdentity(fromEmail);
                }

                // Similarly as above, ensure that fromEmailIdentity points to an NXdrive account.
                if (fromEmailIdentity.DerivedProviderId != (short)PredefinedUserProvider.NXdriveAccount)
                {
                    var from = fromEmailIdentity as EmailIdentity;
                    from.DerivedProviderId = (short)PredefinedUserProvider.NXdriveAccount;
                    from.DerivedUserId = fromCustomer.Id;

                    await _userIdentityService.UpdateEmailIdentity(from);
                }

                await _userIdentityService.BuildConnection(toEmailCustomer.Id, fromEmailIdentity.Id);
            }

            // Delete the attributes
            _genericAttributeRepo.Delete(entry);

            // There could be some invitations from this user to this email.
            // However, we rely on our scheduler to clean them up.

            return View("Message4LightPage", new AlertBoxModel()
            {
                AlertType = AlertBoxModel.AlertTypeEnum.Success,
                Title = "Invitation Accepted",
                Opening = "Congrats",
                Message = "You have successfully accepted the friend invitation."
            });
        }

        [Route("customer/contacts/update", Name = "CustomerContactUpdate")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public async Task<IActionResult> UpdateContact(CustomerContactModel newModel)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            // pull out the email identity and connection instance from model
            var toCustomerEmailIdentity = _userIdentityService.GetEmailIdentityById(newModel.IdentityId);
            if (toCustomerEmailIdentity == null)
            {
                return new JsonResult(new
                {
                    Error = "Contact not found"
                })
                {
                    StatusCode = 404
                };
            }

            var connection = (SocialNetwork)await _userIdentityService.FindConnection(_workContext.CurrentCustomer.Id, toCustomerEmailIdentity.Id);
            if (connection == null)
            {
                return new JsonResult(new
                {
                    Error = "Contact not found"
                })
                {
                    StatusCode = 404
                };
            }

            // save attributes of connection
            // so far only allow changing name and comments
            _genericAttributeService.SaveAttribute(connection, Extensions.SystemAttributeExtension.CustomerContactComment, newModel.Comment);
            _genericAttributeService.SaveAttribute(connection, Extensions.SystemAttributeExtension.CustomerContactName, newModel.FullName);

            return new JsonResult(new {});
        }

        [Route("customer/contacts/delete", Name = "CustomerContactDelete")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public async Task<IActionResult> DeleteContact(int connectionId)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var connection = await _userIdentityService.GetConnectionById(connectionId);
            if (connection == null)
            {
                return new JsonResult(new
                {
                    Error = "Connection not found",
                })
                {
                    StatusCode = 404
                };
            }

            if (connection.CustomerId == _workContext.CurrentCustomer.Id)
            {
                await _userIdentityService.DestoryConnection(connection);
            }
            else
            {
                // Error handling
                return new JsonResult(new
                {
                    Error = "Not allowed"
                })
                {
                    StatusCode = 404
                };
            }

            return new JsonResult(new { });
        }

        [Route("customer/contacts/refer", Name = "CustomerReferFriend")]
        [AcceptVerbs("POST")]
        [PublicAntiForgery]
        public async Task<IActionResult> ReferFriends(ContactRequestModel emailRequest)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var sentList = new List<string>();

            var activityType = _customerActivityService.GetAllActivityTypes()
                .Where(x => x.SystemKeyword == ActivityLogTypeExtension.CustomerReferFriendService).First();

            foreach (var email in emailRequest.Emails)
            {
                var emailEntity = await _userIdentityService.FindEmailIdentity(email);

                if (emailEntity == null)
                    continue;

                var activity = _customerActivityService.GetAllActivities(customerId: _workContext.CurrentCustomer.Id,
                    activityLogTypeId: activityType.Id, entityId: emailEntity.Id).FirstOrDefault();

                if (!activity.CreatedOnUtc.IsExpired(ActivityExpirationSetting.OneDayPeriod))
                {
                    // anti-spam: last activity has not passed one day, therefore cannot refer friend
                    continue;
                }

                // no need refer if target email is already a nxdrive customer,
                // or, is user himself
                if (_workContext.CurrentCustomer.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase))
                {
                    continue;
                }

                var alreadyMember = _customerService.GetCustomerByEmail(email);
                if (alreadyMember != null)
                {
                    continue;
                }

                string personalMessage = emailRequest.PersonalMessage;

                // todo: What is the role of the following token?
                var token = System.Guid.NewGuid();  // might not need token to refer
                _sendEmailService.SendFriendInvitation(_workContext.CurrentCustomer,
                             email, token.ToString(), personalMessage, MessageTemplateIDs.CustomerReferNXdrive2Friend,
                             _workContext.WorkingLanguage.Id);

                // Also record this activity, so we can retrieve its timestamp.
                _customerActivityService.InsertActivity(ActivityLogTypeExtension.CustomerReferFriendService,
                    string.Empty, emailEntity as EmailIdentity);

                sentList.Add(email);
            }

            return new JsonResult(new
            {
                Data = sentList
            });
        }
    }
}