﻿using Microsoft.AspNetCore.Mvc;
using Polpware.NopWeb.Factories;

namespace NXdrive.Web.Controllers
{
    public class CountryController : Polpware.NopWeb.Controllers.CountryController
    {
        public CountryController(ICountryModelFactory countryModelFactory) : base(countryModelFactory)
        {
        }

        [Route("country/getstatesbycountryid/", Name = "GetStatesByCountryId")]
        [AcceptVerbs("GET")]
        public override IActionResult GetStatesByCountryId(string countryId, bool addSelectStateItem)
        {
            return base.GetStatesByCountryId(countryId, addSelectStateItem);
        }
    }
}
