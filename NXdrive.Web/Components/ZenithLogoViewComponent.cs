﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;

namespace NXdrive.Web.Components
{
    public class ZenithLogoViewComponent : NopViewComponent
    {
        private readonly ICommonModelFactory _commonModelFactory;

        public ZenithLogoViewComponent(ICommonModelFactory commonModelFactory)
        {
            this._commonModelFactory = commonModelFactory;
        }

        public IViewComponentResult Invoke(string version = "Default")
        {
            var model = _commonModelFactory.PrepareLogoModel();
            return View(version, model);
        }
    }
}
