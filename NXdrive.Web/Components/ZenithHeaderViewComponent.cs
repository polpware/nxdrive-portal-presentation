﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using Nop.Services.Media;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;

namespace NXdrive.Web.Components
{
    public class ZenithHeaderViewComponent : NopViewComponent
    {
        private readonly ICommonModelFactory _commonModelFactory;
        private readonly ICustomerModelFactory _customerModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;

        public ZenithHeaderViewComponent(ICommonModelFactory commonModelFactory,
            ICustomerModelFactory customerModelFactory,
            IWorkContext workContext,
            IPictureService pictureService)
        {
            this._commonModelFactory = commonModelFactory;
            _customerModelFactory = customerModelFactory;
            _workContext = workContext;
            _pictureService = pictureService;
        }

        public IViewComponentResult Invoke(string version = "Default")
        {
            var model = _commonModelFactory.PrepareHeaderLinksModel();
            // Avatar model
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                var avatarModel = new Polpware.NopWeb.Models.Customer.CustomerAvatarModel();

                avatarModel.AvatarUrl = Url.RouteUrl("CustomerLoadAvatar", new { id = _workContext.CurrentCustomer.CustomerGuid.ToString() });
                var imageId = _workContext.CurrentCustomer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId);

                // Read from the image title attributes, as we have used it for storing ticks
                if (imageId > 0)
                {
                    var image = _pictureService.GetPictureById(imageId);
                    avatarModel.AvatarUrl = avatarModel.AvatarUrl + "?v=" + (image == null ? "" : (image.TitleAttribute ?? ""));
                }

                model.CustomProperties.Add("AvatarUrl", avatarModel.AvatarUrl);
            }

            return View(version, model);
        }
    }
}
