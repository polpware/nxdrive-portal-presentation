﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;

namespace NXdrive.Web.Components
{
    public class ZenithFooterViewComponent : NopViewComponent
    {
        private readonly ICommonModelFactory _commonModelFactory;

        public ZenithFooterViewComponent(ICommonModelFactory commonModelFactory)
        {
            this._commonModelFactory = commonModelFactory;
        }

        public IViewComponentResult Invoke(string version = "Default")
        {
            var model = _commonModelFactory.PrepareHeaderLinksModel();
            return View(version, model);
        }
    }
}
