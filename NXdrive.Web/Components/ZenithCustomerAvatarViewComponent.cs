﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Media;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;

namespace NXdrive.Web.Components
{
    public class ZenithCustomerAvatarViewComponent : NopViewComponent
    {
        private readonly ICustomerModelFactory _customerModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;

        public ZenithCustomerAvatarViewComponent(ICustomerModelFactory customerModelFactory,
            IWorkContext workContext, IPictureService pictureService)
        {
            this._customerModelFactory = customerModelFactory;
            this._workContext = workContext;
            this._pictureService = pictureService;
        }

        public IViewComponentResult Invoke(string version = "Default")
        {
            var model = new Models.Customer.CustomerAvatarModel();

            model.AvatarUrl = Url.RouteUrl("CustomerLoadAvatar", new { id = _workContext.CurrentCustomer.CustomerGuid.ToString() });

            var imageId = _workContext.CurrentCustomer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId);

            // Read from the image title attributes, as we have used it for storing ticks
            if (imageId > 0)
            {
                var image = _pictureService.GetPictureById(imageId);
                model.AvatarUrl = model.AvatarUrl + "?v=" + (image == null ? "" : (image.TitleAttribute ?? ""));
            }

            model.CustomerEmail = _workContext.CurrentCustomer.Email;
            model.CustomerName = _workContext.CurrentCustomer.FormatUserName(true, 30);

            return View(version, model);
        }
    }
}
