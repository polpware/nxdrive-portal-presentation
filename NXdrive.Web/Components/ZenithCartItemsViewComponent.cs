﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.Customer;
using Polpware.NopExt.SubsPlan.Orders;
using Polpware.NopWeb.Factories;
using Polpware.NopWeb.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Components
{
    using PolpwareNopExt = Polpware.NopExt;
    public class ZenithCartItemsViewComponent : NopViewComponent
    {
        private readonly IProductService _productService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly PolpwareNopExt.SubsPlan.Orders.IOrderProcessingService _orderProcessingService;
        private readonly PolpwareNopExt.SubsPlan.Orders.SubstOrientedOrderAccessService _substOrientedOrderAccessService;
        private readonly IServicePlanModelFactory _servicePlanModelFactory;

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

        public ZenithCartItemsViewComponent(IProductService productService,
            IProductModelFactory productModelFactory,
            IShoppingCartService shoppingCartService,
            PolpwareNopExt.SubsPlan.Orders.IOrderProcessingService orderProcessingService,
            PolpwareNopExt.SubsPlan.Orders.SubstOrientedOrderAccessService substOrientedOrderAccessService,
            IWorkContext workContext,
            IStoreContext storeContext,
            IServicePlanModelFactory servicePlanModelFactory)
        {
            this._productService = productService;
            this._productModelFactory = productModelFactory;
            this._shoppingCartService = shoppingCartService;
            this._servicePlanModelFactory =  servicePlanModelFactory;
            this._orderProcessingService = orderProcessingService;
            this._substOrientedOrderAccessService = substOrientedOrderAccessService;

            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            Customer currentCustomer = null;
            if (_workContext.CurrentCustomer.IsRegistered())
                currentCustomer = _workContext.CurrentCustomer;
            Product currentProduct = null;
            if (currentCustomer != null)
            {
                var order = _substOrientedOrderAccessService.EffectiveOrder(currentCustomer);
                if (order != null && order.OrderItems != null && order.OrderItems.Count() > 0)
                {
                    var currentOrder = order.OrderItems.FirstOrDefault();
                    currentProduct = _productService.GetProductById(currentOrder.ProductId);
                }
            }

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == Nop.Core.Domain.Orders.ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var models = new List<ServicePlanDetailModel>();
            var isPaymentRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
            // var models = _productModelFactory.PrepareProductOverviewModels(cart.Select(x => x.Product).ToList());
            var cartItems = cart.Select(x => x.Product).ToList();

            foreach (var product in cartItems)
            {
                var model = _servicePlanModelFactory.PrepareServicePlanDetailModel(product, currentProduct);
                models.Add(model);
            }
            return View(new Tuple<IEnumerable<ServicePlanDetailModel>, bool>(models, isPaymentRequired));
        }
    }
}
