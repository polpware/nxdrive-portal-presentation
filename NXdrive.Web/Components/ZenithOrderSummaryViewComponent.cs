﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using Polpware.NopExt.Presentation.SubsPlan.Models;
using Polpware.NopExt.Presentation.SubsPlan.Services;
using Polpware.NopWeb.Factories;
using System.Linq;

namespace NXdrive.Web.Components
{
    using CustomizedOrderProcessingService = Polpware.NopExt.SubsPlan.Orders.OrderProcessingService;
    using ICustomizedOrderProcessingService = Polpware.NopExt.SubsPlan.Orders.IOrderProcessingService;

    public class ZenithOrderSummaryViewComponent : NopViewComponent
    {
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        private readonly IPriceCalculationService _priceCalculationService;

        private readonly ICustomizedOrderProcessingService _orderProcessingService;
        private readonly PrepareOrderModelService _prepareOrderModelService;

        private readonly Polpware.NopExt.SubsPlan.Orders.SubstOrientedOrderAccessService _substOrientedOrderAccessService;
        private readonly Polpware.NopExt.SubsPlan.Orders.OrderFactoryService _orderFactoryService;

        public ZenithOrderSummaryViewComponent(IShoppingCartModelFactory shoppingCartModelFactory,
            IStoreContext storeContext,
            IWorkContext workContext,
            IPriceCalculationService priceCalculationService,
            ICustomizedOrderProcessingService orderProcessingService,
            Polpware.NopExt.SubsPlan.Orders.SubstOrientedOrderAccessService substOrientedOrderAccessService,
            Polpware.NopExt.SubsPlan.Orders.OrderFactoryService orderFactoryService,
            PrepareOrderModelService prepareOrderModelService)
        {
            this._shoppingCartModelFactory = shoppingCartModelFactory;
            this._storeContext = storeContext;
            this._workContext = workContext;

            this._priceCalculationService = priceCalculationService;

            this._orderProcessingService = orderProcessingService;
            this._prepareOrderModelService = prepareOrderModelService;
            this._substOrientedOrderAccessService = substOrientedOrderAccessService;
            this._orderFactoryService = orderFactoryService;
        }

        public IViewComponentResult Invoke()
        {
            //if not passed, then create a new model
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == Nop.Core.Domain.Orders.ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            // todo: Handle the case when the cart is empty.
            // We should have one.
            var shoppingItem = cart.First();

            var model = new ShoppingCartModel();
            _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart,
                isEditable: false,
                prepareAndDisplayOrderReviewData: _orderProcessingService.IsPaymentWorkflowRequired(cart));

            decimal newOrderSubTotal, newOrderTotal, newOrderDiscount;
            // Calculate order total model
            model.OrderTotals = _prepareOrderModelService.PrepareOrderTotalsModel(cart, false,
                 out newOrderSubTotal, out newOrderTotal, out newOrderDiscount);

            var customer = _workContext.CurrentCustomer;
            var prevOrders = _substOrientedOrderAccessService.ActiveOrders(customer);
            if (prevOrders.Any())
            {
                var effectiveOrder = _substOrientedOrderAccessService.EffectiveOrder(customer);
                var effectiveOrderWrapper = _orderFactoryService.CreateBaseOrder(effectiveOrder);
                // Build up model
                model.ToBeCalledOrder = new ShoppingCartModel.CancelledOrderInfo
                {
                    Balance = effectiveOrderWrapper.Balance(),
                    PreviousOrder = _prepareOrderModelService.PrepareOrderDetailsModel(effectiveOrder)
                };

                // If we have some credit, let's show the new payment date
                if (effectiveOrderWrapper.Balance() > decimal.Zero)
                {
                    var credit = effectiveOrderWrapper.Balance();
                    var paymentStartDate = System.DateTime.UtcNow;

                    // The new plan is not free. The user is
                    // rquired to make some payment sooner or later.
                    if (_orderProcessingService.IsPaymentWorkflowRequired(cart))
                    {
                        // Yearly
                        if (shoppingItem.Quantity == 12)
                        {
                            while (credit > newOrderTotal)
                            {
                                paymentStartDate = paymentStartDate.AddYears(1);
                                credit -= newOrderTotal;
                            }
                        }

                        // Days
                        var leftDays = _substOrientedOrderAccessService.CalculateDaysByCredit(cart, credit);
                        paymentStartDate = paymentStartDate.AddDays(leftDays);

                        // Put it into model
                        model.PaymentStartDate = paymentStartDate;
                        model.ServiceStartDate = System.DateTime.UtcNow;
                    }
                    else // The new plan is free
                    {
                        if (effectiveOrderWrapper.CurrentBillingPeriodEnd.HasValue)
                        {
                            model.ServiceStartDate = effectiveOrderWrapper.CurrentBillingPeriodEnd.Value;
                        }
                        else
                        {
                            model.ServiceStartDate = System.DateTime.UtcNow;
                        }
                    }
                }
                else // no credit to applied
                {
                    model.ServiceStartDate = System.DateTime.UtcNow;
                    model.PaymentStartDate = System.DateTime.UtcNow;
                }
            }
            else
            {
                model.ToBeCalledOrder = null;
            }

            // Previous order
            // Start date
            // End date

            // New order

            // Show Total


            return View(model);
        }
    }
}
