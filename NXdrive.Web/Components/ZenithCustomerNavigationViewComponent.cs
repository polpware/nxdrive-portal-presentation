﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;
using Polpware.NopWeb.Models.Customer;

namespace NXdrive.Web.Components
{
    public class ZenithCustomerNavigationViewComponent : NopViewComponent
    {
        private readonly ICustomerModelFactory _customerModelFactory;

        public ZenithCustomerNavigationViewComponent(ICustomerModelFactory customerModelFactory)
        {
            this._customerModelFactory = customerModelFactory;
        }

        public IViewComponentResult Invoke(string version = "Default", bool skipModel = false, int selected = 0)
        {
            if (skipModel)
            {
                return View(version);
            }

            var model = new CustomerNavigationModel();

            // Dashboard
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "CustomerDashboard",
                Title = "Dashboard",
                Tab = (CustomerNavigationEnum)300,  // add more enums
                ItemClass = "icon-real-estate-003 u-line-icon-pro" // icon class
            });

            // Personal info
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "CustomerInfo",
                Title = "Profile", // todo: Internationlization
                Tab = CustomerNavigationEnum.Info,
                ItemClass = "icon-user" // icon class
            });

            // Change Password
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "CustomerChangePassword",
                Title = "Change Password",
                Tab = CustomerNavigationEnum.ChangePassword,
                ItemClass = "et-icon-key"
            });

            // Service plan and usage
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "CustomerPlanUsage",
                Title = "Plan and Usage",
                Tab = (CustomerNavigationEnum)301,  // add more enums
                ItemClass = "icon-pie-chart" // icon class
            });

            // Notifications
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "Notifications",
                Title = "Notifications",
                Tab = (CustomerNavigationEnum)302,  // add more enums
                ItemClass = "fa fa-bell-o" // icon class
            });

            // Contact list
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "ContactList",
                Title = "Contacts",
                Tab = (CustomerNavigationEnum)303,  // add more enums
                ItemClass = "fa fa-address-card-o" // icon class
            });

            // Transactions
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "Billing",
                Title = "Billing",
                Tab = (CustomerNavigationEnum)304,  // add more enums
                ItemClass = "fa fa-credit-card" // icon class
            });

            // Customer address list
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "CustomerAddress",
                Title = "Address",
                Tab = (CustomerNavigationEnum)306,
                ItemClass = "fa fa-street-view"
            });

            // Privacy
            model.CustomerNavigationItems.Add(new CustomerNavigationItemModel()
            {
                RouteName = "Privacy",
                Title = "Privacy",
                Tab = (CustomerNavigationEnum)305,
                ItemClass = "et-icon-shield"
            });

            model.SelectedTab = (CustomerNavigationEnum)selected;

            return View(version, model);
        }
    }
}
