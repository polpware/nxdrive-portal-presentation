﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Media;
using Nop.Web.Framework.Components;
using Polpware.NopWeb.Factories;
using System.Collections.Generic;

namespace NXdrive.Web.Components
{
    public class ZenithPrivateMessagesViewComponent : NopViewComponent
    {
        private readonly IPrivateMessagesModelFactory _privateMessagesModelFactory;
        private readonly ICustomerService _customerService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly Nop.Core.IWorkContext _workContext;

        public ZenithPrivateMessagesViewComponent(IPrivateMessagesModelFactory privateMessagesModelFactory,
            ICustomerService customerService,
            IPictureService pictureService,
            MediaSettings mediaSettings,
            IWorkContext workContext)
        {
            _privateMessagesModelFactory = privateMessagesModelFactory;
            _customerService = customerService;
            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke(int pageNumber, string tab, string version)
        {

            var isInbox = tab == "inbox";

            var model = isInbox ? _privateMessagesModelFactory.PrepareInboxModel(pageNumber, tab) :
                _privateMessagesModelFactory.PrepareSentModel(pageNumber, tab);

            var customerAvatars = new Dictionary<int, string>();
            foreach (var m in model.Messages)
            {
                var targetCustomerId = isInbox ? m.FromCustomerId : m.ToCustomerId;

                if (customerAvatars.ContainsKey(targetCustomerId))
                {
                    m.CustomProperties.Add("AvatarUrl", customerAvatars[targetCustomerId]);
                }
                else
                {
                    var targetCustomer = _customerService.GetCustomerById(targetCustomerId);
                    var avatarUrl = _pictureService.GetPictureUrl(
                        targetCustomer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId),
                        _mediaSettings.AvatarPictureSize,
                        false);
                    m.CustomProperties.Add("AvatarUrl", avatarUrl);
                    customerAvatars[targetCustomerId] = avatarUrl;
                }
            }

            return View(version, model);
        }
    }
}
