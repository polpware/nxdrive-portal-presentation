﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Customers;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Extensions;
using NXdrive.Web.Factories;
using NXdrive.Web.Models.UserGroup;
using Polpware.OAuth2.Security.Authorization;

namespace NXdrive.Web.WebAPI
{
    [Route("api/UserGroupService")]
    [ApiController]
    [EnableCors("AllowLocalHost")]
    public class UserGroupServiceController : ControllerBase
    {
        private readonly IUserGroupService _userGroupService;
        private readonly ICustomerService _customerService;
        private readonly IGroupMemberService _groupMemberService;
        private readonly IMemberInvitationService _memberInvitationService;

        private readonly IUserGroupModelFactory _userGroupModelFactory;

        public UserGroupServiceController(
            IUserGroupService userGroupService,
            ICustomerService customerService,
            IGroupMemberService groupMemberService,
            IMemberInvitationService memberInvitationService,
            IUserGroupModelFactory userGroupModelFactory,
            IUserGroupMemberModelFactory userGroupMemberFactory)
        {
            this._userGroupService = userGroupService;
            this._customerService = customerService;
            this._memberInvitationService = memberInvitationService;
            this._groupMemberService = groupMemberService;

            this._userGroupModelFactory = userGroupModelFactory;
        }

        // GET: api/UserGroupAPI
        [HttpGet]
        [RegisterUserAuthorization]
        public async Task<IActionResult> Get([FromQuery] bool owned = false,
            [FromQuery] int pageIndex = 0, [FromQuery] int pageSize = int.MaxValue)
        {
            var email = HttpContext.User.Identity.Name;
            var customer = _customerService.GetCustomerByEmail(email);

            var groups = await _userGroupService.FindAssociatedGroups(customer.Id,
                ignoreDeleted: true,
                pageIndex: pageIndex, pageSize: pageSize);
            var invitations = await _memberInvitationService
                .FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);
            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            var models = new List<GroupModel>();
            foreach (var group in groups)
            {
                var gm = new GroupModel();
                this._userGroupModelFactory.PrepareGroupModel(group, customer, gm, invitations, joinedGroups);
                models.Add(gm);
            }

            return Ok(new
            {
                total = groups.TotalCount,
                offset = groups.PageIndex * groups.PageSize,
                data = models
            });
        }

        // GET: api/UserGroupAPI/5
        [HttpGet("{id}", Name = "Get")]
        [RegisterUserAuthorization]
        public async Task<IActionResult> Get(int id)
        {
            var email = HttpContext.User.Identity.Name;
            var customer = _customerService.GetCustomerByEmail(email);
            var group = await _userGroupService.GetGroupById(id);
            var invitations = await _memberInvitationService
                .FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);
            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            if (group == null)
            {
                return StatusCode(404, new
                {
                    errorInfo = String.Format(ResourceStrings.UserGroup_GroupNotFound, id.ToString()),
                    errorCode = ResourceStrings.MessageToErrorCode(ResourceStrings.UserGroup_GroupNotFound)
                });
            }

            var model = new GroupModel();
            this._userGroupModelFactory.PrepareGroupModel(group, customer, model, invitations, joinedGroups);

            return Ok(model);
        }

        public class Input
        {
            public int length { get; set; }
            public int offset { get; set; }
            public string keyword { get; set; }

        }

        // POST: api/UserGroupAPI
        [HttpPost]
        [RegisterUserAuthorization]
        public async Task<IActionResult> Post([FromForm] Input inputs)
        {
            // todo: Validate inputs
            var pageSize = inputs.length;

            var pageIndex = inputs.offset / pageSize;  

            var email = HttpContext.User.Identity.Name;
            var customer = _customerService.GetCustomerByEmail(email);

            var groups = await _userGroupService.FindAssociatedGroups(customer.Id,
                ignoreDeleted: true,
                pageIndex: pageIndex, pageSize: pageSize);
            var invitations = await _memberInvitationService
                .FindInvitationAndApplicationsAssociatedWithCustomer(customer.Id);
            var joinedGroups = await _userGroupService.FindAssociatedGroups(customer.Id);

            var models = new List<GroupModel>();
            foreach (var group in groups)
            {
                var gm = new GroupModel();
                this._userGroupModelFactory.PrepareGroupModel(group, customer, gm, invitations, joinedGroups);
                models.Add(gm);
            }

            return Ok(new
            {
                total = groups.TotalCount,
                offset = groups.PageIndex * groups.PageSize,
                data = models
            });
        }

        // PUT: api/UserGroupAPI/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
