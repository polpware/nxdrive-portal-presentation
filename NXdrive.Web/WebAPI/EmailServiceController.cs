﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Customers;
using Nop.Services.Messages;
using NXdrive.Web.Models.API.Emails;
using NXdrive.Web.Models.API.Response;
using Polpware.OAuth2.Security.Authorization;

namespace NXdrive.Web.WebAPI
{
    [Route("api/EmailService")]
    [EnableCors("AllowLocalHost")]
    public class EmailServiceController : ControllerBase
    {

        private readonly Extensions.SendEmailService _sendEmailService;
        private readonly ICustomerService _customerService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IWorkContext _workContext;

        public EmailServiceController(IWorkContext workContext, Extensions.SendEmailService sendEmailService,
            ICustomerService customerService, IMessageTemplateService messageTemplateService)
        {
            _workContext = workContext;
            _sendEmailService = sendEmailService;
            _messageTemplateService = messageTemplateService;
            _customerService = customerService;
        }

        [HttpPost]
        [RegisterUserAuthorization]
        public IActionResult Post(EmailModel model)
        {
            var response = new DataPayload<AccountOpError>
            {
                Version = 1
            };
            var email = HttpContext.User.Identity.Name;
            var senderCustomer = _customerService.GetCustomerByEmail(email);

            foreach (var receiverEmail in model.ReceiverAsList)
            {
                bool receiverRegistered = true;
                var receiverInstance = _customerService.GetCustomerByEmail(receiverEmail);
                if (receiverInstance == null)
                    receiverRegistered = false;

                switch (model.EmailType)
                {
                    // todo: 
                }
            }

            // todo: Send email service
            // todo: convert from EmailTypeId to emailTemplateId
            return Ok(response);
        }

    }
}
