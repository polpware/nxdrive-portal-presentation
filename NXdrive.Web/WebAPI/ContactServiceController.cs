﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Models.Customer;
using Polpware.OAuth2.Security.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.WebAPI
{
    [Route("api/ContactService")]
    [EnableCors("AllowLocalHost")]
    public class ContactServiceController : ControllerBase
    {
        private readonly IUserIdentityService _userIdentityService;
        private readonly ICustomerService _customerService;

        public ContactServiceController(ICustomerService customerService,
            IUserIdentityService userIdentityService)
        {
            _customerService = customerService;
            _userIdentityService = userIdentityService;
        }

        [HttpPost]
        [RegisterUserAuthorization]
        public async Task<IActionResult> Post(ContactLiveSearchModel model)
        {
            model.name = model.name ?? string.Empty;
            var provider = model.onlyUser ? PredefinedUserProvider.NXdriveAccount : PredefinedUserProvider.Min;

            var email = HttpContext.User.Identity.Name;
            var customer = _customerService.GetCustomerByEmail(email);

            // Only show the first 15
            var matched = await _userIdentityService.SearchConnections(customer.Id, model.name, provider, model.pageNumber, model.pageSize);

            var data = matched.Select(x => new
            {
                value = x.Item2.Id,
                customerId = x.Item2.DerivedUserId,
                text = x.Item2.Email,
                userName = customer.GetFullName(),
                disabled = false
            });

            return Ok(data);
        }
    }
}
