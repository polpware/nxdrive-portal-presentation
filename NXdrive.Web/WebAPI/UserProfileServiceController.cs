﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Media;
using NXdrive.NopExt.ServicePlans;
using Polpware.NopExt.SubsPlan.Orders;
using Polpware.OAuth2.Security.Authorization;
using System;
using System.Threading.Tasks;

namespace NXdrive.Web.WebAPI
{
    [EnableCors("AllowLocalHost")]
    [Route("api/UserProfileService")]
    [ApiController]
    public class UserProfileServiceController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IPlanSpecificationService _planSpecificationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;
        private readonly OrderFactoryService _orderFactoryService;
        private readonly IPictureService _pictureService;

        public UserProfileServiceController(ICustomerService customerService, 
            IPlanSpecificationService planSpecificationService,
            IGenericAttributeService genericAttributeService,
            SubstOrientedOrderAccessService substOrientedOrderAccessService,
            OrderFactoryService orderFactoryService,
            IPictureService pictureService)
        {
            this._customerService = customerService;
            this._planSpecificationService = planSpecificationService;

            this._genericAttributeService = genericAttributeService;
            this._substOrientedOrderAccessService = substOrientedOrderAccessService;
            this._orderFactoryService = orderFactoryService;
            this._pictureService = pictureService;
        }

        [HttpGet]
        [HttpPost]
        [RegisterUserAuthorization]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() =>
            {
                var email = HttpContext.User.Identity.Name;
                var customer = _customerService.GetCustomerByEmail(email);

                var firstname = customer.GetAttribute<String>(SystemCustomerAttributeNames.FirstName, _genericAttributeService);
                var lastname = customer.GetAttribute<String>(SystemCustomerAttributeNames.LastName, _genericAttributeService);

                var order = _substOrientedOrderAccessService.EffectiveOrder(customer);
                // There must be one order, otherwise, exception.
                var plan = _orderFactoryService.CreateBaseOrder(order);
                var spec = _planSpecificationService.GetPlanSpecification(plan.Product);

                var imageId = customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId);
                string imageContent = ""; // default empty or null?
                if (imageId > 0)
                {
                    var image = _pictureService.GetPictureById(imageId);
                    // imageContent = System.Text.Encoding.UTF8.GetString(image.PictureBinary);
                    imageContent = Convert.ToBase64String(image.PictureBinary); 
                }

                return Ok(new
                {
                    email = email,
                    firstName = firstname,
                    lastName = lastname,
                    userID = customer.Id,
                    userGUID = customer.CustomerGuid,
                    // file system
                    avatarContent = imageContent,
                    plan = plan.Product.Name,
                    expireDate = plan.IneffectiveOnUtc,
                    uploadFileLimitInMB = spec.SingleFileUpperLimit,
                    capacityInGig = spec.Capacity
                });
            });
        }
    }
}