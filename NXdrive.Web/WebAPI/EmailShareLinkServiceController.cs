﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Customers;
using NXdrive.Web.Models.API.Emails;
using NXdrive.Web.Models.API.Response;
using Polpware.OAuth2.Security.Authorization;

namespace NXdrive.Web.WebAPI
{
    [Route("api/EmailShareLinkService")]
    [EnableCors("AllowLocalHost")]
    public class EmailShareLinkServiceController : ControllerBase
    {

        private readonly Extensions.SendEmailService _sendEmailService;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;

        public EmailShareLinkServiceController(IWorkContext workContext, 
            Extensions.SendEmailService sendEmailService,
            ICustomerService customerService)
        {
            _workContext = workContext;
            _sendEmailService = sendEmailService;
            _customerService = customerService;
        }

        [HttpPost]
        [RegisterUserAuthorization]
        public IActionResult Post([FromForm] EmailShareLinkModel model)
        {
            var response = new DataPayload<AccountOpError>
            {
                Version = 1
            };
            var email = HttpContext.User.Identity.Name;
            var senderCustomer = _customerService.GetCustomerByEmail(email);

            foreach (var receiverEmail in model.ReceiverAsList)
            {
                bool isTargetOurCustomer = true;
                var targetCustomer = _customerService.GetCustomerByEmail(receiverEmail);
                if (targetCustomer == null)
                    isTargetOurCustomer = false;

                if (isTargetOurCustomer)
                {
                    _sendEmailService.SendSharelink2User(senderCustomer, targetCustomer,
                        model.ShareLinkURL,
                        _workContext.WorkingLanguage.Id,
                        model.ItemName,
                        model.IsFolder,
                        model.Message);
                }
                else
                {
                    _sendEmailService.SendSharelink2Visitor(senderCustomer, receiverEmail,
                        model.ShareLinkURL,
                        _workContext.WorkingLanguage.Id, 
                        model.ItemName,
                        model.IsFolder,
                        model.Message);
                }
            }

            // todo: Send email service
            // todo: convert from EmailTypeId to emailTemplateId
            return Ok(response);
        }

    }
}
