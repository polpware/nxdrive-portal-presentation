﻿using Nop.Core.Domain.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;
using System.Threading.Tasks;

namespace NXdrive.Web.Constraints
{
    public interface IUserGroupConstraints
    {
        /// <summary>
        /// Check if a group is valid by a set of built-in rules
        /// </summary>
        /// <param name="group">Supposed group</param>
        /// <param name="customer">Customer who is associated with this group</param>
        /// <returns>Constraint Result</returns>
        Task<GroupConstraintCode> IsValidGroupName(GroupModel group, Customer customer, bool editCase);

        /// <summary>
        /// Check if a user may create a group or not.
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Constraint Result</returns>
        Task<GroupConstraintCode> CanCreateGroup(Customer customer);

        /// <summary>
        /// Check if a user can edit a group or not.
        /// </summary>
        /// <param name="group">group model</param>
        /// <param name="customer">Customer</param>
        /// <returns>Constraint Result</returns>
        GroupConstraintCode CanEditGroup(IUserGroup group, Customer customer);

        /// <summary>
        /// Check if a user can delete a group or not
        /// </summary>
        /// <param name="group">group model</param>
        /// <param name="customer">Customer</param>
        /// <returns>Constraint Result</returns>
        GroupConstraintCode CanDeleteGroup(IUserGroup group, Customer customer);

        // todo: check if user can manage group members

        // todo: check if user can update other members status in a group

        /// <summary>
        /// Check if a user can invite more people to his group
        /// for the time-being, only check if group member number exceeds the owner's quota
        /// </summary>
        /// <param name="group">group model</param>
        /// <param name="customer">customer</param>
        /// <returns>Constraint Result</returns>
        Task<GroupConstraintCode> CanInvitePeople(IUserGroup group);

        /// <summary>
        /// Check if a user can apply for one more group
        /// result depends on user's plan for the time-being
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        Task<GroupConstraintCode> CanApplyGroup(int customerId);

        /// <summary>
        /// put customer into group if customer is not in group yet
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="customerId"></param>
        Task ValidateGroup(int groupId, int customerId);
    }
}
