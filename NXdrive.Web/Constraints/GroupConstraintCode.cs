﻿namespace NXdrive.Web.Constraints
{
    public enum GroupConstraintCode
    {
        NoError = 0,    // code of no error 
        Error = 10000,  // unclassified error

        // User group relavent 
        UserGroup_Start = 100,

        // Constraints on user group basic property
        UserGroup_NameNotUnique = 10005,
        UserGroup_NameEmpty = 10001,
        UserGroup_NameTooLong = 10004,
        UserGroup_GroupNotFound = 10003,
        UserGroup_NameTooShort = 10002,
        UserGroup_NameInvalid = 10022,
        UserGroup_GroupHasBeenDeleted = 10015,
        

        // Constraints on user group member and invitation       
        UserGroup_YouJoinedTooManyGroups = 10013,
        UserGroup_NotOwnedByUser = 10006,
        UserGroup_GroupInvitationInvalid = 10014,
        UserGroup_PeopleAlreadyJoined = 10010,
        UserGroupMember_AcceptInvitationSuccess = 135,
        UserGroupMember_DeclinedInvitation = 136,
        UserGroup_CannotRemoveOwner = 10007,
        UserGroup_GroupFull = 10009,
        UserGroup_CannotTakeBackDeclinedStuff = 10008,
        UserGroup_UserHasAlreadyJoined = 10011,
        UserGroup_AlreadyInvited = 10012,
        UserGroup_InvitationTokenInvalid = 10016,
        UserGroup_InvitationNotFound = 10017,
        UserGroup_MemberCannotViewGroupInvitations = 10018,
        UserGroup_AlreadyApplied = 10019,
        UserGroup_MembershipNotFound = 10020,
        UserGroup_UserJoinedTooManyGroups = 10021,
        UserGroup_EmailResendTooOften = 10023,

        // avatar
        UserGroup_AvatarTooBig = 11001,
        UserGroup_AvatarUploadError = 11000
    }
}
