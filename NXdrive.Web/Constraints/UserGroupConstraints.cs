﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;

namespace NXdrive.Web.Constraints
{
    public class UserGroupConstraints : IUserGroupConstraints
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IMemberInvitationService _memberInvitationService;
        private readonly IGroupMemberService _groupMemberService;

        public UserGroupConstraints(IUserGroupService userGroupService,
            IMemberInvitationService memberInvitationService,
            IGroupMemberService groupMemberService)
        {
            _userGroupService = userGroupService;
            _memberInvitationService = memberInvitationService;
            _groupMemberService = groupMemberService;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public async Task<GroupConstraintCode> CanCreateGroup(Customer customer)
        {
            // todo: Extending user plan settings

            // Each user is assigned a quota... , based on his/her plan
            var maxGroupNum = 30;

            // Rule 1:
            // get the number of groups that the user has joined,
            // pending /rejected groups does not count

            var currentGroupNum = (await _userGroupService.FindAssociatedGroups(customer.Id)).Count();

            if (currentGroupNum >= maxGroupNum)
            {
                return GroupConstraintCode.UserGroup_YouJoinedTooManyGroups;
            }

            return GroupConstraintCode.NoError;
        }

        /// <summary>
        /// a valid group name means it can be used directly as a folder name
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns>whether this name is valid</returns>
        private bool ValidateGroupName(string groupName)
        {
            Regex regex = new Regex("^([a-zA-Z0-9][^*/><?\"|:]*)$");
            return regex.IsMatch(groupName);
        }

        /// <summary>
        /// validate group name
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customer"></param>
        /// <param name="editCase"></param>
        /// <returns></returns>
        public async Task<GroupConstraintCode> IsValidGroupName(GroupModel model, Customer customer, bool editCase)
        {
            // new model should have a valid name which is not empty
            if (string.IsNullOrEmpty(model.Name))
            {
                return GroupConstraintCode.UserGroup_NameEmpty;
            }

            // new name cannot be longer than 64
            if (model.Name.Length >= 64)
            {
                return GroupConstraintCode.UserGroup_NameTooLong;
            }

            // new name must be a valid folder/file name
            if (!ValidateGroupName(model.Name))
            {
                return GroupConstraintCode.UserGroup_NameInvalid;
            }

            // new name should not conflict with any existing groups in the system
            var sameNameGroup = await _userGroupService.CheckGroupName(model.Name);
            if(sameNameGroup.Count()>0 && !editCase)
                return GroupConstraintCode.UserGroup_NameNotUnique;

            return GroupConstraintCode.NoError;
        }

        /// <summary>
        /// defines who can edit the group
        /// only creator can edit group for the time-being
        /// </summary>
        /// <param name="group"></param>
        /// <param name="customer">current customer</param>
        /// <returns>true if can edit</returns>
        public GroupConstraintCode CanEditGroup(IUserGroup group, Customer customer)
        {
            // for the timebeing, as long as user is the creator of group, he/she can edit the group
            if (group.CreatorId != customer.Id)
                return GroupConstraintCode.UserGroup_NotOwnedByUser;
            else
                return GroupConstraintCode.NoError;
        }

        /// <summary>
        /// defines who can delete the group
        /// only creator can delete the group for the time-being
        /// </summary>
        /// <param name="group"></param>
        /// <param name="customer"></param>
        /// <returns>true if allowed to delete group</returns>
        public GroupConstraintCode CanDeleteGroup(IUserGroup group, Customer customer)
        {
            // for the timebeing, as long as user is the creator of group, he/she can delete the group
            if (group.CreatorId != customer.Id)
                return GroupConstraintCode.UserGroup_NotOwnedByUser;
            else
                return GroupConstraintCode.NoError;
        }

        /// <summary>
        /// defines whether this customer can invite more people
        /// cannot invite more people if the group member count is too big
        /// </summary>
        /// <param name="group"></param>
        /// <param name="customer"></param>
        /// <returns>true if allowed to invite more people</returns>
        public async Task<GroupConstraintCode> CanInvitePeople(IUserGroup group)
        {
            var memberCount = (await this._groupMemberService.GetGroupMembers(group.Id)).Count();

            // todo: add a Field in Customer class to indicate maximum number of group members allowed
            // var maxMemberCount = customer.Id % 100;
            var maxMemberCount = 100;

            if (memberCount >= maxMemberCount)
                return GroupConstraintCode.UserGroup_GroupFull;
            else
                return GroupConstraintCode.NoError;

        }

        public async Task<GroupConstraintCode> CanApplyGroup(int customerId)
        {
            // todo: read this field from user plan in the future
            var maxGroupNum = 50;

            var currentGroupNum = (await this._userGroupService.FindAssociatedGroups(customerId)).Count();

            if (currentGroupNum >= maxGroupNum)
            {
                return GroupConstraintCode.UserGroup_YouJoinedTooManyGroups;
            }

            return GroupConstraintCode.NoError;
        }

        /// <summary>
        /// put customer into group if customer is not in group yet
        /// </summary>
        public async Task ValidateGroup(int groupId, int customerId)
        {
            var userInGroup = (await _groupMemberService.GetGroupMembers(groupId)).Any(x => x.CustomerId == customerId);
            if (userInGroup)
            {
                return;
            }
            else
            {
                // We do not need to worry about its origin
                await _groupMemberService.AddMemberIntoGroup(groupId, customerId);
            }
        }
    }
}
