﻿namespace NXdrive.Web.Constraints
{
    public enum PortalConstraintCode
    {
        NoError = 0,    // code of no error 
        Error = 10000,  // unclassified error

        NotificationCannotInsert = 12000,
        NotificationNotFound = 12001,
        AddressNotFound = 12002,
        AddressInvalid = 12003,
        AddressDeleted = 12004,
        CannotDeleteLastAddress = 12005,
        ContactNotFound = 12006,
        ContactDeleted = 12007,
        ContactRequestDeleted = 12008,

        AlreadyInvited = 13000
    }
}
