﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using System;
using System.Linq;

namespace NXdrive.Web.Tests
{
    public class TestWorkContext : IWorkContext
    {

        private readonly CurrencySettings _currencySettings;
        private readonly IAuthenticationService _authenticationService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILanguageService _languageService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUserAgentHelper _userAgentHelper;
        private readonly IVendorService _vendorService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly TaxSettings _taxSettings;

        private Customer _cachedCustomer;
        private Customer _originalCustomerIfImpersonated;
        private Vendor _cachedVendor;
        private Language _cachedLanguage;
        private Currency _cachedCurrency;
        private TaxDisplayType? _cachedTaxDisplayType;

        private const string TestEmail = "xxlongtang@gmail.com";

        public TestWorkContext(CurrencySettings currencySettings,
            IAuthenticationService authenticationService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            IHttpContextAccessor httpContextAccessor,
            ILanguageService languageService,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IUserAgentHelper userAgentHelper,
            IVendorService vendorService,
            LocalizationSettings localizationSettings,
            TaxSettings taxSettings)
        {
            this._currencySettings = currencySettings;
            this._authenticationService = authenticationService;
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._httpContextAccessor = httpContextAccessor;
            this._languageService = languageService;
            this._storeContext = storeContext;
            this._storeMappingService = storeMappingService;
            this._userAgentHelper = userAgentHelper;
            this._vendorService = vendorService;
            this._localizationSettings = localizationSettings;
            this._taxSettings = taxSettings;
        }

        public Customer CurrentCustomer
        {
            get
            {
                return _customerService.GetCustomerByEmail(TestEmail);
            }
            set => throw new NotImplementedException();
        }

        public Customer OriginalCustomerIfImpersonated => throw new NotImplementedException();

        public Vendor CurrentVendor
        {
            get
            {
                return _vendorService.GetVendorById(this.CurrentCustomer.VendorId);
            }
        }

        public Language WorkingLanguage
        {
            get
            {
                var customerLanguage = _languageService.GetAllLanguages().FirstOrDefault();
                return customerLanguage;
            }
            set => throw new NotImplementedException();
        }
        public Currency WorkingCurrency
        {
            get {
                var customerCurrency = _currencyService.GetAllCurrencies().FirstOrDefault();
                return customerCurrency;
            }
            set => throw new NotImplementedException();
        }
        public TaxDisplayType TaxDisplayType
        {
            get
            {
                return TaxDisplayType.IncludingTax;
            }
            set => throw new NotImplementedException();
        }
        public bool IsAdmin { get => false; set => throw new NotImplementedException(); }
    }
}
