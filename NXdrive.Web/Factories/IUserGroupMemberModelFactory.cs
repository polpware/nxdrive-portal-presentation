﻿using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;
using System.Threading.Tasks;

namespace NXdrive.Web.Factories
{
    public interface IUserGroupMemberModelFactory
    {
        /// <summary>
        /// Builds the right information for the given member model based on the 
        /// the given group information and member information.
        /// That is, this method conducts in-place writes for the given model.
        /// </summary>
        /// <remarks>
        /// Besides, please note the following facts: 
        /// <para>1. This method uses the current active user in the request session to decide 
        /// the permission on the resulting model.
        /// </para>
        /// <para>
        /// 2. We on purpose use the last parameter as the output information, as this 
        /// design allows the caller (of this method) to decide 
        /// whether to use the exact group member model or a dervied one.
        /// </para>
        /// </remarks>
        /// <param name="group">group model from db</param>
        /// <param name="member">a member of this group</param>
        /// <param name="memberModel">member model with attributes set</param>
        void PrepareGroupMemberModel(IUserGroup group, IUserGroupMember member, MemberModel memberModel);

        /// <summary>
        /// build a model of view for unregistered user
        /// </summary>
        /// <param name="group"></param>
        /// <param name="email"></param>
        /// <param name="memberModel"></param>
        [System.Obsolete]
        void PrepareUnregisteredGroupMemberModel(IUserGroup group, string email, MemberModel memberModel);

        /// <summary>
        /// Builds the right information for the given inivation model 
        /// based on the given group information and invitation information. 
        /// That is, this method conducts in-place writes for the given model.
        /// </summary>
        /// <remarks>
        /// Besides, please note the following facts: 
        /// <para>
        /// 1. This method uses the current active user in the request session to decide 
        /// the permission on the resulting model, including 
        /// <c>AllowDelete</c> and <c>AllowAcceptDeny</c>, 
        /// in InvitationModel depend on who is asking for this model
        /// </para>
        /// <para>
        /// 2. We on purpose use the last parameter as the output information, as this 
        /// design allows the caller (of this method) to decide 
        /// whether to use the exact group member model or a dervied one. 
        /// </para>
        /// </remarks>
        /// <param name="group">group model</param>
        /// <param name="invitation">db layer invitation model</param>
        /// <param name="invitationModel">frontend layer invitation model</param>
        Task PrepareGroupInvitationModel(IUserGroup group, IUserGroupInvitation invitation, InvitationModel invitationModel);
    }
}
