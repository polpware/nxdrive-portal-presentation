﻿using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Web.Models.Customer;

namespace NXdrive.Web.Factories
{
    public interface ICustomerContactModelFactory
    {
        /// <summary>
        /// Builds the given model from the given connection and identity.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="identity"></param>
        /// <param name="model"></param>
        void BuildContactModel(ISocialNetwork connection, IEmailIdentity identity, CustomerContactModel model);
    }
}
