﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Infrastructure.Mapper;
using Nop.Services.Customers;
using Nop.Services.Media;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NXdrive.Web.Factories
{
    public class UserGroupModelFactory : IUserGroupModelFactory
    {
        private readonly IPictureService _pictureService;
        private readonly IUserGroupService _userGroupService;
        private readonly ICustomerService _customerService;
        private readonly IGroupMemberService _groupMemberService;
        private readonly MediaSettings _mediaSettings;
        private readonly IMemberInvitationService _memberInvitationService;

        // fields to improve performance of listing groups
        public int maxPreviewMemberCount = 3;  // maximum number of group members that shows in the member preview tool tip
        private int maxPreviewGroupCount = 5;   // if there are more than a certain groups that user is in, only show the owner in member preview tool tip

        public UserGroupModelFactory(IPictureService pictureService,
            IUserGroupService userGroupService,
            IGroupMemberService groupMemberService,
            MediaSettings mediaSettings,
            ICustomerService customerService,
            IMemberInvitationService memberInvitationService)
        {
            this._groupMemberService = groupMemberService;  // todo: this service is not needed
            this._pictureService = pictureService;
            this._userGroupService = userGroupService;
            this._mediaSettings = mediaSettings;
            this._customerService = customerService;
            this._memberInvitationService = memberInvitationService;
        }

        public void PrepareGroupModel(IUserGroup group, Customer customer, GroupModel model,
            IEnumerable<IUserGroupInvitation> usersInvitations = null,
            IEnumerable<IUserGroup> joinedGroups = null)
        {
            AutoMapperConfiguration.Mapper.Map(group, model);

            var tyGroup = group as UserGroup;

            model.CreatedOn = group.CreatedOnUtc;
            model.IsPublic = !group.Privacy;
            model.IsAdmin = (group.AdminId == customer.Id);

            // todo: Make sure that the following assignments are ok
            model.MemberCount = tyGroup.Members.Count(x => !x.IsDeleted);
            model.ImpendingMemberCount = tyGroup.Invitations.Count(x => !x.IsDeleted);

            model.NewGroup = false;
            model.IsMember = false;
            model.IsInvited = false;
            model.IsApplied = false;

            // calculate the relationship between this customer and this group
            if (usersInvitations != null && usersInvitations.Count() > 0)
            {
                var invitedOrAppliedRecord = usersInvitations.FirstOrDefault(x => x.GroupId == group.Id && x.IsDeleted == false);
                if (invitedOrAppliedRecord != null)
                {
                    if (invitedOrAppliedRecord.CreatorId == group.CreatorId)
                    {
                        model.IsInvited = true;
                    }
                    else
                    {
                        model.IsApplied = true;
                    }

                    model.AssociationId = invitedOrAppliedRecord.Id;
                }
            }

            if (joinedGroups != null && joinedGroups.Count() > 0)
            {
                model.IsMember = joinedGroups.Any(x => x.Id == group.Id && x.IsDeleted == false);
            }

            model.AvatarUrl = _pictureService.GetPictureUrl(group.AvatarImageId,
                _mediaSettings.AvatarPictureSize, true);
        }

        public async Task<string> PrepareGroupDesc(IUserGroup group, bool brief)
        {
            if (_userGroupService == null)
                return "";
            var memberList = (await _groupMemberService.GetGroupMembers(group.Id)).ToList();
            string creatorName = _customerService.GetCustomerById(group.CreatorId).GetFullName();

            if (brief)
            {
                return "created by " + creatorName;
            }

            int memberCount = memberList.Count();
            string memberDesc = "";
            int count = memberCount < maxPreviewMemberCount ? memberCount : maxPreviewMemberCount;

            for (int i = 0; i < count; i++)
            {
                var member = memberList[i];
                var memberName = _customerService.GetCustomerById(member.CustomerId).GetFullName();
                if (!memberName.Equals(creatorName))
                {
                    memberDesc += memberName;
                }
                else
                {
                    continue;
                }
                if (i < count - 1)
                    memberDesc += ", ";
            }
            if (memberList.Count() > maxPreviewMemberCount)
                return creatorName + ", " + memberDesc + " and " + (memberList.Count() - maxPreviewMemberCount) + " others";
            else if (memberDesc.Equals(""))
            {
                return creatorName;     // only creator
            }
            else
                return creatorName + ", " + memberDesc; // creator and some other members
        }

    }
}
