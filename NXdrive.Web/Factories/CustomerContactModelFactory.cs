﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Media;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.DataModeling.UserIdentity.Domain;
using NXdrive.Web.Models.Customer;

namespace NXdrive.Web.Factories
{
    public class CustomerContactModelFactory : ICustomerContactModelFactory
    {
        private readonly ICustomerService _customerService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;

        public CustomerContactModelFactory(ICustomerService customerService,
            IPictureService pictureService,
            MediaSettings mediaSettings)
        {
            _customerService = customerService;
            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
        }

        private void BuildNXdriveContact(IEmailIdentity identity, CustomerContactModel model)
        {
            var customer = _customerService.GetCustomerById(identity.DerivedUserId);
            if (customer != null)
            {
                // Customer id
                model.CustomerId = customer.Id;
                // Customer avatar
                var pictureId = customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId);
                model.AvatarURL =  _pictureService.GetPictureUrl(pictureId,
                   _mediaSettings.AvatarPictureSize,
                   true);
                // Customer full name
                model.FullName = customer.GetFullName();
            }
        }

        private void BuildEmailContact(SocialNetwork network, IEmailIdentity identity, CustomerContactModel model)
        {
            // Contact name (might be empty)
            model.FullName = network.GetAttribute<string>(Extensions.SystemAttributeExtension.CustomerContactName);

            // An alternative to avatar
            var splitName = identity.Email.Split('@');
            // At this moment, we may assume the correctness of the email

            if (splitName.Count() >= 2)
            {
                char[] chars = { splitName[0][0], splitName[1][0] };
                model.Abbr = new string(chars);
            }

            // Comments
            model.Comment = network.GetAttribute<string>(Extensions.SystemAttributeExtension.CustomerContactComment);
        }

        public void BuildContactModel(ISocialNetwork connection, IEmailIdentity identity, CustomerContactModel model)
        {
            if (identity == null || connection == null)
            {
                return;
            }

            // Build the common part
            model.ConnectionId = connection.Id;
            model.IdentityId = identity.Id;
            model.CreatedDate = identity.CreatedOnUtc;
            model.Email = identity.Email;

            model.IsNXdriveUser = identity.DerivedProviderId == (short)PredefinedUserProvider.NXdriveAccount;
            if (model.IsNXdriveUser)
            {
                BuildNXdriveContact(identity, model);
            }
            else
            {
                // Now the only other case
                BuildEmailContact(connection as SocialNetwork, identity, model);
            }
        }
    }
}
