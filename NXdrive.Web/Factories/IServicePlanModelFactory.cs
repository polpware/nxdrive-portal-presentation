﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using NXdrive.NopExt.ServicePlans;
using NXdrive.Web.Models.Customer;

namespace NXdrive.Web.Factories
{
    public interface IServicePlanModelFactory
    {
        CustomerServicePlanModel PrepareCustomerServicePlanModel(Customer customer);

        ServicePlanDetailModel PrepareServicePlanDetailModel(Product product, Product currentProduct =null);
    }
}
