﻿using Nop.Core.Domain.Customers;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NXdrive.Web.Factories
{
    public interface IUserGroupModelFactory
    {
        /// <summary>
        /// Builds the user group model with the given information. Note that
        /// we have a group model as an input type. This is because
        /// the caller can decide whether to use the exact group model or a dervied one.
        /// </summary>
        /// <param name="group">User group</param>
        /// <param name="customer">Customer who is requesting to construct the group model</param>
        /// <param name="model">Group model</param>
        void PrepareGroupModel(IUserGroup group, Customer customer, GroupModel model,
            IEnumerable<IUserGroupInvitation> associatedInvitationOrApplications = null, IEnumerable<IUserGroup> joinedGroups = null);


        /// <summary>
        /// helper to get a brief description of a group
        /// </summary>
        /// <param name="group">group model from db</param>
        /// <param name="brief">description will be shorter if this true</param>
        /// <returns></returns>
        [Obsolete("user will edit this field now instead of system generate")]
        Task<string> PrepareGroupDesc(IUserGroup group, bool brief);


    }
}
