﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Vendors;
using Nop.Core.Infrastructure.Mapper;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Security.Captcha;
using NXdrive.NopExt.ServicePlans;
using NXdrive.Web.Extensions;
using NXdrive.Web.Models.Customer;
using Polpware.NopExt.SubsPlan.Orders;
using Polpware.NopWeb.Factories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NXdrive.Web.Factories
{
    public class ServicePlanModelFactory : ProductModelFactory, IServicePlanModelFactory
    {
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPlanSpecificationService _planSpecificationService;
        private readonly IProductService _productService;
        private readonly SubstOrientedOrderAccessService _substOrientedOrderAccessService;
        private readonly OrderFactoryService _orderFactoryService;

        public ServicePlanModelFactory(ISpecificationAttributeService specificationAttributeService, ICategoryService categoryService, IManufacturerService manufacturerService,
            IProductService productService, IVendorService vendorService, IProductTemplateService productTemplateService,
            IProductAttributeService productAttributeService, IWorkContext workContext, IStoreContext storeContext,
            ITaxService taxService, ICurrencyService currencyService, IPictureService pictureService,
            ILocalizationService localizationService, IMeasureService measureService, IPriceCalculationService priceCalculationService, IPriceFormatter priceFormatter,
            IWebHelper webHelper, IDateTimeHelper dateTimeHelper, IProductTagService productTagService, IAclService aclService, IStoreMappingService storeMappingService,
            IPermissionService permissionService, IDownloadService downloadService, IProductAttributeParser productAttributeParser, IDateRangeService dateRangeService,
            MediaSettings mediaSettings, CatalogSettings catalogSettings, VendorSettings vendorSettings, CustomerSettings customerSettings, CaptchaSettings captchaSettings,
            OrderSettings orderSettings, SeoSettings seoSettings, IStaticCacheManager cacheManager, IPlanSpecificationService specService,
            SubstOrientedOrderAccessService substOrientedOrderAccessService,
            OrderFactoryService orderFactoryService)
            : base(specificationAttributeService, categoryService, manufacturerService, productService, vendorService, productTemplateService, productAttributeService, workContext, storeContext, taxService, currencyService, pictureService, localizationService, measureService, priceCalculationService, priceFormatter, webHelper, dateTimeHelper, productTagService, aclService, storeMappingService, permissionService, downloadService, productAttributeParser, dateRangeService, mediaSettings, catalogSettings, vendorSettings, customerSettings, captchaSettings, orderSettings, seoSettings, cacheManager)
        {
            this._dateTimeHelper = dateTimeHelper;
            this._planSpecificationService = specService;
            this._productService = productService;

            this._substOrientedOrderAccessService = substOrientedOrderAccessService;
            this._orderFactoryService = orderFactoryService;
        }


        public CustomerServicePlanModel PrepareCustomerServicePlanModel(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }

            // TODO: Optimize. E.g., reduce traffice between wcf and web
            var model = new CustomerServicePlanModel();
            var order = _substOrientedOrderAccessService.EffectiveOrder(customer);
            if (order == null)
            {
                return model;   // not likely to happen, should consider as bug
            }
            var orderItem = order.OrderItems.FirstOrDefault();
            if (null == orderItem) throw new Exception("No orderitem");

            var product = orderItem.Product;
            if (null == product) throw new Exception("No product");

            model.CreatedOn = this._dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
            model.IsCancelled = order.OrderStatus == OrderStatus.Cancelled;

            if (model.IsCancelled)
            {
                var orderWrapper = _orderFactoryService.CreateBaseOrder(order);
                model.EndOn = orderWrapper.IneffectiveOnUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(orderWrapper.IneffectiveOnUtc.Value, DateTimeKind.Utc).ToString("f") : null;
                var nextOrder = _substOrientedOrderAccessService.NextOrder(order);
                if (nextOrder != null)
                {
                    var nextOrderItem = nextOrder.OrderItems.FirstOrDefault();
                    if (nextOrderItem != null)
                    {
                        model.NextPlanDetailModel = PrepareServicePlanDetailModel(nextOrderItem.Product);
                    }
                }
            }

            model.CurrentPlanDetailModel = PrepareServicePlanDetailModel(product);
            model.PlanPreferenceModel.IsTopSecurity = customer.GetAttribute<bool>(SystemAttributeExtension.IsTopSecurityChosed);

            // prepare usage model
            // todo: Design a new service to conduct the statistics about the file system of a user.
            /*
            var accountId = customer.GetAttribute<Int64>(SystemAttributeExtension.AccountId);
            var data = _fileSystemService.GetStorageUsage(customer.Id, new Runtime.Internal.Services.Request.StorageUsageRequest
            {
                SkipMimeClassification = false
            });
            var sizeInKB = data.TotalSize / 1024;

            var spec = product.GetSpecification();

            double ratio = (double)sizeInKB / spec.CapacityInKB;

            var formatedRation = string.Format("{0:p}", ratio);

            model.UsageModel.UseRation = formatedRation;

            model.UsageModel.FileCount = data.FileCount.ToString();

            model.UsageModel.ImageCount = data.ImageCount.ToString();

            model.UsageModel.DocumentCount = data.DocumentCount.ToString();

            model.UsageModel.TrashSize = data.TrashSize.ToString();

            model.UsageModel.TrashCount = data.TrashCount.ToString();
            */

            return model;
        }

        public ServicePlanDetailModel PrepareServicePlanDetailModel(Product product, Product currentProduct=null)
        {
            var spec = _planSpecificationService.GetPlanSpecification(product);
            ProductSpecification currentProductSpecification = null;
            if (currentProduct != null)
            {
                currentProductSpecification = _planSpecificationService.GetPlanSpecification(currentProduct);
            }

            var model = new ServicePlanDetailModel
            {
                Id = product.Id,
                Name = product.Name,
                ShortDescription = product.ShortDescription,
                FullDescription = product.FullDescription,
                IsEnrolled = false,
                SingleFileUpperLimit =spec.SingleFileUpperLimit,
                IsTwoLayerEncryption = spec.IsTwoLayerEncryption,
                RelibilityIndicatorLower = spec.RelibilityIndicatorLower,
                RelibilityIndicatorUpper = spec.RelibilityIndicatorUpper,
                SecurityIndicatorNLower = spec.SecurityIndicatorNLower,
                SecurityIndicatorNUppper = spec.SecurityIndicatorNUppper,
                SecurityIndicatorOther = spec.SecurityIndicatorOther,
                PrivacyTrustworthinessId = spec.PrivacyTrustworthinessId,
                IsFileShareAllowed = spec.IsFileShareAllowed,
                IsVersionControlEnabled = spec.IsVersionControlEnabled,
                IsAdvertisementAllowed = spec.IsAdvertisementAllowed,
                IsFileSynchroizationEnabled = spec.IsFileSynchroizationEnabled,
                BandwidthUpperLimit = spec.BandwidthUpperLimit,
                Capacity = spec.Capacity,
                ServiceGroup = spec.ServiceGroup,
                ServiceGroupRepresentative = spec.ServiceGroupRepresentative,
            };
            model.ProductPrice = PrepareProductOverviewPriceModel(product);
            model.ProductPrice.PriceValue = decimal.Round(model.ProductPrice.PriceValue, 2, MidpointRounding.AwayFromZero);

            // AutoMapperConfiguration.Mapper.Map(spec, model); // convert from ProductSpecification to ServicePlanDetailModel
            if (currentProduct != null)
            {
                if (currentProduct.Id == model.Id)
                    model.IsEnrolled = true;
                else if (currentProductSpecification != null)
                {
                    if (currentProductSpecification.ServiceGroup.Equals("Group1")
                      || (currentProductSpecification.ServiceGroup.Equals(model.ServiceGroup)
                              && currentProduct.Price < model.ProductPrice.PriceValue))
                        model.IsUpgradeOption = true;
                }
            }

            // prepare the hightlights of this model
            // todo: put those numeric attributes in the hightlights

            return model;
        }


    }
}
