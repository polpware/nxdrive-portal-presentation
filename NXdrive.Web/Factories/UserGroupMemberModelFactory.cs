﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Customers;
using Nop.Services.Media;
using NXdrive.DataModeling.Internal.UserIdentity;
using NXdrive.Services.Internal.UserIdentity;
using NXdrive.Web.Models.UserGroup;
using System;
using System.Threading.Tasks;

namespace NXdrive.Web.Factories
{
    public class UserGroupMemberModelFactory : IUserGroupMemberModelFactory
    {
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IUserIdentityService _userIdentityService;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;

        public UserGroupMemberModelFactory(IWorkContext workContext,
            ICustomerService customerService,
            IUserIdentityService userIdentityService,
            IPictureService pictureService,
            MediaSettings mediaSettings)
        {
            this._workContext = workContext;
            this._customerService = customerService;
            this._userIdentityService = userIdentityService;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
        }

        public void PrepareGroupMemberModel(IUserGroup group, IUserGroupMember member, MemberModel memberModel)
        {
            memberModel.Id = member.Id;
            memberModel.CreatedOn = member.CreatedOnUtc;
            memberModel.DeletedOn = member.DeletedOnUtc;
            memberModel.CustomerId = member.CustomerId;
            memberModel.GroupId = group.Id;

            var currentCustomer = this._workContext.CurrentCustomer;
            if (member.CustomerId != currentCustomer.Id)
            {
                var memberUser = this._customerService.GetCustomerById(member.CustomerId);
                memberModel.CustomerEmail = memberUser.Email;
                var name = memberUser.GetFullName();
                memberModel.CustomerName = string.IsNullOrEmpty(name) ? memberUser.Email : name;
            }
            else
            {
                memberModel.CustomerEmail = currentCustomer.Email;
                var name = currentCustomer.GetFullName();
                memberModel.CustomerName = string.IsNullOrEmpty(name) ? currentCustomer.Email : name;
            }

            // allow delete in two cases:
            // - current viewer is the group creator
            // - current viewer is a member of the group
            // never allow delete when the owner is viewing himself
            memberModel.AllowDelete = (currentCustomer.Id == group.CreatorId || currentCustomer.Id == member.CustomerId)
                && (member.CustomerId != group.CreatorId);

            memberModel.IsAboutMe = currentCustomer.Id == member.CustomerId;
        }

        private static bool CanDeleteInvitation(Customer customer, IUserGroup group, IUserGroupInvitation invitation)
        {
            // The owner of the group associated with the invition can
            if (customer.Id == group.AdminId)
            {
                return true;
            }

            // If the invitation is not yet declined, then the creator for this invitation can do so
            if (!invitation.Declined && invitation.CreatorId == customer.Id)
            {
                return true;
            }

            return false;
        }

        private static bool CanAcceptOrDenyInvitation(Customer customer, IUserGroup group, IUserGroupInvitation invitation)
        {
            if (invitation.Declined)
            {
                return false;
            }

            // The creator is not allowed to do so ...
            if (customer.Id == invitation.CreatorId)
            {
                return false;
            }

            if (invitation.ProviderId == (short)PredefinedUserProvider.NXdriveAccount) {
                // The invitation is created by the target user,
                // so this falls into an application case
                if (invitation.UserId == invitation.CreatorId)
                {
                    if (customer.Id == group.AdminId)
                    {
                        // Only the owner of the group can accept or deny it.
                        return true;
                    }

                    return false;

                }

                // Otherwise, we wait for the target user to accept or deny it.
                if (customer.Id == invitation.UserId)
                {
                    // The group owner
                    return true;
                }

                return false;
            }

            return false;
        }


        public async Task PrepareGroupInvitationModel(IUserGroup group, IUserGroupInvitation invitation, InvitationModel model)
        {
            if (invitation == null || model == null)
                return;

            var invitationCreator = _customerService.GetCustomerById(invitation.CreatorId);

            model.Id = invitation.Id;

            model.GroupId = invitation.GroupId;
            model.OwnerId = group.AdminId;

            model.Agree = !invitation.Declined;
            // invitationModel.DeclinedByOwner = invitation.Declined;

            model.CreatedOn = invitation.CreatedOnUtc;
            model.Comment = invitation.Comment;
            model.CreatorId = invitation.CreatorId;
            model.ExpiredOn = invitation.ExpiredOnUtc;
            model.UserId = invitation.UserId;
            model.ProviderId = invitation.ProviderId;
            model.CreatorEmail = invitationCreator.Email;
            model.CreatorName = invitationCreator.GetFullName();

            // User name depends on user provider
            if (invitation.ProviderId == (short)PredefinedUserProvider.NXdriveAccount)
            {
                var user = this._customerService.GetCustomerById(model.UserId);

                model.UserEmail = user.Email;
                var fullName = user.GetFullName();
                if (string.IsNullOrEmpty(fullName))
                    model.UserDisplayName = user.Email;
                else
                    model.UserDisplayName = fullName;
            } else if (invitation.ProviderId == (short)PredefinedUserProvider.Email)
            {
                var emailUser = await this._userIdentityService.GetEmailIdentityById(model.UserId);

                model.UserEmail = emailUser.Email;
                model.UserDisplayName = emailUser.Email;
            }

            model.GroupName = group.Name;
            model.AvatarImageId = group.AvatarImageId;
            model.AvatarUrl = _pictureService.GetPictureUrl(group.AvatarImageId,
                _mediaSettings.AvatarPictureSize, true);

            // determine the type of this invitation
            if (invitation.CreatorId == group.CreatorId)
            {
                model.Type = InvitationType.GroupOwnerInvitation;
            }
            else if (invitation.ProviderId == (short)PredefinedUserProvider.NXdriveAccount  && invitation.CreatorId == invitation.UserId)
            {
                // Must be a nxdrive user and the creator is the target as well
                model.Type = InvitationType.NonMemberApplicant;
            }
            else
            {
                model.Type = InvitationType.GroupMemberInvitation;
            }

            var customer = this._workContext.CurrentCustomer;

            model.Declined = invitation.Declined;
            model.AllowDelete = CanDeleteInvitation(customer, group, invitation);
            model.AllowAcceptOrDeny = CanAcceptOrDenyInvitation(customer, group, invitation);
        }

        public void PrepareUnregisteredGroupMemberModel(IUserGroup group, string email, MemberModel memberModel)
        {
            if (group == null || memberModel == null)
                return;

            // Note: no customerId in memberModel
            memberModel.CustomerEmail = email;
            memberModel.GroupId = group.Id;
            memberModel.CustomerName = email;
            memberModel.ModifiedOn = DateTime.Now;
            // memberModel.State = UserGroupMemberState.Pending;

            memberModel.AllowDelete = false;
        }
    }
}
