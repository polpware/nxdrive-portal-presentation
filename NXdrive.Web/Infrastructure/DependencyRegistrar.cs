using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Web.Framework.Factories;
using NXdrive.NopExt.ServicePlans;

namespace NXdrive.Web.Infrastructure
{
    using Factories = Polpware.NopWeb.Factories;
    using NXdriveFactories = NXdrive.Web.Factories;
    using PolpwareNopExtSubsPlan = Polpware.NopExt.SubsPlan;
    using PolpwareNopExtPresentationSubsPlan = Polpware.NopExt.Presentation.SubsPlan;


    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //installation localization service
            //  builder.RegisterType<InstallationLocalizationService>().As<IInstallationLocalizationService>().InstancePerLifetimeScope();

            // Services from other libraries
            builder.RegisterType<PlanSpecificationService>()
                .As<IPlanSpecificationService>()
                .WithParameter("specRelativePath", "~/App_Data/PlanSpecification.xml")
                .InstancePerLifetimeScope();

            // Override processing services
            builder.RegisterType<PolpwareNopExtSubsPlan.Orders.OrderProcessingService>()
                .As<PolpwareNopExtSubsPlan.Orders.IOrderProcessingService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PolpwareNopExtSubsPlan.Common.AddressService>()
                .As<PolpwareNopExtSubsPlan.Common.IAddressService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PolpwareNopExtPresentationSubsPlan.Services.PrepareOrderModelService>()
                .As<PolpwareNopExtPresentationSubsPlan.Services.PrepareOrderModelService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PolpwareNopExtSubsPlan.Orders.OrderFactoryService>()
                .As<PolpwareNopExtSubsPlan.Orders.OrderFactoryService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PolpwareNopExtSubsPlan.Orders.SubstOrientedOrderAccessService>()
                .As<PolpwareNopExtSubsPlan.Orders.SubstOrientedOrderAccessService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PolpwareNopExtSubsPlan.Customers.PaymentRequestSerializationService>()
                .As<PolpwareNopExtSubsPlan.Customers.PaymentRequestSerializationService>()
                .InstancePerLifetimeScope();

            //common factories
            builder.RegisterType<AclSupportedModelFactory>().As<IAclSupportedModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<DiscountSupportedModelFactory>().As<IDiscountSupportedModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<LocalizedModelFactory>().As<ILocalizedModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<StoreMappingSupportedModelFactory>().As<IStoreMappingSupportedModelFactory>().InstancePerLifetimeScope();

            //factories
            builder.RegisterType<Factories.AddressModelFactory>().As<Factories.IAddressModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.BlogModelFactory>().As<Factories.IBlogModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.CatalogModelFactory>().As<Factories.ICatalogModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.CheckoutModelFactory>().As<Factories.ICheckoutModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.CommonModelFactory>().As<Factories.ICommonModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.CountryModelFactory>().As<Factories.ICountryModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.CustomerModelFactory>().As<Factories.ICustomerModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ForumModelFactory>().As<Factories.IForumModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ExternalAuthenticationModelFactory>().As<Factories.IExternalAuthenticationModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.NewsModelFactory>().As<Factories.INewsModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.NewsletterModelFactory>().As<Factories.INewsletterModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.OrderModelFactory>().As<Factories.IOrderModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.PollModelFactory>().As<Factories.IPollModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.PrivateMessagesModelFactory>().As<Factories.IPrivateMessagesModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ProductModelFactory>().As<Factories.IProductModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ProfileModelFactory>().As<Factories.IProfileModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ReturnRequestModelFactory>().As<Factories.IReturnRequestModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ShoppingCartModelFactory>().As<Factories.IShoppingCartModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.TopicModelFactory>().As<Factories.ITopicModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.VendorModelFactory>().As<Factories.IVendorModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.WidgetModelFactory>().As<Factories.IWidgetModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Factories.ProductModelFactory>().As<Factories.IProductModelFactory>().InstancePerLifetimeScope();

            // More Factories
            builder.RegisterType<NXdriveFactories.ServicePlanModelFactory>().As<NXdriveFactories.IServicePlanModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<Constraints.UserGroupConstraints>().As<Constraints.IUserGroupConstraints>().InstancePerLifetimeScope();
            builder.RegisterType<NXdriveFactories.UserGroupModelFactory>().As<NXdriveFactories.IUserGroupModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<NXdriveFactories.UserGroupMemberModelFactory>().As<NXdriveFactories.IUserGroupMemberModelFactory>().InstancePerLifetimeScope();
            builder.RegisterType<NXdriveFactories.CustomerContactModelFactory>().As<NXdriveFactories.ICustomerContactModelFactory>().InstancePerLifetimeScope();

            builder.RegisterType<Extensions.SendEmailService>().As<Extensions.SendEmailService>().InstancePerLifetimeScope();


            // Debug helpers
#if DEBUG
            // builder.RegisterType<Tests.TestWorkContext>().As<Nop.Core.IWorkContext>().InstancePerLifetimeScope();
#endif

        }

        /// <summary>
        /// Gets order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 2; }
        }
    }
}
